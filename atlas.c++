// ©120[17-20] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats

#include "atlas.h++"
#include "util.h++"
#include "msg.h++"
#include "selfed.h++"

#ifndef LICENSE
# error "Expecting a LICENSE definition"
#endif

#include <cstdlib>
#include <climits>
#include <sys/mman.h>
#include <linux/mman.h>
#include <linux/fs.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/user.h>
#include <sys/file.h>
#include <sys/sendfile.h>
#include <fcntl.h>
#include <dirent.h>
#include <unistd.h>
#include <stropts.h>
#include <cstring>
#include <cassert>
#include <cstdio>
#include <csignal>

#include <array>
#include <bit>
#include <bitset>
#include <utility>
#include <span>
using std::span;
#include <new>
#include <memory>
#include <iterator>
#include <optional>
#include <limits>
#include <type_traits>
#include <iostream>

//#include "xxhash.h"  // We want the one from our externals/, not the system

#ifdef ENABLE_CHARTING
# include "conc/concbuilder.h++"
#endif

#ifdef EMBARALLEL
# define FOLLY_NO_CONFIG
# include "folly/AtomicBitSet.h"
using folly::AtomicBitSet;
# include <atomic>
#endif

#ifdef DEBUG
# include "itox.h++"
//# include <libexplain/mmap.h>
//# include <libexplain/open.h>
#endif

#if defined ENABLE_INDEXING && !defined NDEBUG && defined DEBUG
# define ENABLE_QUERIES
#endif

//namespace atlas {

////////////////////////////////////////////////////////////////////////////////////////////////////
// Globals

#ifdef SINGULAR_ATLAS
extern WordAtlasSlice _atlas_start, _atlas_end;
template <typename uint = Word>
static inline /*constexpr*/ const auto& _atlas = *(Atlas<uint>*)(_atlas_start);
static inline /*constexpr*/ const auto& atlas = _atlas<Word>;
static consteval size_t atlas_size = ATLAS_SIZE;
#endif // SINGULAR_ATLAS

static volatilish bool cancelled = false;

// Set-once globals
static std::optional<Direction> sole_direction = std::nullopt;
static std::optional<Negation> sole_negation = std::nullopt;
static std::optional<Offset> sole_size = std::nullopt;
static int work_fd = -1, dest_dir_fd = -1;

static
#ifndef ENABLE_INDEXING
consteval
#endif
std::optional<size_t> work_blkdev_off = std::nullopt;
static const size_t blkdev_alignment = sysconf(_SC_PAGESIZE); // Conservatively use page size

#ifdef DEBUG
static const char *debug_magic = nullptr;
#endif

static INLINE constexpr PURE bool
skip_sole_sized(SZ sz) { return UNLIKELY(!!sole_size) && LIKELY(*sole_size != size_for_sz(sz)); }
static INLINE constexpr PURE bool
skip_sole_directed(Direction d) { return UNLIKELY(!!sole_direction) && (*sole_direction != d); }
static INLINE constexpr PURE bool
skip_sole_negated(Negation n) { return UNLIKELY(!!sole_negation) && LIKELY(*sole_negation != n); }

#define ENV_VAR_PFX "ATLAS_"

#define SOLE_DIRECTION_ENV_VAR ENV_VAR_PFX "SOLE_DIRECTION"
#define SOLE_NEGATION_ENV_VAR ENV_VAR_PFX "SOLE_NEGATION"
#define SOLE_SIZE_ENV_VAR ENV_VAR_PFX "SOLE_SIZE"
#define WORK_ENV_VAR ENV_VAR_PFX "WORK"
#define DDIR_ENV_VAR ENV_VAR_PFX "DDIR"
#define WORK_OFF_ENV_VAR ENV_VAR_PFX "WORK_OFFSET"

#ifdef DEBUG
# define DEBUG_ENV_VAR ENV_VAR_PFX "DEBUG"
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////
// Support API
////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////
// Utilities

// Word to (padded) hex
#define wtox(w) itox((w), sizeof(Word)*2)

using std::byte;
using std::move;

template<typename Ret, typename F, typename... Args>
static consteval bool
result_conforms = std::is_same_v<Ret, std::invoke_result_t<F, Args...>>;

#define LIST_CHAR(_) CHARIFY(_),
static constexpr const char Dirs[] = { APPLY_Directions(LIST_CHAR) };
static constexpr const char Negs[] = { APPLY_Negations(LIST_CHAR) };
#undef LIST_CHAR
static_assert(sizeof(Negs) == num_negations);
static consteval char dir_chr(Direction D) { return Dirs[(uint8_t)D]; }
static consteval char neg_chr(Negation N)  { return Negs[(uint8_t)N]; }

static constexpr CONST std::optional<Direction>
parse_direction(const char _) { switch (_) {
case 'F': case 'f': return Direction::F;
case 'B': case 'b': return Direction::B;
default: return std::nullopt;
}}

static constexpr PURE NONNULL std::optional<Direction>
parse_direction(const char *str)
{ ifU (strlen(str) != 1) return std::nullopt; else return parse_direction(str[0]); }

static constexpr CONST std::optional<Negation>
parse_negation(const char _) { switch (_) {
case 'I': case 'i': return Negation::I;
case 'N': case 'n': return Negation::N;
case 'E': case 'e': return Negation::E;
case 'O': case 'o': return Negation::O;
default: return std::nullopt;
}}

static constexpr PURE NONNULL std::optional<Negation>
parse_negation(const char *str)
{ ifU (strlen(str) != 1) return std::nullopt; else return parse_negation(str[0]); }

static PURE NONNULL std::optional<size_t>
parse_size(const char *str, int base = 0) {
    static_assert(sizeof(size_t) == sizeof(unsigned long long));
    const auto prev_errno = errno; errno = 0;
    const auto size = strtoull(str, nullptr, base);
    const auto post_errno = errno; errno = prev_errno;
    return LIKELY(post_errno == 0) ? std::optional(size) : std::nullopt;
}
static PURE NONNULL std::optional<Offset>
parse_sz_size(const char *str, int base = 0) {
    if (const auto size = parse_size(str, base); size && is_sz_size(*size)) return (Offset)*size;
    return std::nullopt;
}

static PURE NONNULL std::optional<SZ>
parse_sz(const char *str) {
    if (const auto size = parse_sz_size(str)) return size_to_sz(*size);
    else return std::nullopt;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Streams

#if defined ENABLE_CHARTING || defined ENABLE_DECHARTING
// TODO: streams
# include <ostream>
using ByteOStream = std::basic_ostream<byte>;
using WordOStream = std::basic_ostream<Word>;
using DescOStream = std::basic_ostream<Desc>;
# include <istream>
using ByteIStream = std::basic_istream<byte>;
using WordIStream = std::basic_istream<Word>;
using DescIStream = std::basic_istream<Desc>;
# include <fstream>
# include <ext/stdio_filebuf.h>
template<typename T>
using StdIOBuf = __gnu_cxx::stdio_filebuf<T>;
using ByteStdIOBuf = StdIOBuf<byte>;
using WordStdIOBuf = StdIOBuf<Word>;
using DescStdIOBuf = StdIOBuf<Desc>;
#endif // defined(ENABLE_CHARTING) || defined(ENABLE_DECHARTING)

////////////////////////////////////////////////////////////////////////////////////////////////////
// Bit fiddling and Negations

#define ASSUME_EQ_IFF_IDENTITY(N, w, w_) ASSUME(((N) == Negation::I) ^ ((w) != (w_)))

#define _(n) (byte)(n)
#define R2(n)  _(n),  _(n + (2u*64u)),  _(n + (1u*64u)),  _(n + (3u*64u))
#define R4(n) R2(n), R2(n + (2u*16u)), R2(n + (1u*16u)), R2(n + (3u*16u))
#define R6(n) R4(n), R4(n + (2u*4u )), R4(n + (1u*4u )), R4(n + (3u*4u ))
static constexpr const byte byte_reverse_bit_table[256] = { R6(0u), R6(2u), R6(1u), R6(3u) };
#undef _
#undef R2
#undef R4
#undef R6
static_assert(byte_reverse_bit_table[0] == (byte)0);
static_assert(byte_reverse_bit_table[0xFF] == (byte)0xFF);
static_assert(byte_reverse_bit_table[0b00000010] == (byte)0b01000000);
static_assert(byte_reverse_bit_table[0b00001100] == (byte)0b00110000);
static_assert(byte_reverse_bit_table[0b00111000] == (byte)0b00011100);
static_assert(byte_reverse_bit_table[0b01110000] == (byte)0b00001110);
static_assert(byte_reverse_bit_table[0b11110000] == (byte)0b00001111);

static consteval byte reverse_bits(byte b) {return byte_reverse_bit_table[(uint8_t)b];}

/// Recommended to investigate only if your D$ is > 64K
#if 0
static constexpr const auto _2byte_reverse_bit_table = []{
    constexpr uint32_t max = std::numeric_limits<uint16_t>::max();
    std::array<uint16_t, max+1> arr{0};
    for(uint32_t i=0; i<=max; i++) {
        const auto w = (uint16_t)i;
        arr[i] = ((uint16_t)reverse_bits((byte)(w & 0xFF)) << 8)
               |  (uint16_t)reverse_bits((byte)(w >> 8));
    }
    return arr;
}();
static_assert(_2byte_reverse_bit_table[0] == 0);
static_assert(_2byte_reverse_bit_table[0xFFFF] == 0xFFFF);

static inline constexpr HOT void
reverse_bits(const uint16_t *const NO_ALIAS in, uint16_t *const NO_ALIAS out, uint8_t len) {
    //#pragma omp simd simdlen(2)
    for(uint8_t i=0; i<len; w++)
        word_[i] = _2byte_reverse_bit_table[word[len-1-i]];
}

template<typename I>
static consteval std::enable_if_t<is_unsigned_integral<I> && divides(2, sizeof(I))
, I> reverse_bits(const I _word) {
    constexpr auto len = sizeof(I)/2;
    auto word = reinterpret_cast<const uint16_t *const NO_ALIAS>( LAUNDER(&_word) );
    uint16_t word_[len];
    reverse_bits(word, word_, len);
    return *LAUNDER(reinterpret_cast<I*>( LAUNDER(&word_) ));
}
#endif

static inline constexpr HOT void
reverse_bits(const byte *const NO_ALIAS in, byte *const NO_ALIAS out, const uint8_t size) {
    ASSUME(size); uint8_t i=0;
    //#pragma omp simd simdlen( sizeof(std::byte) )
    for(i=0; i<size; i++) out[i] = reverse_bits(in[size-1u-i]);
}

template<typename I>
static consteval HOT std::enable_if_t<is_unsigned_integral<I>
, I> reverse_bits(const I _word) {
    constexpr auto size = sizeof(I);
    const auto word = reinterpret_cast<const byte *const NO_ALIAS>( LAUNDER(&_word) );
    byte word_[size];
    reverse_bits(word, word_, size);
    return *LAUNDER(reinterpret_cast<I*>( LAUNDER(&word_) ));
}

// Value-level dispatcher
template<typename uint>
static consteval HOT std::enable_if_t<is_unsigned_integral<uint>
, uint> negate(uint w, Negation neg = Negation::I) {
    switch(neg) {
    #define CASE(Neg)                                                                            \
        case Negation::Neg: {                                                                    \
            const auto w_ = negate<uint, Negation::Neg>(w);                                      \
            DBG("Negating: %s ~~%c> %s", wtox((Word)w), neg_chr(Negation::Neg), wtox((Word)w_)); \
            return w_;                                                                           \
        }
        APPLY_Negations(CASE)
    #undef CASE
        default: UNREACHABLE;
    }
}

// TODO: No real point in a reverse negation table, as `negate` doesn't cost that much
#if 0
#include "iota.h++"
constexpr byte reverse_negation_table[256][num_negations-1] = { // Minus 1 Negation for I
# define _ ENTRY_(IOTA)
// NB: `i` is for capture by `ENTRY`
# define ENTRY_(i) { ENTRY(i,N), ENTRY(i,E), ENTRY(i,O) }
// FIXME: Is `i` only evaluated once for IOTA?
#define ENTRY(i,N) negate(reverse_bits((uint8_t)i), Negation::N)
    _, _, _, _, _, _, _, _,
    _, _, _, _, _, _, _, _,
    _, _, _, _, _, _, _, _,
    _, _, _, _, _, _, _, _,
    _, _, _, _, _, _, _, _,
    _, _, _, _, _, _, _, _,
    _, _, _, _, _, _, _, _,
    _, _, _, _, _, _, _, _,

#undef _
#undef ENTRY_
#undef ENTRY
};

template<typename uint>
inline constexpr std::enable_if_t<is_unsigned_integral<uint>
, uint> reverse_negate(uint w, Negation neg = Negation::I) {
    if (neg == Negation::I) return reverse_bits(w);
    else {
        constexpr uint8_t size = sizeof(uint);
        auto word = (const byte*)&w;
        byte word_[size];
        #pragma omp simd simdlen(1)
        for(uint8_t b=0; b<size; b++)
            word_[b] = reverse_negation_table[word[size-b]][(uint8_t)neg-1];
        return *(uint*)&word_;
    }
}
#endif

using Hash = uint32_t;  //XXH32_hash_t;

template<typename uint, size_t len>
static consteval bool
is_acceptable_equivalence = len > 0 && is_unsigned_integral<uint>;
//                          && divides(sizeof(Hash), sizeof(uint));
//                         && divides(sizeof(Hash), sizeof(uint) * len);

template<typename uint, Direction D = Direction::F, Negation N = Negation::I>
static inline constexpr PURE HOT std::enable_if_t<is_unsigned_integral<uint>, bool>
negationwise_equivalent(debuggable AtlasSlice<uint> atlas_blk,
                        debuggable AtlasSlice<uint> blk,
                        size_t len=1) {
    DBG("@%c,%c; Equivocating on %p vs. %p of len %lu and size %lu",
        dir_chr(D), neg_chr(N), atlas_blk, blk, len, sizeof(uint)*len);
    for (size_t _i = 0; _i < len; _i++) {
        // NB: Though it semantically doesn't matter in which order we traverse, let's be consistent
        debuggable const auto i = (D == Direction::F) ? _i : (len-1) - _i;
        ASSUME(&blk[i] != &atlas_blk[i]);
        debuggable const uint _word = atlas_blk[i];
        debuggable const uint word  = (D == Direction::F) ? _word : reverse_bits(_word);
        debuggable const uint word_ = negate<uint, N>(word);
        ASSUME_EQ_IFF_IDENTITY(N, word, word_);
        if (word_ != blk[i]) {
            // FIXME: this DBG is hardcoded to Words
            DBG("@%zu; %s != %s ~~%c> %s ~~%c> %s",
                i, wtox((Word)blk[i]), wtox((Word)_word),
                dir_chr(D), wtox((Word)word),
                neg_chr(N), wtox((Word)word_));
            return false;
        }
    }
    return true;
}

#define ASSUME_SEPARATE_SPANS(S1, S2)   \
    ASSUME(S1.cbegin() != S2.cbegin()); \
    ASSUME(S1.cend()   != S2.cend());   \
    ASSUME(S1.data()   != S2.data());

template<typename uint, size_t len, Direction D = Direction::F, Negation N = Negation::I>
static inline constexpr PURE HOT std::enable_if_t<is_acceptable_equivalence<uint, len>, bool>
negationwise_equivalent(const span<const uint, len> atlas_blk, const span<const uint, len> blk) {
    ASSUME_SEPARATE_SPANS(atlas_blk, blk);
    return negationwise_equivalent<uint, D, N>(atlas_blk.data(), blk.data(), len);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Numerics

template<typename T>
static consteval std::enable_if_t<is_unsigned_integral<T>, T>
abs_diff(T a, T b) { return (a > b) ? a - b : b - a; }

template<typename T>
static consteval std::enable_if_t<is_unsigned_integral<T>, T>
round_up(T val, T factor) { return ((val / factor) + ((val % factor) ? 1 : 0)) * factor; }

static consteval Offset
round_up_pow2(Offset v) { return std::ceil2(v); }

static consteval Offset
log2_pow2(Offset v) { ASSUME(std::ispow2(v)); return std::log2p1(v); }

////////////////////////////////////////////////////////////////////////////////////////////////////
// Hash API

static consteval Hash max_Hash = std::numeric_limits<Hash>::max();
static consteval auto max_Hashes = (size_t)max_Hash+1;
static_assert(divides(sizeof(Hash), sizeof(Word)));

static INLINE_ constexpr CONST HOT Hash
_hash(Hash addendum, const Hash init = 0) {
    Hash hash = init;
    // Since our input is (supposed to be) random, we can't rely on normal hashing algorithms
    // which distribute/diffuse entropy via primes. Instead, we rely on the input itself alone,
    // though encouraging some slight symmetric skewing for avalanching. In particular, we can't
    // rely on _xor alone, given that our Negations would all result in the same hash. We thus
    // use _add to saturate in one direction, _sub for the other, and shifting them back to
    // "cancel" the motion, around an _xor. Further, the parity of the hash determines in which
    // direction we start the saturation.
    //const auto _add = [&] { hash += addendum; };
    auto&& _xor = [&] { hash ^= addendum; };
    //const auto _sub = [&] { hash -= addendum; };
#if 0
    // FIXME: abstract popcount
    switch (__builtin_popcountll(addendum) % 2) {
    case 0: _sub(); _xor(); _add(); break;
    case 1: _add(); _xor(); _sub(); break;
    default: UNREACHABLE;
    }
#endif
    // NOTE: For now, we simply xor so as to keep e.g. indices Negation-invariant wrt hashes
    _xor();
    return hash;
}

template<Direction D = Direction::F, Negation N = Negation::I>
static inline constexpr PURE Hash
short_hash(const uint8_t size, const byte blk[/*size*/], const Hash init = 0) {
    ASSUME(size && size < sizeof(Hash));
    // FIXME: Hash casting probably just reads excess memory, not 0, though we truncate at the end
    debuggable const Hash _addendum = [size,blk]{
        if constexpr (D == Direction::F) return *LAUNDER(reinterpret_cast<const Hash *const>(&blk));
        else { ASSUME(D == Direction::B);
            byte blk_[size];
            reverse_bits(blk, blk_, size);
            return *LAUNDER(reinterpret_cast<const Hash *const>(&blk_));
        }
    }();
    debuggable const Hash addendum = negate<Hash, N>(_addendum);
    // Pad the uneven remainder with 0 after the data
    constexpr Hash full_mask = max_Hash;
    const auto shift = sizeof(Hash) - size;
    // FIXME: Maybe negate before masking?
    const auto partial = addendum & (((full_mask >> shift) << shift) >> shift);
    return _hash(partial, init);
}

// For any other hashing, we have to stream since we dynamically manipulate the bits
template<Direction D = Direction::F, Negation N = Negation::I>
static /*inline*/ constexpr PURE NONNULL _FLATTEN HOT Hash
_hash_blk(WordAtlasSlice _blk, const size_t size, const Hash init = 0) {
    //DBG("Hashing: %p, %lu, %.8x", _blk, size, init);
#ifdef ENABLE_SEGMENTS
    ASSUME(size);
    ifU (size < sizeof(Hash))
        return short_hash<D,N>((uint8_t)size, reinterpret_cast<ByteAtlasSlice>(_blk), init);
#else
    ASSUME_SZ_SIZE(size);
#endif //ENABLE_SEGMENTS
    Hash hash = init; const auto parts = size/sizeof(Hash) ?: 1u;
    //XXH32_state_t state;      // Stack-allocate instead of using their allocation API
    //XXH32_reset(&state, init);
    if (init) { DBG_(init, ".8x"); }
    auto blk = (AtlasSlice<Hash>)_blk;
    const auto&& get = [blk,parts] (Offset i)
    { if constexpr (D == Direction::F) return blk[i]; else return reverse_bits(blk[parts-1-i]); };
    // GCC 8.2 ICEs (@_@), so we bypass `_hash` and speak of an xor directly
//#if 0
    #pragma omp declare reduction \
        ( hash : Hash : omp_out=_hash(omp_in,omp_out) ) \
        initializer(omp_priv=omp_orig)
//#endif
    //#pragma omp simd reduction(^:hash)
    //#pragma omp simd simdlen( sizeof(Hash) ) aligned(blk: sizeof(Hash))
    for(size_t i=0; i<parts; i++) {
        debuggable const Hash _addendum = get(i);
        debuggable const Hash addendum = negate<Hash, N>(_addendum);
        hash = _hash(addendum, hash);
        //hash ^= addendum;
        //DBG("Hash: %.8x", hash);

    #if 0
        auto subhash = std::hash<Word>{}(word);
        auto subhash_ = (const Hash*)&subhash;
        static_assert(sizeof(subhash) >= sizeof(hash));
        for(uint8_t i=0; i<(sizeof(subhash)/sizeof(hash)); i++)
            hash ^= subhash_[i];
    #endif

    #if 0
        ifU (XXH32_update(&state, &word, sizeof(Word)) == XXHERROR)
            ERR("Failed to XXH32_update: %lu", i);
    #endif
    }
#ifdef ENABLE_SEGMENTS
    debuggable const auto rem = size % sizeof(Hash);
    ifU (rem) hash = short_hash<D,N>((uint8_t)rem , (ByteAtlasSlice)&blk[parts], hash);
#else
    ASSUME(divides(sizeof(Hash), size));
#endif // ENABLE_SEGMENTS
#if 0
    Hash hash = XXH32_digest(&state);
    DBG("Hashed: %.8x", hash);
#endif
    //if (!hash) { WRN("Nullary hash!"); }
    return hash;
}

template<Direction D = Direction::F, Negation N = Negation::I>
static INLINE_ constexpr NONNULL void
hash_blk(WordAtlasSlice blk, const size_t size, Hash& hash) { ASSUME_SZ_SIZE(size);
    hash = _hash_blk<D,N>(blk, size, hash);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// mmap API

static inline const unsigned page_size = blkdev_alignment, madvise_limit = page_size/2;
template<typename T>
static consteval bool mmappablish =
    /*(std::is_lvalue_reference_v<T> || std::is_rvalue_reference_v<T> || !std::is_reference_v<T>)
    &&*/ !std::is_pointer_v<T>;
template<typename T/*, typename = std::enable_if_t<mmappablish<T>>*/>
static consteval bool mmappable = mmappablish<T> /*&& std::has_unique_object_representations_v<T>*/;
static_assert(!mmappablish<void*>);
template<typename T/*, typename = std::enable_if_t<mmappablish<T>>*/>
using to_mmappable = std::remove_cvref_t<std::remove_pointer_t<std::remove_reference_t<T>>>;
static_assert(mmappablish<to_mmappable<bool*>>);

template<typename T, typename = std::enable_if_t<mmappable<T>>>
static PURE COLD T /*&*/*const NO_ALIAS SHOULD_NONNULL
mmap_safe(int fd, size_t size=sizeof(T), int prot=PROT_READ, long map_flags=MAP_POPULATE,
          long madv_flags=MADV_NORMAL, bool should_close=true) {
    using T_ = to_mmappable<T>;
    auto _ = (const T_ *const)::mmap(nullptr, size, prot, map_flags, fd, 0);
    ifU (!_ || _ == MAP_FAILED) {
    //#ifndef DEBUG
        ERR("Failed to mmap [%d]: %p (%lu); %m", fd, _, size);
    /*#else
        ERR("%s", explain_mmap(nullptr, size, prot, map_flags, fd, 0));
    #endif*/
        abort();
    #ifdef NDEBUG
        UNREACHABLE;
    #endif
        return nullptr;
    }
    if (should_close) close(fd);
    (void)madvise((void*)_, size, madv_flags | MADV_DONTDUMP);
    return /*LAUNDER*/((T_ *const NO_ALIAS)LAUNDER(_));
}

template<typename T, typename = std::enable_if_t<mmappable<T>>>
static inline PURE COLD const T/*&*/ *const NO_ALIAS SHOULD_NONNULL
mmap_in(int fd, size_t size=sizeof(T), long madv_flags=MADV_NORMAL, bool should_close=true)
{ return mmap_safe<const T>(fd, size, PROT_READ, MAP_PRIVATE, madv_flags, should_close); }

template<typename T, typename = std::enable_if_t<mmappable<T>>>
static inline COLD T/*&*/ *const NO_ALIAS SHOULD_NONNULL
mmap_out(int fd, size_t size=sizeof(T), long madv_flags=MADV_NORMAL, bool should_close=true) {
    return mmap_safe<T>(fd, size, PROT_READ | PROT_WRITE, MAP_SHARED_VALIDATE
                      , madv_flags, should_close);
}

template<typename T, typename = std::enable_if_t<mmappable<T>>>
static inline COLD /*auto*/ T /*&*/*const NO_ALIAS SHOULD_NONNULL
mmap_dyn(const int fd, const bool readonly, const size_t size=sizeof(T), long madv_flags=MADV_NORMAL,
         const bool should_close=true) /*-> decltype(auto)*/ { return readonly
         ? mmap_in<const T>(fd, size, madv_flags, should_close)
         : mmap_out<     T>(fd, size, madv_flags, should_close);
}

template<typename uint, typename = std::enable_if_t<mmappable<uint>>>
static inline PURE COLD const Atlas<uint>& NO_ALIAS
mmap_atlas(int fd, size_t atlas_size, int madv_flags=MADV_NORMAL) { ASSUME_ATLAS_SIZE(atlas_size);
    const auto map_flags = MAP_PRIVATE | MAP_POPULATE
             , madv_flags_ = madv_flags | MADV_HUGEPAGE;
    return *mmap_safe<const Atlas<uint>>(fd, atlas_size, PROT_READ, map_flags, madv_flags_, true);
}

static inline NONNULL int /* errno */
maybe_madvise(void *const addr, size_t size, int advice) {
    ifU (size >= madvise_limit && divides(page_size, (uintptr_t)addr))
        ifU (madvise(addr, size, advice) != 0) return errno;
    return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Recursive directory & file creation API

static consteval uint8_t max_current_path_len = 32;

#define STATIC_ASSERT_VALID_FILENAME(path)              \
static_assert(sizeof(path)-1 <= max_path_component_len, \
              "Expected " path " to be no longer than max_path_component_len");

static inline COLD std::optional<struct stat>
get_file_stats(int fd) {
    struct stat stats; if (auto ret = fstat(fd, &stats); UNLIKELY(ret != 0)) {
        ERR("Failed to stat: %d; %m", ret); return std::nullopt;
    } else return stats;
}

static inline COLD bool is_blkdev(const struct stat& stats) { return S_ISBLK(stats.st_mode); }
static inline COLD bool is_blkdev(int fd)
{ if (auto stats = get_file_stats(fd)) return is_blkdev(*stats); else return false; }

static inline COLD bool is_pipe(const struct stat& stats) { return S_ISFIFO(stats.st_mode); }
static inline COLD bool is_pipe(int fd)
{ if (auto stats = get_file_stats(fd)) return is_pipe(*stats); else return false; }

/// FIXME: Assumes block device or file or fifo
static inline COLD size_t get_file_size(int fd, const struct stat& stats) {
    /*constexpr*/ const size_t storage_block_size = blkdev_alignment;
    if (is_pipe(stats)) return 0;
    return !is_blkdev(stats) ? stats.st_size : storage_block_size * ({
        unsigned long size = 0; ioctl(fd, BLKGETSIZE, &size); size;
    });
}

static inline COLD size_t get_file_size(int fd)
{ if (auto stats = get_file_stats(fd)) return get_file_size(fd, *stats); else return 0; }

/// NB: Nonreentrant
template<typename T>
static inline COLD std::enable_if_t<std::is_trivial_v<T>, uint8_t>
append_mkdir(char current_path[max_current_path_len], uint8_t current_path_len, T component);

#define IMPL_append_mkdir(T, fmt, expr)                                                \
template<> inline COLD uint8_t                                                         \
append_mkdir<T>(char current_path[max_current_path_len], uint8_t pathlen, T component) \
{return snprintf(&current_path[pathlen], max_current_path_len, "%" fmt "/", expr);}

IMPL_append_mkdir(SZ       , "hhu", idx_for_sz(component))
IMPL_append_mkdir(Direction,   "c", dir_chr(component))
IMPL_append_mkdir(Negation ,   "c", neg_chr(component))
#undef IMPL_append_mkdir

/// Saving some space from these functions, at the cost of assuming single-threaded usage
// (which is a reasonable assumption for the cold action of file/directory creation)
static char current_path[max_current_path_len] = {'\0'}; /// simple scratch buffer
static uint8_t current_path_len = 0u;
#define ASSUME_VALID_current_path_len \
ASSUME(0 < current_path_len && current_path_len < max_current_path_len, current_path_len, "hhu")

static inline void clear_current_path(void) { memset(current_path, 0, max_current_path_len); }
static inline void clear_trailing_current_path(uint8_t trail=0) { ASSUME_VALID_current_path_len;
    memset(&current_path[current_path_len], 0, max_current_path_len-current_path_len);
    current_path_len -= trail; ASSUME_VALID_current_path_len;
}

/// NB: Nonreentrant
template<typename T> static COLD int_fast8_t /* number of chars appended */
append_mkdir_current_path(const T component, const bool create=false) {
#ifndef ENABLE_INDEXING
    ASSUME(!create);
#endif
    ASSUME_VALID_current_path_len;
    const auto appended = append_mkdir<T>(current_path, current_path_len, component);
    ASSUME(appended); current_path_len += appended; ASSUME_VALID_current_path_len;
    if (create) {
        constexpr auto mode = S_IRUSR | S_IWUSR | S_IXUSR;
        const int ret = [=]{
            ifU (work_fd == -1 && dest_dir_fd == -1) return mkdir(current_path, mode);
            else { const auto&& mk = [=](int fd)
               { return mkdirat((fd != -1) ? fd : AT_FDCWD, current_path, mode); };
               ifL (!work_blkdev_off) {
                   int ret = mk(work_fd);
                   ifU (ret != 0 && errno != EEXIST) return ret;
               }
               return mk(dest_dir_fd);
            }
        }();
        ifU (ret != 0) {
            ifL (errno == EEXIST) {DBG("Accepting existing directory: %s", current_path);} else {
                ERR("Failed mkdir %.*2$s ;[%hhu]: %m", current_path, current_path_len);
                clear_trailing_current_path(appended);
                return ret;
            }
        }
        DBG("Created %s", current_path);
    }
    ASSUME_VALID_current_path_len; return appended;
}

static consteval uint8_t max_path_component_len = 6u;
static_assert(max_path_component_len * 5u < max_current_path_len);

/// NB: Nonreentrant
static NONNULL COLD decltype(auto) /* fd */
open_file(const char *const path, size_t size=0, bool create=false, const bool readonly=true,
          const int dir_fd=-1) { DBG("opening: %s; %zu", path, size);
    /// "If create is true, readonly shouldn't be"
    ASSUME(!create ?: !readonly); const auto ro = !(create || !readonly);
    auto flags = (ro ? O_RDONLY : O_RDWR) | O_LARGEFILE | O_CLOEXEC;
    if (create) flags |= O_CREAT | O_EXCL | O_TRUNC;
    const auto fd = (dir_fd == -1)
                  ? ro ? open(path, flags) : open(path, flags, 0644)
                  : ro ? openat(dir_fd, path, flags) : openat(dir_fd, path, flags, 0644);
    ifU (fd < 0) {
    //#ifndef DEBUG
        WRN("Failed to open(%hhu/%hhu) %s: %d; %m", ro, (dir_fd==-1), path, fd);
    /*#else
        WRN("%s", create ? explain_open(current_path, flags, 0644)
                         : explain_open(current_path, flags);
    #endif*/
        return fd;
    }
    if (create) { DBG("Creating %s of size %zu", path, size); ftruncate(fd, size); }
    if (!ro) ifU (flock(fd, LOCK_EX) != 0) WRN("Failed to flock %s: %m", path);
    DBG("Bound %s of size %zu; create: %d, readonly: %d", path, size, create, ro);
    return fd;
}

/// NB: Nonreentrant
static COLD int /* fd */
open_pathed(const char label[max_path_component_len] = {'\0'}, const size_t size=0,
            const bool create=false, const bool readonly=true, const int dir_fd=-1) {
    DBG("opening: %.*2$s ;[%hhu] %s, %zu", current_path, current_path_len, label, size);
    ASSUME_VALID_current_path_len;
    /// "If create is true, readonly shouldn't be"
    ASSUME(!create ?: !readonly);

    const auto to_append = strlen(label);
    ASSUME(to_append <= max_path_component_len, to_append, "hhu");
    stpncpy(&current_path[current_path_len], label, to_append)[0] = '\0';
    current_path_len += to_append; ASSUME_VALID_current_path_len;

    const auto fd = open_file(current_path, size, create, readonly, dir_fd);
    clear_trailing_current_path(to_append);
    return fd;
}

/// NB: Nonreentrant
static COLD int /* ret from unlink */
unlink_pathed_file(const char label[max_path_component_len], const int dir_fd = -1) {
    DBG("unlinking: %.*2$s ;[%hhu] %s ; %d", current_path, current_path_len, label, dir_fd);
    ASSUME_VALID_current_path_len;

    const auto to_append = strlen(label);
    ASSUME(to_append <= max_path_component_len, to_append, "hhu");
    stpncpy(&current_path[current_path_len], label, to_append)[0] = '\0';
    current_path_len += to_append; ASSUME_VALID_current_path_len;

    const auto ret = (dir_fd == -1) ? unlink(current_path) : unlinkat(dir_fd, current_path, 0);
    ifU (ret != 0) WRN("Failed to unlink %s: %m", current_path);
    clear_trailing_current_path(to_append);
    return ret;
}

using uloff_t = unsigned loff_t;
static_assert(sizeof(size_t) <= sizeof(uloff_t));
static COLD NOINLINE bool
copy_file(int from_fd, int to_fd=-1, loff_t from_off=0, const size_t stopping_size=0) {
    ASSUME(from_fd != -1 && to_fd != -1);
    DBG("copy_file: %d (@ %ld) -> %d", from_fd, from_off, to_fd);
    loff_t off_data=0, off_out=from_off;
    const auto& fail_seek = [&](MAYBE_UNUSED unsigned line = __LINE__) {
        WRN("Failed seeking{:%u} while copying: [%d] @ %ld -> [%d] @ %ld: %m",
            line, from_fd, off_data, to_fd, off_out);
    };
    const bool from_blkdev = is_blkdev(from_fd);
    ASSUME(!from_blkdev || stopping_size); /// Iff from_blkdev then stopping_size
    auto&& copy = [&](auto&& copier) mutable -> bool {
        bool done = false; do {
            ASSUME(!stopping_size || off_data < (uloff_t)stopping_size);
            if (!from_blkdev) {
                /// Seek for data in potentially sparse file
                off_data = lseek(from_fd, off_out, SEEK_DATA);
                ifU (off_data < 0) {
                    ifL (errno == ENXIO) { /// EOF
                        ifU (stopping_size && off_out < (uloff_t)stopping_size) {
                            WRN("Reached EOF before stopping size: %zu", stopping_size);
                            return false;
                        } else break;
                    }
                    fail_seek(); return false;
                }
            } else off_data = off_out;
            done = stopping_size && off_data >= (uloff_t)stopping_size;
            ifU (done) off_data = stopping_size; /// Clamp the hole to our stopping size
            /// Pad the output with any sparse data
            ifL (off_data > off_out) { off_out = lseek(to_fd, off_data, SEEK_SET);
                ifU (off_out < 0) { fail_seek(); return false; }
            }
            ifU (done) break;

            auto off_hole = stopping_size;
            if (!from_blkdev) {
                /// Find the next hole in potentially sparse file
                off_hole = lseek(from_fd, off_data, SEEK_HOLE);
                ifU (off_hole < 0) { fail_seek(); return false; }
                /// Undo the seek we used to get the hole
                ifU (lseek(from_fd, off_out, SEEK_SET) < 0) { fail_seek(); return false; }
            }
            done = stopping_size && off_hole >= (uloff_t)stopping_size;
            ifU (done) off_hole = stopping_size; /// Clamp the data to our stopping size

            const auto amount = off_hole - off_data;
            ASSUME(amount); /// Because we break before here if we just padded a hole to the end
            const auto copied = copier(amount);
            ifU (copied < 0 && errno != EINTR) {
                WRN("Failed copying %zu: [%d] @ %ld -> [%d] @ %ld: %zd: %m",
                    amount, from_fd, off_data, to_fd, off_out, copied);
                return false;
            }
            ifU (copied < amount) {
                WRN("Undercopied: %zd/%zu@%zu", copied, amount, off_out); done = false;
            }
            ifL (stopping_size) {
                ifL (off_out == (uloff_t)stopping_size) done = true;
                else ifU (off_out > (uloff_t)stopping_size) {
                    ERR("Copied beyond stopping size: %zu/%zu", off_out, stopping_size);
                    return false;
                }
            }
        } while (!done);
        return true;
    };
    auto&& copied = copy([&](const auto size) mutable -> auto
                         { return copy_file_range(from_fd, nullptr, to_fd, &off_out, size, 0); });
    bool ret;
    ifU (!LIKELY(copied) && ((errno == EXDEV) || (errno == ENOSYS) || (errno == EINVAL))) {
        DBG("Failed copy_file_range, so using send_file");
        copied = copy([&](auto size) { return sendfile(to_fd, from_fd, &off_out, size); });
        ifU (!copied) {DBG("Failed sendfile as well...");} else ret = true;
    } else ret = true;
    close(to_fd); close(from_fd); return ret;
}

static NONNULL COLD bool
copy_file(int from_fd, const char *to, const size_t to_size=0, loff_t from_off=0) {
    /// We create the destination file
    const int to_fd = open_file(to, to_size, true, false, dest_dir_fd);
    ifU (to_fd == -1) { WRN("Failed to open %s: %m", to); return false; }
    else return copy_file(from_fd, to_fd, from_off, to_size);
}

static NONNULL COLD NOINLINE bool
copy_pathed_file(int from_fd, const char *to, const size_t to_size=0, loff_t from_off=0) {
    const auto to_len = strlen(to);
    ASSUME(to_len <= max_path_component_len, to_len, "lu");
    char to_buf[max_current_path_len] = {'\0'};
    strncpy(to_buf, current_path, current_path_len);
    stpncpy(&to_buf[current_path_len], to, to_len)[0] = '\0';
    return copy_file(from_fd, to_buf, to_size, from_off);
}

static COLD int /* ret from syscalls */
move_file(int _from_dir_fd=-1, const char from[max_current_path_len] = {'\0'},
          int   _to_dir_fd=-1, const char   to[max_current_path_len] = {'\0'}) {
    const auto from_len = strlen(from), to_len = strlen(to);
    ASSUME(from_len <= max_current_path_len, from_len, "lu");
    ASSUME(  to_len <= max_current_path_len,   to_len, "lu");
    auto from_dir_fd = (_from_dir_fd != -1) ? _from_dir_fd : AT_FDCWD
       ,   to_dir_fd =  ( _to_dir_fd != -1) ?   _to_dir_fd : AT_FDCWD;
    DBG("move_file: %d -> %d; %s -> %s", from_dir_fd, to_dir_fd, from, to);

    auto&& ret = renameat2(from_dir_fd, from, to_dir_fd, to, RENAME_NOREPLACE | RENAME_WHITEOUT);
    ifU (ret == -1) { DBG("Trying direct move instead");
        const auto from_fd = open_file(from, 0, false, true, from_dir_fd);
        ifU (from_fd == -1) {
            ERR("Failed opening to copy from: %s: %m", from);
            return errno;
        }
        const auto size = get_file_size(from_fd);
        ifU (!size) return -1; /// TODO: better error code

        const auto to_fd = open_file(to, size, true, false, to_dir_fd);
        ifU (to_fd == -1) {
            ERR("Failed opening to copy to: %s: %m", to);
            close(from_fd);
            return errno;
        }

        ifU (!copy_file(from_fd, to_fd)) {
            WRN("Failed while copying: %s -> %s: %m", from, to);
            ret = -1;
        } else ret = 0;
        ifL (ret == 0) ret = unlinkat(from_dir_fd, from, 0);
        /// NB: `close`s done by copy_file
    }
    return ret;
}

static COLD NOINLINE decltype(auto) /* ret from syscalls */
move_pathed_file(const char from[max_path_component_len],
                 const char to  [max_path_component_len]) {
    DBG("moving: %.*2$s ;[%hhu] \"%s\" -> \"%s\"", current_path, current_path_len, from, to);
    ASSUME_VALID_current_path_len;
    const auto from_len = strlen(from), to_len = strlen(to);
    ASSUME(from_len <= max_path_component_len, from_len, "lu");
    ASSUME(  to_len <= max_path_component_len,   to_len, "lu");

    char from_buf[max_current_path_len] = {'\0'}, to_buf[max_current_path_len] = {'\0'};
    strncpy(from_buf, current_path, current_path_len);
    stpncpy(&from_buf[current_path_len], from, from_len)[0] = '\0';
    strncpy(to_buf, current_path, current_path_len);
    stpncpy(&to_buf[current_path_len], to, to_len)[0] = '\0';

    const bool normal_rename = work_fd == -1 && dest_dir_fd == -1;
    auto ret = normal_rename ? rename(from_buf, to_buf)
             : move_file(work_fd, from_buf, dest_dir_fd, to_buf);
    ifU (ret != 0) {
        const auto&& bitch=[&]{ WRN("Failed to rename \"%s\" -> \"%s\": %m", from_buf, to_buf); };
        bitch(); ifL (normal_rename && ret == EXDEV) {
            ret = move_file(-1, from_buf, -1, to_buf);
            ifU (ret != 0) bitch();
        }
    }
    ASSUME_VALID_current_path_len;
    return ret;
}

/// Using the convention of `.filename` as incomplete and `filename` as complete
#define MAYBE_INCOMPLETE_FILE(incomplete, filename) ((incomplete) ? "." filename : filename)

static inline COLD decltype(auto) /* ret from move_pathed_file */
move_complete_pathed_file(const char incomplete_filename[max_path_component_len]) {
    ASSUME(incomplete_filename[0] == '.');
    return move_pathed_file(incomplete_filename, &incomplete_filename[1]);
}

static NONNULL COLD NOINLINE bool
discard_blkdev(const char *blkdev, size_t amount=0, const size_t off=0, bool zero=false) {
    char *cmd; const auto size = round_up(amount, blkdev_alignment);
discard:
    ifU (asprintf(&cmd, "blkdiscard -%sl %zu -o %zu %s",
                  zero ? "z" : "", size, off, blkdev) == -1 || !cmd) {
        ERR("Failed to asprintf discard command for %s (%zu@%zu): %m", blkdev, size, off);
        return false;
    }
    bool success = false; DBG("Invoking: %s", cmd);
    const auto ret = system(cmd); switch (ret) {
    case -1: WRN("Error invoking `%s` : %m", cmd); break;
    case  0: success = true; break;
    default: WRN("Command `%s` failed: %d", cmd, ret); break;
    }
    ifL (cmd) free(cmd); if (zero) { zero = false; goto discard; }; return LIKELY(success);
}

#ifdef ENABLE_INDEXING
static inline COLD bool
discard_work(const size_t amount=SIZE_MAX, const bool zero=false) {
    ASSUME(work_blkdev_off); const auto work = secure_getenv(WORK_ENV_VAR);
    ASSUME(work); ifU (!discard_blkdev(work, amount, *work_blkdev_off, zero)) {
    #define NOTE(VIA) \
        { VIA("Failed to discard %zu blocks from %s @ %zu", amount, work, *work_blkdev_off); }
        ifU (zero) NOTE(ERR)
        else NOTE(WRN)
    #undef NOTE
        return false;
    } else return true;
}
#endif // ENABLE_INDEXING

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// Indexing
////////////////////////////////////////////////////////////////////////////////////////////////////

using UntypedAtlasDesc = decltype(AtlasDesc::off); /// Just the offset

////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Persistent variable-collision Index of AtlasDescs, as a full-sized (max_Hashes) "radix" array of
 * progressively lesser resolution; i.e. it's able to represent progressively less higher-order
 * collisions
 *
 * Transformations and size class are implicit, through the FS structure
 *
 * NB: We do not protect this with ENABLE_VINDEX so that the macro definitions are still available
 * NMB: Consider this the icing before the cake ;)
**/
class VIndex final {
    #define VINDEX_NAME "vndex"
    STATIC_ASSERT_VALID_FILENAME(VINDEX_NAME)
    /// Leveraging this sentinel leaves us unable to distinguish the 0th offset entry; this is deemed
    /// to be a worthwhile sacrifice in the name of simplifying logic and not having to explicitly
    /// encode for emptiness, further wasting memory at this scale
    static consteval UntypedAtlasDesc empty_desc_entry = 0u;
    #define DIRECT_ENTRIES 4
public:
    /// NOTE: The empty state is indistinguishable from the 0th offset; but that's OK, because in the
    /// unlikely event we're trying to encode 0, it's still OK to be lossy (as charting checks)
    /// NOTE: If changing the number of direct entries, one must also change code further below
    using DirectEntry = condst std::array<condst UntypedAtlasDesc, DIRECT_ENTRIES>;
    static consteval uint16_t direct_storage = /// In bits
        sizeof(HiFittedUInt<sizeof(DirectEntry) / sizeof(UntypedAtlasDesc)>) * __CHAR_BIT__;
    static consteval uint8_t direct_bytes = direct_storage / sizeof(std::byte);
    static_assert(divides(sizeof(UntypedAtlasDesc), sizeof(DirectEntry)));

    /// NB: These have to be `#define`s because we need the numerals in struct bitfield declarations
    #define APNDX_BITS BPNDX_BITS
    #define BPNDX_BITS 24/*28*//*24*/
    #define CPNDX_BITS 22/*18*//*22*/
    #define DPNDX_BITS 19/*12*//*18*/
    #define EPNDX_BITS 16/*6*//*12*/
    #define FPNDX_BITS 13/*2*//*8*/
    #define GPNDX_BITS 9
    #define HPNDX_BITS 6
    #define IPNDX_BITS 3
    #define JPNDX_BITS 1
    //#define KPNDX_BITS 2
    //#define LPNDX_BITS 1
    //#define MPNDX_BITS 3
    //#define NPNDX_BITS 1
    //#define OPNDX_BITS 1

    /* ...
    _(18, O, o) \
    _(17, N, n) \
    _(16, M, m) \
    _(15, L, l) \
    _(14, K, k) \ */
#define APPLY_APNDXS(_) \
    _(13, J, j) \
    _(12, I, i) \
    _(11, H, h) \
    _(10, G, g) \
    _(9,  F, f) \
    _(8,  E, e) \
    _(7,  D, d) \
    _(6,  C, c) \
    _(5,  B, b)
    /// NOTE: For display only. This wouldn't be a valid value; 'A' is reserved.
    /*_(DIRECT_ENTRIES,  A, a)*/

#define DEF_IOffset(N, A, _a) \
    using I##N##Offset = HiFittedUInt<A##PNDX_BITS>;
    APPLY_APNDXS(DEF_IOffset)
#undef DEF_IOffset
    using I4Offset = I5Offset;
    ///using I3Offset = I4Offset;
    //using I2Offset = I3Offset;

#define DEF_MAX_APNDX_ENTRIES(N, A, a) \
    static consteval I##N##Offset max_##a##ppendix_entries = (1 << A##PNDX_BITS);
    APPLY_APNDXS(DEF_MAX_APNDX_ENTRIES)
#undef DEF_MAX_APNDX_ENTRIES
    static consteval auto max_appendix_entries = max_bppendix_entries;

    /// We assert that the input will be SO random that there will be no more than 2**APNDX_BITS
    /// collisions for all N corresponding to all A as in APPLY_APNDXS
    struct IndirectEntry final {
    friend class VIndex;
        static consteval uint8_t indirects_start = DIRECT_ENTRIES + 1
                               , max_indirects = DIRECT_ENTRIES*3u + 1;
    private:
        #define _INDIRECTS_BITS DIRECT_ENTRIES
        /// `indirects_start` (5) less than the number of indirects
        condst uint8_t _indirects : _INDIRECTS_BITS = 0;
        /// NB: While this is subsumed by the `sizeof Direct >= sizeof Indirect` check below, this
        /// makes the intent more explicit
        static_assert(_INDIRECTS_BITS /* + */
        #define ADD_BITS(_N, A, _a) \
            + A##PNDX_BITS
            APPLY_APNDXS(ADD_BITS)
        #undef ADD_BITS
            <= (sizeof(DirectEntry) * __CHAR_BIT__));

    #ifdef ENABLE_INDEXING
        inline void add_indirect() &
        { ASSUME(_indirects < max_indirects - indirects_start); _indirects++; }
    #endif
    public:
        inline constexpr PURE uint8_t
        indirects() const& { return _indirects + indirects_start; }

    #define DECL_APNDX_ENTRY(N, A, _a) \
        condst I##N##Offset _##N : A##PNDX_BITS = 0;
        APPLY_APNDXS(DECL_APNDX_ENTRY)
    #undef DECL_APNDX_ENTRY
    } PACKED;

    static_assert(sizeof(DirectEntry) >= sizeof(IndirectEntry));
    union _Entry { DirectEntry d; IndirectEntry i; };
    using Entry = condst
    #ifndef EMBARALLEL
        _Entry;
    #else
        std::atomic<condst _Entry>;
    #endif

    // Sorted in increasing Hash order
    using Entries = condst std::array<Entry, max_Hashes>;
    // Whether the entry is indirect; by the assertion above, this should be an EXTREMELY sparse set
    static consteval size_t typedex_size = max_Hashes;
    using Bitset =
    #ifndef EMBARALLEL
        std::bitset<typedex_size>;
    #else
        // HACK: AtomicBitSet speaks in terms of absolute size in its block size (@_@)
        AtomicBitSet<(typedex_size / std::numeric_limits<unsigned long>::digits)>;
    #endif
    using Typedex = condst Bitset;
    /// NOTE: It's OK for Appendix entries not to technically be threadsafe, as their counters are.
#define DEF_APNDX_TYPE(_N, A, a) \
    using A##ppendix = condst std::array<condst UntypedAtlasDesc, max_##a##ppendix_entries>;
    APPLY_APNDXS(DEF_APNDX_TYPE)
#undef DEF_APNDX_TYPE
    using Appendix = condst std::array<condst DirectEntry, max_appendix_entries>;

    #define RECOVERY_NAME "rcvry"
    STATIC_ASSERT_VALID_FILENAME(RECOVERY_NAME)
    /// This is used during indexing to keep track of progress and resume if need be
    /// NOTE: This is machine-dependent since it uses the HiFittedUInt IOffsets
    struct Recovery final {
    #ifndef EMBARALLEL
        // NB: Only used when writing the index so we always start with 0
        volatile condst Offset latest_offset = 0;
    # define DECL_APNDX_ENTRY(N, _, a) \
        volatile I##N##Offset a##ppendix_entries = 0;
    #else // EMBARALLEL
        using SeenOffsets = condst AtomicBitSet<max_Offsets>;
        SeenOffsets seen_offsets;
    # define DECL_APNDX_ENTRY(N, _, a) \
        std::atomic<condst I##N##Offset> a##ppendix_entries = 0;
    #endif // EMBARALLEL

        APPLY_APNDXS(DECL_APNDX_ENTRY)
    #undef DECL_APNDX_ENTRY
        // Notably, there's no "a"ppendix tracking because it's the same as bppendix
    } PACKED;

    static consteval size_t flat_size = sizeof(Typedex) + sizeof(Entries)
                                    #ifdef ENABLE_INDEXING
                                        + sizeof(Recovery)
                                    #endif
                                    #define ADD_SIZE(_N, A, _a) \
                                            + sizeof(A##ppendix)
                                        APPLY_APNDXS(ADD_SIZE)
                                    #undef ADD_SIZE
                                        + sizeof(Appendix);
private:
    #define TYPEDEX_NAME "typdx"
    STATIC_ASSERT_VALID_FILENAME(TYPEDEX_NAME)
    condst Typedex &NO_ALIAS typedex;
    condst Entries &NO_ALIAS entries;

#define DECL_APNDX_ENTRY(_N, A, a) \
    condst A##ppendix &NO_ALIAS a##ppendix;
    APPLY_APNDXS(DECL_APNDX_ENTRY)
#undef DECL_APNDX_ENTRY
    condst Appendix &NO_ALIAS appendix;
#define APPENDIX_SUFFIX "pndx"
#define APPENDIX_NAME "a" APPENDIX_SUFFIX
STATIC_ASSERT_VALID_FILENAME(APPENDIX_NAME)

#ifdef ENABLE_INDEXING
    /// NOTE: Can be null iff it's for read-only use
    Recovery *const NO_ALIAS recovery;
#endif

#ifdef ENABLE_INDEXING
# define fd_importer_ro_default false
#else
# define fd_importer_ro_default true
#endif

#define DECL_FD_IMPORTER(field) \
    static inline COLD auto     \
    fd_to_##field(const int fd, condst bool readonly = fd_importer_ro_default)

#define _DEF_FD_IMPORTER(field, Type) DECL_FD_IMPORTER(field) {         \
    using T = to_mmappable<Type>;                                       \
    condst auto&& _ = mmap_dyn<condst T>(fd, readonly); ASSUME(_);      \
    if constexpr (!mmappable<Type>) return /*&*/_; else return FWD(*_); \
}

#define DEF_FD_IMPORTER(field) _DEF_FD_IMPORTER(field, decltype(field))
    DEF_FD_IMPORTER(entries)
    DEF_FD_IMPORTER(typedex)

#define DEF_APNDX_IMPORTER(_N, _A, a) DEF_FD_IMPORTER(a##ppendix)
    APPLY_APNDXS(DEF_APNDX_IMPORTER)
#undef DEF_APNDX_IMPORTER
    DEF_FD_IMPORTER(appendix)

#ifdef ENABLE_INDEXING
    DEF_FD_IMPORTER(recovery)
#endif

public:
    inline constexpr COLD
    VIndex(condst Typedex &&NO_ALIAS typedex, condst Entries &&NO_ALIAS entries
    #ifdef ENABLE_INDEXING
         , Recovery *const NO_ALIAS recovery
    #endif
    #define LIST_APNDX_ENTRY(_N, A, a) \
         , condst A##ppendix &&NO_ALIAS a##ppendix
           APPLY_APNDXS(LIST_APNDX_ENTRY)
    #undef LIST_APNDX_ENTRY
         , condst Appendix &&NO_ALIAS appendix)
    : typedex(typedex), entries(entries)
#define LIST_APNDX_ENTRY(_N, _A, a) \
    , a##ppendix(a##ppendix)
    APPLY_APNDXS(LIST_APNDX_ENTRY)
#undef LIST_APNDX_ENTRY
    , appendix(appendix)
#ifdef ENABLE_INDEXING
    , recovery(recovery)
#endif
    {}

    COLD
    VIndex(int typedex_fd, int entries_fd
    #ifdef ENABLE_INDEXING
         , int recovery_fd
    #endif
    #define LIST_APNDX_ENTRY(_N, _A, a) \
         , int a##ppendix_fd
         APPLY_APNDXS(LIST_APNDX_ENTRY)
    #undef LIST_APNDX_ENTRY
         , int appendix_fd
         , bool readonly = fd_importer_ro_default)
    : VIndex(FWD(fd_to_typedex(typedex_fd, readonly))
           , FWD(fd_to_entries(entries_fd, readonly))
    #ifdef ENABLE_INDEXING
        #define maybe_recovery_from_fd               \
             ((recovery_fd != -1)                    \
             ? fd_to_recovery(recovery_fd, readonly) \
             : ({ ASSUME(readonly); nullptr; }))
           , maybe_recovery_from_fd
    #endif
    #define IMPORT_APNDX_ENTRY(_N, _A, a) \
           , FWD(fd_to_##a##ppendix(a##ppendix_fd, readonly))
           APPLY_APNDXS(IMPORT_APNDX_ENTRY)
    #undef IMPORT_APNDX_ENTRY
           , FWD(fd_to_appendix(appendix_fd, readonly)) )
    {}

    COLD
    ~VIndex() {
        DBG("Destroying VIndex: %p", this);
    #define _munmap(field) \
        munmap(field, sizeof(std::remove_cvref_t<decltype(*field)>));
    #define munmap_(field) \
        munmap(&field, sizeof(std::remove_cvref_t<decltype(field)>));
    #define munmap_APNDX(_N, _A, a) \
        munmap_(a##ppendix);
        APPLY_APNDXS(munmap_APNDX)
    #undef munmap_APNDX
        munmap_(appendix);
        munmap_(entries);
        munmap_(typedex);
    #ifdef ENABLE_INDEXING
        ifL (recovery) _munmap(recovery);
    #endif
    }

    ONLY_DEFAULT_MOVEABLE(VIndex)

    inline PURE bool is_direct(const Hash hash) const& { return !typedex[hash]; }
#if defined ENABLE_INDEXING && ! defined EMBARALLEL
    inline PURE Offset recover_offset() const& { return recovery->latest_offset; }
#endif

    #define _mark(_field, field_size) {                     \
        using Field = std::remove_cvref_t<decltype(_field)>;\
        auto& field = const_cast<condst Field&>(_field);    \
        msync(&field, field_size, MS_SYNC);                 \
        madvise(&field, field_size, MADV_COLD);             \
    }
    #define mark(field) _mark(field, sizeof(std::remove_cvref_t<decltype(field)>))

#ifdef ENABLE_INDEXING
    #define __mark_complete(filename) move_complete_pathed_file("." filename)
    /// HACK: Assumes recovery is at the beginning of any blkdev mapping
    #define _mark_complete(field, field_size, filename) (work_blkdev_off                        \
        ? copy_pathed_file(work_fd, filename, field_size,                                       \
                           abs_diff((uintptr_t)&field, (uintptr_t)recovery) + *work_blkdev_off) \
          ? 0 : -1                                                                              \
        : __mark_complete(filename))
    #define mark_complete(field, filename) \
        _mark_complete(field, sizeof(std::remove_reference_t<decltype(field)>), filename)
#endif

    inline PURE const Entry& find_entry(Hash hash) const& { return entries[hash]; }
#ifdef ENABLE_INDEXING
    inline PURE       Entry& find_entry(Hash hash)      & { return entries[hash]; }
#endif

    COLD bool
    mark_done() condst& {
    #define mark_APNDX(_N, _A, a) \
        mark(a##ppendix);
        APPLY_APNDXS(mark_APNDX)
    #undef mark_APNDX
        mark(appendix);
        mark(entries);
        mark(typedex);
    #ifdef ENABLE_INDEXING
    # define mark_complete_APNDX(_N, _A, a) \
        mark_complete(a##ppendix, #a APPENDIX_SUFFIX);
        APPLY_APNDXS(mark_complete_APNDX)
    # undef mark_complete_APNDX
        ifU (mark_complete(appendix, APPENDIX_NAME) != 0) return false;
        ifU (mark_complete(entries ,   VINDEX_NAME) != 0) return false;
        ifU (mark_complete(typedex ,  TYPEDEX_NAME) != 0) return false;
        ifL (recovery) {
            mark(*recovery);
            if (!work_blkdev_off && UNLIKELY(unlink_pathed_file(RECOVERY_NAME) != 0)) return false;
            if (work_blkdev_off) return discard_work(flat_size);
        } else ASSUME(!work_blkdev_off); /// We REQUIRE recovery for work_blkdev_off due to our HACK
    #endif // ENABLE_INDEXING
        return true;
    }

protected:
#ifdef ENABLE_INDEXING
    inline void
    set_recovery(UntypedAtlasDesc desc) & { ASSUME(recovery);
    #ifndef EMBARALLEL
        recovery->latest_offset = desc;
    #else
        recovery->seen_offsets.set(desc);
    #endif
    }

    /// NOTE: `which` is 0-indexed
    /// Returns how many entries there now are, and 0 on failure
    template<uint8_t which = DIRECT_ENTRIES-1>
    static constexpr inline std::enable_if_t<(which < DIRECT_ENTRIES)
    , uint8_t> record_direct(DirectEntry& existing, const UntypedAtlasDesc&& desc) {
        // We actually access forward, finding the first free slot
        constexpr auto at = DIRECT_ENTRIES-1-which;
        ASSUME(at < DIRECT_ENTRIES);
        if (auto& d = std::get<at>(existing)
          ; LIKELY(d == empty_desc_entry)) {
            d = desc;
            return at+1;
        }
        else if constexpr(which > 0) return record_direct<which-1>(FWD(existing), FWD(desc));
        else return 0;
    }

private:
    /// Returns how many entries share this hash now (0 on failure)
    HOT uint8_t
    insert_entry(const Hash hash, const UntypedAtlasDesc&& desc) & { ASSUME(recovery);
    #ifdef EMBARALLEL
        ifU (recovery->seen_offsets.test(desc)) return 0;
    #endif
        auto& existing = find_entry(hash);
        uint8_t entries = 0;
        ifL (is_direct(hash)) {
            if (const auto record_res = record_direct(existing.d, FWD(desc))
              ; LIKELY(record_res != 0))
                entries = record_res;
            else
        #ifdef EMBARALLEL
            // Since making a new indirect isn't threadsafe...
            #pragma omp single
        #endif
            {
                DBG("Having to make a new indirect");
                ASSUME(recovery->bppendix_entries < max_bppendix_entries);
                const auto ioff = recovery->bppendix_entries++;
                appendix[ioff] = existing.d;
                bppendix[ioff] = desc;
                /** We don't need to touch up to here anymore during indexing **/
                //madvise(&appendix, ioff * sizeof(desc), MADV_COLD);
                //madvise(&bppendix, ioff * sizeof(desc), MADV_COLD);
                existing.i._indirects = 0;
                existing.i._5 = ioff; // NOTE: Must be changed whenever adjusting tiering
                typedex.set(hash); // Must be done last, to expose
                entries = existing.i.indirects();
            }
        } else
        /// Append to an indirect
    #ifdef EMBARALLEL
        /// Since indirects deal entirely in bitfields and we can't deal with those atomically...
        #pragma omp single
    #endif
        {
            const auto indirects = EXPECT(existing.i.indirects(), IndirectEntry::indirects_start);
            ASSUME(indirects < IndirectEntry::max_indirects, indirects, "u");
            ASSUME(indirects >= IndirectEntry::indirects_start, indirects, "u");
            /// NOTE: Bppendix can't happen, even though we generate a B-case below
            switch (indirects+1) {
        #define CASE(N, A, a)                                                    \
            case N: {                                                            \
                ASSUME(recovery->a##ppendix_entries < max_##a##ppendix_entries); \
                I##N##Offset ioff = recovery->a##ppendix_entries++;              \
                DBG(#A "ppending @ %zu", (size_t)ioff);                          \
                a##ppendix[ioff] = desc;                                         \
                /* We don't need to touch up to here anymore during indexing */  \
                /*madvise(&a##ppendix, ioff * sizeof(desc), MADV_COLD);*/    \
                existing.i._##N = ioff;                                          \
                existing.i.add_indirect();                                       \
                entries = EXPECT(existing.i.indirects(), indirects+1);           \
                break;                                                           \
            }
            APPLY_APNDXS(CASE)
        #undef CASE
            default: UNREACHABLE;
            }
        }
        /// We shouldn't need to touch this bucket again in a while
        //maybe_madvise(&existing, sizeof(Entry), MADV_COLD);
        set_recovery(desc);
        return entries;
    }
#endif // ENABLE_INDEXING

private:
    /// NOTE: `which` is 0-indexed
    template<typename Predicate, uint8_t which = DIRECT_ENTRIES-1>
    static inline PURE std::enable_if_t<(which < DIRECT_ENTRIES)
    , std::optional<Offset>>
    try_direct(const DirectEntry& existing, condst Predicate&& pred) {
        if (const auto off = std::get<which>(existing)
          ; LIKELY(off != empty_desc_entry) && pred(off))
            return off;
        else if constexpr(which > 0)
            return try_direct<Predicate, which-1>(FWD(existing), FWD(pred));
        else return std::nullopt;
    }

public:
#ifdef ENABLE_INDEXING
    /// Returns whether successful
    template<Direction D = Direction::F, Negation N = Negation::I>
    inline NONNULL HOT bool
    update_index(const WordAtlasSlice& blk, const size_t size, const UntypedAtlasDesc&& desc) & {
        ASSUME_SZ_SIZE(size);
        const Hash hash = _hash_blk<D,N>(blk, size);
        const auto entries = insert_entry(hash, FWD(desc));
        ASSUME(entries < IndirectEntry::max_indirects);
    #define NOTE(M, pfx) \
        M(pfx ": %p; %c, %c, %lu \t; %.8x ->(%hhu)> %u", \
          blk, dir_chr(D), neg_chr(N), size, hash, entries, desc)
    #ifndef TERSE
        NOTE(DBG, "Indexing");
    #else
        ifU (entries > 1)
            NOTE(WRN, "Collision");
    #endif // !TERSE
    #undef NOTE
        /// NB: currently we just skip errors, e.g. having seen the offset already in EMBARALLEL
        return true;
        //return entries > 0;
    }
#endif // ENABLE_INDEXING

    template<typename Predicate>
    PURE std::optional<Offset>
    first_satisfying_offset(const Hash hash, condst Predicate&& pred) const& {
        const auto& entry = find_entry(hash);
        ifL (is_direct(hash)) {
            if (const auto off = try_direct(entry.d, FWD(pred))) return off;
            /// NB: intentionally falls through to final return
        } else {
            /// Check the original entries first
            const auto ioff = entry.i._5; /// NOTE: Must be changed whenever adjusting tiering
            if (const auto off = try_direct(appendix[ioff], FWD(pred))) return off;
            // Whelp, time to check the indirect entries
            const auto indirects = EXPECT(entry.i.indirects(), IndirectEntry::indirects_start);
            ASSUME(indirects <= IndirectEntry::max_indirects);
            switch(indirects) {
            /// NOTE: Intentionally first, so that we can fallthrough to final return
	    default: UNREACHABLE;
            /// NB: We want to fall through every indirect we have, ending in failure
        #define CASE(N, _A, a)                    \
            case N: {                             \
                I##N##Offset ioff = entry.i._##N; \
                Offset off = a##ppendix[ioff];    \
                if (pred(off)) return off;        \
            [[fallthrough]];                      \
            }
            APPLY_APNDXS(CASE)
        #undef CASE
            }
        }
        return std::nullopt;
    }

#ifndef NDEBUG
    template<Direction D, Negation N>
    PURE bool
    validate(const ByteAtlas& atlas, const SZ sz) const& {
        static_assert(sizeof(uint64_t) > sizeof(Hash));
        const auto size = size_for_sz(sz);
        for (uint64_t h=0; h<=max_Hash; h++)
            if (first_satisfying_offset(h, [=](const Offset _off) -> std::optional<Offset> {
                const auto off = offset_for_sz(sz, _off);
                const auto h_ = _hash_blk<D,N>((WordAtlasSlice)&atlas[off], size);
                ifU (h != h_) {
                    WRN("Failed validation @ %zu(%u); %c/%c;%u: %x != %x",
                        off, _off, dir_chr(D), neg_chr(N), size, h, h_);
                    return off;
                } else return std::nullopt;
            })) return false;
        return true;
    }
#endif // !NDEBUG
#undef DIRECT_ENTRIES
};

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// ...
// So the above allows for a little "give"/variability in our collisions. Instead, we'd prefer to be
// ABSOLUTELY exact in cases where we know the atlas ahead-of-time...

#if defined ENABLE_VINDEX && defined ENABLE_ALMANAC
# warning "Why bother enabling support for VIndices if you could just use an Almanac...?"
#endif

#if ! defined ENABLE_ALMANAC && defined SINGULAR_ATLAS
# warning "If you're using a SINGULAR_ATLAS, you should consider making and enabling ENABLE_ALMANAC"
#endif

#if defined ENABLE_ALMANAC && ! defined SINGULAR_ATLAS && ! defined SINGULAR_ATLAS_SIZE
# warning "Almanacs are inherently atlas-specific; it's dangerous not to use SINGULAR_ATLAS{,_SIZE}"
#endif

#if defined(ENABLE_ALMANAC) || defined(ENABLE_COUNTING)
// A HIGHLY specialized accounting of all buckets seen in our indexing of the atlas, perfectly sized
// and contiguous, eliding holes. It's the skeleton of a perfect hash (table).
struct Almanac final {
friend class AIndex;
friend class CIndex;
    #define ALMANAC_NAME "almnc"
    STATIC_ASSERT_VALID_FILENAME(ALMANAC_NAME)

    struct _Stat final {
    friend Almanac;
        Hash hash; /// Starting hash of this Stat block
    private:
        uint16_t offset_diff; /// Cumulative offset in Stats, expressed as a difference with hash
        bool offset_diff_sign : 1; /// Whether the difference is negative (hash > offset)
    public:
        uint8_t count : 7; /// The count of hash buckets this Stat block represents
        /// len is just (next_offset - offset) / count

    protected:
        static_assert(sizeof(decltype(offset_diff)) < sizeof(Offset));
        static consteval auto
            max_offset_diff = std::numeric_limits<decltype(offset_diff)>::max();

        static consteval decltype(offset_diff)
        diff_offset(const Hash hash, const Offset offset) {
            Offset d = (hash > offset) ? (hash-offset) : (offset-hash);
        #ifndef NDEBUG
            ifU (d > max_offset_diff)
                WRN("Failed to encode offset_diff: %u = |%u - %u|", d, hash, offset);
        #else
            ASSUME(d <= max_offset_diff, d, "u");
        #endif
            return d;
        }

    public:
        inline constexpr
        _Stat(Hash hash, Offset offset, uint8_t count)
        : hash(hash)
        , offset_diff(diff_offset(hash, offset))
        , offset_diff_sign(hash > offset)
        , count(count)
        {}

        static consteval uint8_t max_count = 1 << 4
                               , max_len   = 1 << 5;

        consteval HOT Offset offset() condst&
        { return hash + (offset_diff * (offset_diff_sign ? -1 : 1)); }
    } PACKED;
    using Stat = condst _Stat;

    // TODO: SINGULAR_ALMANAC_SIZE
    using Stats = condst span<Stat>;
    /// NB: This is only used for verification purposes in AIndex
    using Zeroes = condst std::bitset<max_Hashes>;
    #define ZEROES_NAME "zeros"
    STATIC_ASSERT_VALID_FILENAME(ZEROES_NAME)
    using ID = Offset;

    // NB: -1 because it'd be a pretty damn useless almanac if it didn't "chart" any, besides
    // ensuring that we can always use the last value as an anchor
    static consteval Offset max_stats_elems = max_Offset-1;

private:
    condst Stats stats;
    condst Zeroes &NO_ALIAS zeroes;

    inline COLD
    Almanac(condst Stats &&NO_ALIAS stats, condst Zeroes &&NO_ALIAS zeroes)
    : stats(FWD(stats)), zeroes(zeroes) { }

    // Custom implementation instead of DEF_FD_IMPORTER
    DECL_FD_IMPORTER(stats) {
        const auto size = get_file_size(fd);
        ASSUME(divides(sizeof(Stat), size), size, "zu");
        const auto elems = size/sizeof(Stat);
        ASSUME(elems > 1);
        DBG("mmapping stats of size %lu (%lu)", size, elems);
        condst auto&& stats = Stats((condst _Stat*)mmap_dyn<Stat>(fd, readonly, size, MADV_RANDOM), elems);
        ASSUME(stats.size() == elems && 1 < elems && elems < max_stats_elems, stats.size(), "ld");
        return FWD(stats);
    }
    DEF_FD_IMPORTER(zeroes)

    /// The last value is just an anchor
    inline PURE ID anchor_id() const
    { ASSUME(stats.size() < max_stats_elems, stats.size(), "zu"); return stats.size()-1; }

    inline PURE bool is_valid (const ID id) const { return id < stats.size(); }
    inline PURE bool is_anchor(const ID id) const { ASSUME(is_valid(id)); return id == anchor_id(); }
    inline PURE bool is_real  (const ID id) const { ASSUME(is_valid(id)); return id < anchor_id(); }

protected:
    // This accounts for the anchor final value
    inline PURE size_t stats_size() const { return anchor_id(); }

    inline PURE       Stat& stat(const ID id)      & { ASSUME(is_valid(id)); return stats[id]; }
    inline PURE const Stat& stat(const ID id) const& { return (const Stat&)(((Almanac&)*this).stat(id)); }

    inline PURE Offset
    num_0s_before(const ID id) const& { ASSUME(is_valid(id)); return stat(id).hash - id; }

    inline PURE Offset
    num_0s_trailing(const ID id) const&
    { ASSUME(is_real(id)); return num_0s_before(id+1) - num_0s_before(id); }

    /// Count (number of collisions / size of bucket) of given run
    inline PURE uint8_t
    count(const ID id) const& {
        ASSUME(is_real(id));
        const auto count = stat(id).count;
        ASSUME(count < Stat::max_count);
        return count+1;
    }

    /// Length of given run, in buckets/hashes
    inline PURE Offset
    len(const ID id) const& {
        ASSUME(is_real(id));
        const auto count_ = count(id);
        const Offset off_diff = stat(id+1).offset() - stat(id).offset();
        ASSUME(divides(count_, off_diff));
        const uint8_t len = off_diff / count_;
        ASSUME(0 < len && len <= Stat::max_len, len, "hhu");
    #if 0
    //#ifndef NDEBUG
        const auto h_0 = stat(id).hash, h_1 = stat(id+1).hash;
        const auto t = num_0s_trailing(id);
        const auto _len = h_1 - h_0 - t;
        ifU (_len != len) {
            WRN("Len mismatch @ %u!\t %hhu != %hhu (%u, %u, %u)", id, len, _len, h_0, h_1, t);
            return 0;
        }
    #endif
        return len;
    }

    inline PURE bool is_zero(const Hash hash) const& { return zeroes.test(hash); }

    PURE HOT ID
    _find_id(const Hash hash) const& {
        ASSUME(!is_zero(hash));
        //DBG("_Finding ID for: %.8x", hash);
        /* Binary search over the offsets of each value; our `n` is SO large, though, that
           the alternative Bottenbruch approach is more appropriate. */
        ID l=0, r=anchor_id();
    #ifdef DEBUG
        Offset iterations = 0;
    #endif
    #if 1
        while(l < r) {
        #ifdef DEBUG
            iterations++;
        #endif
            ASSUME(is_real(l)); ASSUME(is_valid(r));
            //debuggable const auto m = std::midpoint(l,r);
            /* We're careful to avoid overflow, so we don't use a naive `(l+r)/2` */
            debuggable const auto _m= r-l;
            /* We emulate a ceiling so we don't have to use floats */
            debuggable const uint_fast64_t m = l + (_m/2)+(_m%2);
            static_assert(sizeof(m) > sizeof(_m)); // Protect overflow in case compiler is too smart
            ASSUME(is_valid(m), m, "lu");
            //DBG("%.16X -> %.8x @ %u", m, stat(m).hash, iterations);
            if (hash < stat(m).hash) r=m-1;
            else l=m;
        }
    #endif
    // "Normal" binary search; inferior for our case
    #if 0
        while(l <= r) {
        #ifdef DEBUG
            iterations++;
        #endif
            const auto m = (r-l)/2 + l;
            DBG("%.16X -> %.8x @ %u", m, stat(m).hash, iterations);
            if (stat(m).hash < hash) l = m+1;
            else if (stat(m).hash > hash) r = m-1;
            else { l=m; break; }
        }
    #endif
    // Even interpolation search is inferior, though by less
    #if 0
        while(l != r && hash >= stat(l).hash && hash <= stat(r).hash) {
        #ifdef DEBUG
            iterations++;
        #endif
            const auto m = l + ((hash - stat(l).hash) * (r-l) / (stat(r).hash - stat(l).hash));
            DBG("%.16X -> %.8x @ %u", m, stat(m).hash, iterations);
            if (stat(m).hash < hash) l = m+1;
            else if (stat(m).hash > hash) r = m-1;
            else { l=m; r=m; break; }
        }
    #endif
        // We should NEVER fail to find the value
        ASSUME(l == r);
//         DBG_(l, ".8x");
        ASSUME(is_real(l));
//         DBG_(stat(l).hash, ".8x");
//         DBG_(stat(l).offset, "u");
        ASSUME(hash >= stat(l).hash, hash, ".8x");
        //DBG_(stat(l+1).hash, ".8x");
        //DBG_(stat(l+1).offset, "u");
        ASSUME(hash < stat(l).hash+count(l), stat(l).hash+count(l), ".8x");
        // TODO: May not be true if the last hash is the last bucket; then anchor hash would overflow
        ASSUME(hash < stat(l+1).hash, stat(l+1).hash, ".8x");
        return l;
    }

    inline PURE std::optional<ID>
    find_id(const Hash hash) const& {
        DBG("Finding ID for: %.8x", hash);
        ifU (is_zero(hash)) {
            DBG("It's a zero");
            return std::nullopt;
        } else {
            const ID id = _find_id(hash);
            DBG("It's %.8x", id);
            return id;
        }
    }

#ifdef ENABLE_INDEXING
    // NB: Currently unused
    COLD bool
    mark_done() condst& {
        _mark(stats, stats.size_bytes());
        mark(zeroes);
        if (!work_blkdev_off && UNLIKELY(__mark_complete(ALMANAC_NAME) != 0)) return false;
        ifU (__mark_complete(ZEROES_NAME) != 0) return false;
        return true;
    }
#endif

public:
    COLD
    Almanac(int stats_fd, int zeroes_fd, const bool readonly = fd_importer_ro_default)
    : Almanac(FWD(fd_to_stats(stats_fd, readonly)), FWD(fd_to_zeroes(zeroes_fd, readonly))) {}

    COLD
    ~Almanac() {
        DBG("Destroying Almanac: %p", this);
        munmap(stats.data(), stats.size_bytes());
        munmap_(zeroes);
    }

    ONLY_DEFAULT_MOVEABLE(Almanac)

#ifndef NDEBUG
    PURE COLD bool
    verify() const& {
        for (Offset i=0; i<stats_size(); i++) {
            constexpr auto gap_limit = std::numeric_limits<uint8_t>::max();
            constexpr auto idx_limit = Stat::max_offset_diff;

            const auto h = stat(i).hash, h_ = stat(i+1).hash;
            const auto o = stat(i).offset(), o_ = stat(i+1).offset();
            auto&& check_vs_idx = [=](Offset val, const char label) -> bool {
            #define CHECK(expr) {                                                 \
                const uint64_t e = (expr);                                        \
                const int64_t v = (int64_t)val - e;                               \
                ifU (v && (v > 0 ? v : -v) > idx_limit) {                         \
                    WRN("Idx %c_limit reached @ %.8x:\t%ld = %u - %lu[" #expr"]", \
                        label, i, v, val, e);                                     \
                    return false;                                                 \
                }}
                CHECK(h);
                //CHECK(o);
                //CHECK(i);
                //CHECK((uint64_t)o*(uint64_t)o);
                //CHECK((uint64_t)i*(i/256 ?: 1));
                //CHECK((uint64_t)i*(uint64_t)i);
                return true;
            #undef CHECK
            };

            ifU (h_ - h > gap_limit) {
                WRN("Hash gap too large! %u - %u = %u", h_, h, h_-h);
                return false;
            }
            ifU (!check_vs_idx(h, 'h')) return false;

            ifU (o_ - o > gap_limit) {
                WRN("Offset gap too large! %u - %u = %u", o_, o, o_-o);
                return false;
            }
            ifU (!check_vs_idx(o, 'o')) return false;

            //ifU (!len(i)) return false;
        }
        return true;
    }
#endif
};
#endif // ENABLE_ALMANAC | ENABLE_COUNTING

////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef ENABLE_ALMANAC
#include "nibset.h++"

/// A perfectly-collided/arranged, Almanac-using Index
class AIndex final {
    #define AINDEX_NAME "andex"
    STATIC_ASSERT_VALID_FILENAME(AINDEX_NAME)
public:
    using ID = Almanac::ID;
    using Entry =
    #ifndef EMBARALLEL
        UntypedAtlasDesc;
    #else
        std::atomic<condst UntypedAtlasDesc>;
    #endif // !EMBARALLEL
    using Entries = condst std::array<Entry, max_Hashes>;

#ifdef ENABLE_INDEXING
    /// NB: While EMBARALLEL is technically unsafe with this, we rely on the statistical
    /// improbability of two threads simultaneously changing buckets sharing a byte outside of the
    /// case that they touch literally the same bucket, which is slightly more probable, and thus we
    /// cover the case by using an atomic Storage in Nibset
    using RunningCounts = Nibset<max_Hashes, Almanac::Stat::max_count-1>;

    /// This is used during indexing to keep track of progress and resume if need be
    struct Recovery {
    #ifndef EMBARALLEL
        volatile Offset latest_offset = 0;
    #else
        using SeenOffsets = condst AtomicBitSet<max_Offsets>;
        SeenOffsets seen_offsets;
    #endif // !EMBARALLEL
        RunningCounts running_counts;
    } PACKED;

    static consteval size_t flat_size = sizeof(Recovery) + sizeof(Entries);
#endif // ENABLE_INDEXING

private:
    condst Almanac &NO_ALIAS almanac;
    condst Entries &NO_ALIAS entries;
#ifdef ENABLE_INDEXING
    /// NOTE: Can be null iff it's for read-only use
    Recovery *const NO_ALIAS recovery;
#endif

    DEF_FD_IMPORTER(entries)
#ifdef ENABLE_INDEXING
    DEF_FD_IMPORTER(recovery)
#endif

#ifdef ENABLE_INDEXING
    inline void
    set_recovery(Entry entry) &
# ifndef EMBARALLEL
    { recovery->latest_offset = entry; }
# else
    { recovery->seen_offsets.set(entry); }
# endif // !EMBARALLEL
#endif // ENABLE_INDEXING

    inline PURE RETURNS_NONNULL /*condst*/ Entry*
    find_entry(const ID id) /*condst*/& { return &entries[almanac.stat(id).offset()]; }

    inline PURE RETURNS_NONNULL const Entry *condst
    find_entry(const ID id) const&
    { return const_cast<const Entry*>( const_cast<AIndex&>(*this).find_entry(id) ); }

    inline PURE /*condst*/ Entry&
    __find_subentry(/*condst*/ Entry& entry, condst uint8_t subentry=0) /*condst*/&
    { return *&(&entry)[subentry]; }
    inline PURE const Entry&
    __find_subentry(const  Entry& entry,  const uint8_t subentry=0)  const&
    { return *&(&entry)[subentry]; }

    inline PURE /*condst*/ Entry&
    _find_subentry(const ID id, const uint8_t subentry=0) /*condst*/&
    { return find_entry(id)[subentry]; }

    PURE HOT /*condst*/ Entry&
    find_subentry(const Hash hash, condst uint8_t subentry=0) /*condst*/& {
        ASSUME(!almanac.is_zero(hash));
//         DBG("Finding subentry %hhu: %.8x", subentry, hash);
        const auto id = almanac._find_id(hash);
//         DBG_(id, ".8x");
        ASSUME(subentry < almanac.count(id), subentry, "hhu");
        return _find_subentry(id, subentry);
    }

    INLINE_ PURE HOT /*condst*/ Entry&
    find_superentry(const Hash hash) /*condst*/& { return find_subentry(hash); }

    inline PURE const Entry&
    find_subentry(const Hash hash, const uint8_t subentry=0) const&
    { return const_cast<const Entry&>( const_cast<AIndex&>(*this).find_subentry(hash, subentry) ); }

public:
    inline COLD
    AIndex(condst Almanac &&NO_ALIAS almanac
    #ifdef ENABLE_INDEXING
         , Recovery *const NO_ALIAS recovery
    #endif
         , condst Entries &&NO_ALIAS entries)
    : almanac(almanac)
    , entries(entries)
    #ifdef ENABLE_INDEXING
    , recovery(recovery)
    #endif
    { }

    COLD
    AIndex(condst Almanac&& almanac
    #ifdef ENABLE_INDEXING
         , int recovery_fd
    #endif
         , int entries_fd
         , bool readonly = fd_importer_ro_default)
    : AIndex(FWD(almanac)
      #ifdef ENABLE_INDEXING
           , maybe_recovery_from_fd
      #endif
           , FWD(fd_to_entries(entries_fd, readonly)) )
    {}

    COLD
    ~AIndex() {
        DBG("Destroying AIndex: %p", this);
        //munmap_(almanac);
        delete &almanac;
        munmap_(entries);
    #ifdef ENABLE_INDEXING
        ifL (recovery) _munmap(recovery);
    #endif
    }

    ONLY_DEFAULT_MOVEABLE(AIndex)

    COLD bool
    mark_done() condst& {
        /*mark(almanac);*/
        mark(entries);
    #ifdef ENABLE_INDEXING
        ifU (mark_complete(entries, AINDEX_NAME) != 0) return false;
        ifL (recovery) {
            mark(*recovery);
            if (!work_blkdev_off && UNLIKELY(unlink_pathed_file(RECOVERY_NAME) != 0)) return false;
            if (work_blkdev_off) return discard_work(flat_size);
        } else ASSUME(!work_blkdev_off); // We REQUIRE recovery for work_blkdev_off due to our HACK
    #endif
        return true;
    }

#ifdef ENABLE_INDEXING
# ifndef EMBARALLEL
    inline PURE Offset recover_offset() const& { return recovery->latest_offset; }
# endif
    inline PURE uint8_t recover_count(const Hash hash) condst&
    { return recovery->running_counts.get(hash); }
#endif // ENABLE_INDEXING

private:
#ifdef ENABLE_INDEXING
    HOT void
    insert_entry(const Hash hash, const Entry&& entry) & { ASSUME(recovery);
    #ifdef EMBARALLEL
        ifU (recovery->seen_offsets.test(entry)) return;
    #endif
        auto count = recover_count(hash);
        ASSUME(count < Almanac::Stat::max_count);
        auto& superentry = find_superentry(hash);
        __find_subentry(superentry, count) = entry;
        /// We shouldn't need this bucket for a while
        //maybe_madvise(&superentry, count * sizeof(Entry), MADV_COLD);
        set_recovery(entry);
        /// HACK: We just treat max_count as saturation, praying we won't ever insert there again
        ifL (count < Almanac::Stat::max_count) recovery->running_counts.set(hash, count+1);
    #ifdef DEBUG
        else WRN("Counter saturated: %hhu >= %hhu @ %.8x|%.8X",
                 count, Almanac::Stat::max_count, hash, entry);
    #endif
    }
#endif // ENABLE_INDEXING

public:
    template<typename Predicate>
    INLINE PURE std::result_of_t<Predicate(Entry)> /* Should be some sort of std::optional */
    first_satisfying_offset(const Hash hash, condst Predicate&& pred) const& {
        if (const auto id = almanac.find_id(hash)) {
            /*debuggable*/ const auto& subentries = find_entry(*id);
            debuggable const auto count = almanac.count(*id);
            ASSUME(count > 0);
            for (uint8_t i=0; i<count; i++) {
                DBG("Checking %.8x @ %.8x (%hhu/%hhu) for satisfaction",
                    *id, almanac.stat(*id).offset(), i+1, count);
                debuggable const Entry& subentry = subentries[i];
                if (const auto&& res = pred(subentry)) return *res;
            }
        }
        DBG("Can't get no satisfaction");
        return std::nullopt;
    }

#ifdef ENABLE_INDEXING
    template<Direction D = Direction::F, Negation N = Negation::I>
    inline NONNULL HOT bool
    update_index(const WordAtlasSlice blk, const size_t size, const Entry&& entry) & {
        ASSUME_SZ_SIZE(size);
        Hash hash = _hash_blk<D,N>(blk, size);
    #ifndef TERSE
        DBG("%p; ""%c, "   "%c, "      "%zu \t; ""%.8x -> %u",
            blk, dir_chr(D), neg_chr(N), size, hash, entry);
    #endif
        insert_entry(hash, FWD(entry));
        return true;
    }
#endif // ENABLE_INDEXING

#ifndef NDEBUG
    template<Direction D = Direction::F, Negation N = Negation::I>
    NONNULL HOT bool
    validate(const ByteAtlas& atlas, const SZ sz) const& {
        static_assert(sizeof(uint64_t) > sizeof(Hash));
        const auto size = size_for_sz(sz);
        for (uint64_t h=0; h<=max_Hash; h++) {
            /*debuggable*/ condst auto&& validator = [=]
            (const Entry& subentry) -> std::optional<Offset> {
                const auto off = offset_for_sz(sz, subentry);
                const auto h_ = _hash_blk<D,N>((WordAtlasSlice)&atlas[off], size);
                ifU (h != h_) {
                    WRN("Failed validation @ %zu(%u); %c/%c;%u: %x != %x",
                        off, subentry, dir_chr(D), neg_chr(N), size, h, h_);
                    return off;
                } else return std::nullopt;
            };
            if (first_satisfying_offset<decltype(validator)>(h, FWD(validator))) return false;
        }
        return true;
    }
#endif // !NDEBUG
};
#endif // ENABLE_ALMANAC

////////////////////////////////////////////////////////////////////////////////////////////////////
#if defined(ENABLE_COUNTING) || defined(ENABLED_ALMANAC)
// "Counting" index; just keeps a count (for measuring collisions)
class CIndex final {
    #define CINDEX_NAME "cndex"
    STATIC_ASSERT_VALID_FILENAME(CINDEX_NAME)
public:
    // How many entries/collisions there've been at this key/hash
    using Entry =
    #ifndef EMBARALLEL
        uint8_t;
    #else
        atomic_uint8_t;
    #endif
    // Sorted in increasing Hash order
    using Entries = condst std::array<condst Entry, max_Hashes>;

    // This is used during indexing to keep track of progress and resume if need be
    struct Recovery final {
        volatile condst Offset latest_offset;
    } PACKED;

#ifdef ENABLE_INDEXING
    static consteval size_t flat_size = sizeof(Recovery) + sizeof(Entries);
#endif

private:
    condst Entries &NO_ALIAS entries;
    // NOTE: Can be null iff it's for read-only use
    condst Recovery *const NO_ALIAS recovery;

    // NOTE: This is for READ-ONLY use
    inline COLD
    CIndex(const Entries &NO_ALIAS entries)
    : entries(const_cast<condst Entries&>(entries)), recovery(nullptr) {}

    DEF_FD_IMPORTER(entries)
    DEF_FD_IMPORTER(recovery)

#ifdef ENABLE_INDEXING
    HOT Entry
    insert_entry(const Hash hash) & {
        auto& entry = find_entry(hash);
    #ifndef EMBARALLEL
        ASSUME(recovery /*|| (latest_offset == 0)*/);
    #else
        ASSUME(recovery /*|| (latest_offset = 0)*/);
        #warning "EMBARALLEL broken for CIndex; create_indices_per_sz doesn't init latest_offset"
        #pragma omp single
    #endif
        {
            //DBG("%x -> %hhu", hash, entry);
            ifU (++entry > max_Count) {
                WRN("Hash %x has too many collisions (%hhu > %hhu)", hash, entry, max_Count);
            #ifdef DEBUG
                abort();
            #endif
            }
            recovery->latest_offset++;
        }
        return entry;
    }
#endif

public:
#ifdef ENABLE_INDEXING
    inline COLD NONNULL
    CIndex(condst Entries &&NO_ALIAS entries, condst Recovery *const NO_ALIAS recovery)
    : entries(entries), recovery(recovery) {}
    inline COLD
    CIndex(int entries_fd, int recovery_fd, const bool readonly = fd_importer_ro_default)
    : CIndex(FWD(fd_to_entries(entries_fd, readonly))
            ,maybe_recovery_from_fd)
    {}
#endif
    inline COLD
    CIndex(int entries_fd) : CIndex(FWD(fd_to_entries(entries_fd, true))) {}

    COLD
    ~CIndex() {
        DBG("Destroying CIndex: %p", this);
        munmap_(entries);
    #ifdef ENABLE_INDEXING
        ifL (recovery) munmap_(*recovery);
    #else
        ASSUME(!recovery);
    #endif
    }

    ONLY_DEFAULT_MOVEABLE(CIndex)

    COLD bool
    mark_done() condst& {
    #ifdef ENABLE_INDEXING
        ifL (recovery) {
            // Print the counts when done, since presumably we just finished writing the CIndex
            print_counts();
            mark(entries);
            ifU (mark_complete(entries, CINDEX_NAME) != 0) return false;
            mark(*recovery);
            if (!work_blkdev_off && UNLIKELY(unlink_pathed_file(RECOVERY_NAME) != 0)) return false;
            if (work_blkdev_off) return discard_work(flat_size);
        } else {
            ASSUME(!work_blkdev_off); // We REQUIRE recovery for work_blkdev_off due to our HACK
            mark(entries);
        }
    #else
        ASSUME(!recovery);
        mark(entries);
    #endif
        return true;
    }

    inline PURE condst Entry& find_entry(const Hash hash) condst& { return entries[hash]; }
#ifdef ENABLE_INDEXING
    /// NOTE: DON'T call this if you haven't provided a Recovery
    inline PURE Offset recover_offset() condst& { ASSUME(recovery); return recovery->latest_offset; }

    // Returns whether successful
    template<Direction D = Direction::F, Negation N = Negation::I>
    /*inline*/ HOT bool
    update_index(const WordAtlasSlice blk, const size_t size
               , const MAYBE_UNUSED UntypedAtlasDesc desc=0) & {
        ASSUME_SZ_SIZE(size);
        Hash hash = _hash_blk<D,N>(blk, size);
        // NB: Currently this can't fail
        auto entry = insert_entry(hash);
        ASSUME(entry); return entry;
    }
#endif

private:
    static consteval auto max_Count = Almanac::Stat::max_count;
    static consteval auto max_Counts = 1+(size_t)max_Count;
    using Counts = std::array<Entry, max_Counts>;

    template<typename F> constexpr
    PURE NEWLIKE Counts *condst NO_ALIAS
    count(condst F&& per_entry) const& {
        auto counts = new Counts {0};
        ifU (!counts) return nullptr;
        for (const Entry& entry: entries) {
            if (entry) {
                auto& count = (*counts)[entry-1];
                ifU (entry > max_Count) {
                    ERR("Entry %hhu (#%hhu) surpassed max count of %hhu", entry, count, max_Count);
                    delete counts;
                    return nullptr;
                } else count++;
            }
            per_entry(entry);
        }
        return counts;
    }

    inline NEWLIKE Counts *condst count() const { return count([](auto){}); }

    static consteval auto direct_bits = sizeof(UntypedAtlasDesc) * __CHAR_BIT__;
    /// Returns recommended new direct_storage (@see VIndex)
    // FIXME: Check vis-a-vis bits/bytes
    static uint16_t
    recommend(const Counts& counts,
              uint16_t direct_storage = direct_bits/sizeof(std::byte), bool print=true) {
        Offset total_bits = 0, recommend_bits = 0;
        size_t total = 0;
        for (uint16_t i = 1; i <= max_Count; i++) {
            if (const Entry count = counts[i-1]) {
                total += count * i;
                const auto bits = std::log2p1(count);
                if (print) MSG("%hu: %hhu (<= 2^%u); %lu", i, count, bits, total);
                total_bits += bits ?: 1;
            #ifdef ENABLE_VINDEX
                if (i > direct_storage)
                    recommend_bits += bits ?: 1;
	    #endif // ENABLE_VINDEX
	    }
        }
        ifU (print) {
            MSG("Zeroes: %u", (Hash)(max_Hashes - total));
            MSG("Total bits: %u", total_bits);
        #ifdef ENABLE_VINDEX
            MSG("Advised bits (for %hu direct): %u", direct_storage, recommend_bits);
        #endif
        }
    #ifdef ENABLE_VINDEX
        auto direct_limit = direct_storage * direct_bits;
        if (direct_limit < recommend_bits) {
            if (print) MSG("Over the direct limit! (%lu)", direct_limit);
            // Round to nearest direct_bits
            const auto new_limit = recommend_bits + (recommend_bits % direct_bits);
            // Try again to see if we can optimize further
            const auto next_limit = recommend(counts, new_limit, false);
            direct_limit = (next_limit != direct_limit) ? next_limit : new_limit;
            if (print) {
                const auto new_entries = direct_limit / direct_bits;
                // TODO: Should we recurse further?
                MSG("Recommending new limit %lu for direct entries %lu", direct_limit, new_entries);
            }
        }
        return direct_limit;
    #else
        return 0;
    #endif // ENABLE_VINDEX
    }

public:
    bool print_counts() const& {
        const auto counts = count();
        ifU (!counts) return false;
        MSG("Counts:");
        recommend(*counts, VIndex::direct_storage, true);
        delete counts;
        return true;
    }

#ifdef ENABLE_ALMANAC
    static bool write_stat(const int fd, condst Almanac::Stat&& stat) {
        ASSUME(fd != -1);
        auto written = write(fd, &stat, sizeof(Almanac::Stat));
        ifU (written != sizeof(Almanac::Stat)) {
            ERR("Failed to write out stat: %ld; %m", written);
            return false;
        }
        return true;
    }

    PURE Offset /// PURE b/c fd output is explicit
    export_stats(const int fd, condst Almanac::Zeroes& zeroes) condst& {
        uint_fast64_t current_idx = 0;
        static_assert(sizeof(current_idx) > sizeof(Offset),
                      "current_idx would overflow as an Offset");
        Offset cumul_off = 0, run_len = 0, final_run_len = 0;
        Entry last_count = 0, final_count = 0;
        Hash final_start_hash = 0;
        struct { Offset run = 0; Entry count = 0; } longest;

        static condst bool valid_fd = fd != -1;
        static condst auto on_count_change = [&]() mudable -> bool {
            //DBG_(current_idx, ".8x");
            ifU (cancelled || !valid_fd) return false;
            ASSUME(1 <= current_idx && current_idx <= max_Offsets, current_idx, "lu");
            ASSUME(last_count <= Almanac::Stat::max_count, last_count, "hhu");
            ASSUME(0 < run_len && run_len <= Almanac::Stat::max_len, run_len, "u");
            static_assert(sizeof(Offset) == sizeof(Hash));

            const Offset last_start_idx = current_idx - run_len;
            const Hash start_hash = last_start_idx;
            if (last_count) {
                DBG("Writing stats: %u/%.8x, %hhu; %u", cumul_off, start_hash, last_count, run_len);
                const Entry count = last_count-1u;
                ifU (!write_stat(fd, { start_hash, cumul_off, count } )) return false;
                cumul_off += run_len * last_count;
            } else {
                DBG("Eliding zeroes: %u/%.8x; %u", cumul_off, start_hash, run_len);
                ASSUME(last_start_idx + run_len < zeroes.size());
                for (Offset i = 0; i < run_len; i++) zeroes.set(last_start_idx+i);
            }
            ifU (run_len > longest.run) longest = { run_len, last_count };

            // For everything but the last idx...
            ifL (current_idx <= max_Offset) { ASSUME(cumul_off <= max_Offset, cumul_off, "u"); }
            else DBG_HERE;
            run_len = 0; // Is about to be 1 (via loop)
            return true;
        };

        for (const Entry count: entries) {
            ASSUME(count <= Almanac::Stat::max_count, count, "hhu");
            // Skip the first "change"
            if ((UNLIKELY(cancelled) || UNLIKELY(!valid_fd)
              || LIKELY(current_idx > 0)) && last_count != count)
                ifU (!on_count_change()) { run_len = max_Offset; break; }
            last_count = count;
            run_len++; current_idx++;
        }
        final_run_len = run_len;
        ifU (final_run_len == max_Offset) goto ret;
        MSG("Final: %u, %u", final_run_len, last_count);

        ifU (!on_count_change()) goto ret;
        ASSUME(current_idx == max_Offsets, current_idx, "lu");
        final_start_hash = current_idx - final_run_len;
        /// Because we've reached the end and overflowed by 1
        DBG_(cumul_off, "ux");
	// HACK: Assumes max_Offset / SINGULAR_ATLAS_SIZE
        ASSUME(cumul_off == (last_count ? 0 : (Offset)(-final_run_len)));

        static constexpr const Offset final_anchor_offset = max_Offset;
        final_count = last_count-1u;
        // Write out a final, anchor value, just so we can compute the last value differentially
        ifU (!write_stat(fd, { final_start_hash, final_anchor_offset, final_count } )) goto ret;
        MSG("Longest: %u, %u", longest.run, longest.count);
    ret:
        ifL (valid_fd) close(fd);
        return final_count;
    }
#endif // ENABLE_ALMANAC
};
#endif // defined(ENABLE_COUNTING) || defined(ENABLED_ALMANAC)

////////////////////////////////////////////////////////////////////////////////////////////////////
#undef DECL_FD_IMPORTER
#undef _DEF_FD_IMPORTER
#undef DEF_FD_IMPORTER
#undef _munmap
#undef munmap_
#undef _mark
#undef mark
#undef mark_complete
#undef maybe_recovery_from_fd
////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////
// Top-level Index generation/use API
////////////////////////////////////////////////////////////////////////////////////////////////////

/// Allocate full tree while `todo`ing
/// FS layout: ./szclass_idx(int)/dir/neg/{...}
template<typename F>
static inline bool
do_thru_dirs(condst F&& todo, const bool create = false) {
#ifndef ENABLE_INDEXING
    ASSUME(!create);
#endif
    for(SZ sz = first_sz; sz <= last_sz; sz++) {
        ifU (skip_sole_sized(sz)) { DBG("Skipping sz %hhu", sz); continue; }
        auto appended = append_mkdir_current_path(sz, create);
        for(uint8_t d=0; d<num_directions; d++) { /// For each Direction
		    auto dir = (Direction)d;
            ifU (skip_sole_directed(dir)) continue;
            auto appended = append_mkdir_current_path(dir, create);
 		    for(uint8_t n=0; n<num_negations; n++) { /// For each Negation
 		        auto neg = (Negation)n;
 		        ifU (skip_sole_negated(neg)) continue;
                auto appended = append_mkdir_current_path(neg, create);
                const bool success = !todo(sz, dir, neg);
                ifU (!success) DBG("do_thru_dirs todo unsuccessful!");
                ifU (cancelled || !success) {
                    sync(); return false;
                }
                /// reset for next iteration
                clear_trailing_current_path(appended);
 		    }
            /// reset for next iteration
            clear_trailing_current_path(appended);
	    }
	    clear_trailing_current_path(); /// NB: Subtraction done separately bc we can reach 0
	    current_path_len -= appended; /// reset for next iteration
        ASSUME(current_path_len < max_current_path_len, current_path_len, "hhu");
    }
    return true;
}

/// Final Index selection
using Index =
#ifndef ENABLE_ALMANAC
# ifndef ENABLE_VINDEX
    CIndex;
# define gen_indices gen_cindices
# else
    VIndex;
# define gen_indices gen_vindices
# endif
#else
    AIndex;
# define gen_indices gen_aindices
#endif

template <typename Index = Index>
using _TypedIndex = std::array<Index */*condst*/ NO_ALIAS, num_directions*num_negations>;
template <typename Index = Index>
using _TypedIndices = std::array<_TypedIndex<Index>, num_szs>;

static consteval uint8_t
typed_index(Direction d, Negation n) { return (uint8_t)d*num_negations + (uint8_t)n; }

#ifdef ENABLE_VINDEX
using TypedVIndex = _TypedIndex<VIndex>;
using TypedVIndices = _TypedIndices<VIndex>;
#endif

#ifdef ENABLE_ALMANAC
using TypedAIndex = _TypedIndex<AIndex>;
using TypedAIndices = _TypedIndices<AIndex>;
#endif

#ifdef ENABLE_COUNTING
using TypedCIndex = _TypedIndex<CIndex>;
using TypedCIndices = _TypedIndices<CIndex>;
#endif

using TypedIndices = _TypedIndices<Index>;

// Allocate full tree of typed indices
// FS layout: ./szclass_idx(int)/dir/neg/{...}
template<typename Indices = TypedIndices, typename Constructor>
static NEWLIKE Indices *const SHOULD_NONNULL
gen_indices_thru_dirs(condst Constructor&& make_index, const bool create = false) {
#ifndef ENABLE_INDEXING
    ASSUME(!create);
#else
    ASSUME(!work_blkdev_off); // This would be nonreentrant, and so we must use the index immediately
#endif
    // FIXME: Indices lifecycle (wrt component indices)
    Indices *const indices = new Indices; ASSUME(indices);
    condst auto&& todo = [indices,&make_index]
    (const SZ sz, const Direction dir, const Negation neg) mudable -> bool {
        const auto idx = idx_for_sz(sz);
        //DBG("For %u, %c, %c", idx, Dirs[d], Negs[n]);
        ASSUME_VALID_current_path_len;
        auto& indices_ = (*indices)[idx];
        const auto i = make_index();
        ifU (!i) { DBG("Didn't load index: %hhu", idx); }
        else { DBG("Loaded index: %hhu, %c/%c: %p", idx, dir_chr(dir), neg_chr(neg), i); }
        // NB: Unconditional because nullptr is valid and taken as skipping
        indices_[typed_index(dir,neg)] = i;
    #ifdef NDEBUG
        return true;
    #else
        return i != nullptr;
    #endif
    }; do_thru_dirs(FWD(todo), create);
    return indices;
}

/// HACK: When generating, shortcut to returning (outer) nullptr if any argument fails to open
/// FIXME: Maybe close other arguments that've been opened if we fail (though OS cleans)
#define maybe_open_pathed(name, size) ({                                \
    const auto src_dir = !readonly ? work_fd : dest_dir_fd;             \
    const auto fd = open_pathed(name, size, create, readonly, src_dir); \
    ifU (fd == -1) return nullptr;                                      \
    fd;                                                                 \
})

template<typename Index>
static inline NEWLIKE Index *NO_ALIAS const
gen_index(MAYBE_UNUSED const bool create = false, MAYBE_UNUSED const bool readonly = false);

template<typename Index>
static inline NEWLIKE Index *NO_ALIAS const
gen_index_via_blkdev(MAYBE_UNUSED const bool create = false
                    ,MAYBE_UNUSED const bool readonly = false);

template<typename Index = Index>
static inline NEWLIKE _TypedIndices<Index> *NO_ALIAS const
_gen_indices(const bool create = false, const bool readonly = false) {
    condst auto&& gen = [=]{ if (work_blkdev_off) return gen_index<Index>(create,readonly);
                             else return gen_index_via_blkdev<Index>(create,readonly); };
    return gen_indices_thru_dirs<_TypedIndices<Index>, decltype(gen)>(FWD(gen), create);
}

#define DEF_GEN_INDICES(i, I)                                                                 \
template<> \
INLINE NEWLIKE /*Typed##*/I##Index *NO_ALIAS const SHOULD_NONNULL                                 \
gen_index/*<I##Index>*/(const bool create = false, const bool readonly = false)                       \
{ return gen_##i##index(create, readonly); }                                             \
template<> \
INLINE NEWLIKE /*Typed##*/I##Index *NO_ALIAS const SHOULD_NONNULL                                 \
gen_index_via_blkdev/*<I##Index>*/(const bool create = false, const bool readonly = false)            \
{ return gen_##i##index_via_blkdev(create, readonly); }                                  \
INLINE NEWLIKE Typed##I##Indices *NO_ALIAS const SHOULD_NONNULL                               \
gen_##i##indices(const bool create = false, const bool readonly = true) {                     \
    condst auto&& gen_index = !work_blkdev_off ? gen_##i##index : gen_##i##index_via_blkdev;  \
    condst auto&& gen = [=]{ return gen_index(create,readonly); };                            \
    return gen_indices_thru_dirs<Typed##I##Indices, decltype(gen)>(FWD(gen), create);         \
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Gen/create VIndex
#ifdef ENABLE_VINDEX

static inline NEWLIKE VIndex *NO_ALIAS const SHOULD_NONNULL
gen_vindex(const bool create = false, const bool readonly = true) {
    const bool incomplete = create || !readonly;
    const auto typdx = MAYBE_INCOMPLETE_FILE(incomplete, TYPEDEX_NAME)
             , vndex = MAYBE_INCOMPLETE_FILE(incomplete, VINDEX_NAME);

# ifdef ENABLE_INDEXING
    const auto rcvry = open_pathed(RECOVERY_NAME, sizeof(VIndex::Recovery), create, readonly);
    const bool ro = readonly || (rcvry == -1);
# else
    ASSUME(readonly);
    const bool ro = readonly;
# endif
# define open_pathed_(name, Type) maybe_open_pathed(name, sizeof(VIndex::Type))
    return new VIndex(open_pathed_(typdx, Typedex)
                    , open_pathed_(vndex, Entries)
                #ifdef ENABLE_INDEXING
                    , rcvry
                #endif
                #define OPEN_APNDX(_N, A, a) \
                    , open_pathed_(MAYBE_INCOMPLETE_FILE(incomplete, #a APPENDIX_SUFFIX), A##ppendix)
                    APPLY_APNDXS(OPEN_APNDX)
                #undef OPEN_APNDX
                    , open_pathed_(APPENDIX_NAME, Appendix)
                    , ro);
# undef open_pathed_
}

static inline NEWLIKE VIndex *NO_ALIAS const SHOULD_NONNULL
gen_vindex_via_blkdev(MAYBE_UNUSED const bool create = false
                     ,MAYBE_UNUSED const bool readonly = false) {
#ifndef ENABLE_INDEXING
    UNREACHABLE; return nullptr;
#else
    ASSUME(!readonly);
    ASSUME(work_blkdev_off && work_fd != -1);

    if (create) ifU (!discard_work(VIndex::flat_size, true)) return nullptr;

    // NB: We don't close work_fd so it can be later copied without reopening
    auto blkdev = (uintptr_t)mmap_out<VIndex>(work_fd, VIndex::flat_size, MADV_NORMAL, false);
    const auto rcvry = (VIndex::Recovery*)blkdev;
    const auto typdx = (VIndex::Typedex*)(blkdev += sizeof(VIndex::Recovery));
    const auto vndex = (VIndex::Entries*)(blkdev += sizeof(VIndex::Typedex));
    const auto apndx = (VIndex::Appendix*)(blkdev + sizeof(VIndex::Entries));
    // NOTE: We begin at the end b/c we're going to set up the appendices backwards (i.e., in order)
    blkdev = (uintptr_t)rcvry + VIndex::flat_size;
    return new VIndex(move(*typdx), move(*vndex), move(rcvry)
                #define MAP_APNDX(_N, A, _a) \
                    , move(*(condst VIndex::A##ppendix *const)(blkdev -= sizeof(VIndex::A##ppendix)))
                    APPLY_APNDXS(MAP_APNDX)
                #undef MAP_APNDX
                    , /*[&]{ ASSUME(blkdev-sizeof(VIndex::Appendix) == (uintptr_t)apndx)
                         ; return*/ move(*apndx)/*; }()*/);
#endif // !ENABLE_INDEXING
}

/// Allocate full tree of typed indices
/// FS layout: ./szclass_idx(int)/dir/neg/{typdx,index,apndx,...}
DEF_GEN_INDICES(v, V)

#endif // ENABLE_VINDEX
////////////////////////////////////////////////////////////////////////////////////////////////////
// Gen/create CIndex
#ifdef ENABLE_COUNTING

static inline NEWLIKE CIndex *NO_ALIAS const SHOULD_NONNULL
gen_cindex(const bool create = false, const bool readonly = true) {
    const bool incomplete = create || !readonly;
    const auto cndex = maybe_open_pathed(MAYBE_INCOMPLETE_FILE(incomplete, CINDEX_NAME)
                                       , sizeof(CIndex::Entries));
#ifdef ENABLE_INDEXING
    // NB: If the file doesn't exist, we're just using a nullptr and opening readonly
    const auto rcvry = open_pathed(RECOVERY_NAME, sizeof(CIndex::Recovery), create, readonly);
    return (rcvry != -1) ? new CIndex(cndex, rcvry, readonly) : new CIndex(cndex);
#else
    ASSUME(!incomplete); return new CIndex(cndex);
#endif // ENABLE_INDEXING
}

static inline NEWLIKE CIndex *NO_ALIAS const SHOULD_NONNULL
gen_cindex_via_blkdev(const bool create = false, const bool readonly = true) {
#ifndef ENABLE_INDEXING
    UNREACHABLE; return nullptr;
#else
    ASSUME(!readonly);
    ASSUME(work_blkdev_off && work_fd != -1);

    if (create) ifU (!discard_work(CIndex::flat_size, true)) return nullptr;

    auto blkdev = (uintptr_t)mmap_out<CIndex>(work_fd, CIndex::flat_size, MADV_NORMAL, false);
    return new CIndex(move(*(CIndex::Entries*)(blkdev + sizeof(CIndex::Recovery)))
                    , (CIndex::Recovery*)blkdev);
#endif // ! ENABLE_INDEXING
}

DEF_GEN_INDICES(c, C)

#endif // ENABLE_COUNTING
////////////////////////////////////////////////////////////////////////////////////////////////////
// Gen/create AIndex
#ifdef ENABLE_ALMANAC

static inline NEWLIKE Almanac *NO_ALIAS const SHOULD_NONNULL
gen_almanac(const bool create = false, const bool readonly = true) {
    const bool incomplete = create || !readonly;
    const auto almnc = MAYBE_INCOMPLETE_FILE(incomplete, ALMANAC_NAME)
             , zeros = MAYBE_INCOMPLETE_FILE(incomplete, ZEROES_NAME);
    return new Almanac(maybe_open_pathed(almnc, 0 /*dummy b/c variable size*/)
                     , maybe_open_pathed(zeros, sizeof(Almanac::Zeroes))
                     , readonly);
}

static inline NEWLIKE AIndex *NO_ALIAS const SHOULD_NONNULL
gen_aindex(const bool create = false, const bool readonly = true) {
    const bool incomplete = create || !readonly;
    const auto almnc = gen_almanac(false, true); // Uses extant almanac
    ifU (!almnc) return nullptr;
    const auto andex = MAYBE_INCOMPLETE_FILE(incomplete, AINDEX_NAME);
# ifdef ENABLE_INDEXING
    const auto rcvry = open_pathed(RECOVERY_NAME, sizeof(AIndex::Recovery), create, readonly);
    const bool ro = readonly || (rcvry == -1);
# else
    ASSUME(readonly);
    const bool ro = readonly;
# endif
    return new AIndex(move(*almnc)
                #ifdef ENABLE_INDEXING
                    , rcvry
                #endif
                    , maybe_open_pathed(andex, sizeof(AIndex::Entries))
                    , ro);
}

static inline NEWLIKE AIndex *NO_ALIAS const SHOULD_NONNULL
gen_aindex_via_blkdev(MAYBE_UNUSED const bool create = false
                     ,MAYBE_UNUSED const bool readonly = false) {
#ifndef ENABLE_INDEXING
    UNREACHABLE; return nullptr;
#else
    ASSUME(!readonly);
    ASSUME(work_blkdev_off && work_fd != -1);
    const auto almnc = gen_almanac(false, true); // Uses extant almanac
    ifU (!almnc) return nullptr;

    if (create) ifU (!discard_work(AIndex::flat_size, true)) return nullptr;

    const auto blkdev = (uintptr_t)mmap_out<AIndex>(work_fd, AIndex::flat_size, MADV_NORMAL, false);
    return new AIndex(move(*almnc)
                    , (AIndex::Recovery*)blkdev
                    , move(*(AIndex::Entries*)(blkdev + sizeof(AIndex::Recovery))));
#endif // !ENABLE_INDEXING
}

DEF_GEN_INDICES(a, A)

#endif // ENABLE_ALMANAC

#undef maybe_open_pathed
////////////////////////////////////////////////////////////////////////////////////////////////////
// Indices' creation

template<typename Index = Index>
static PURE const _TypedIndices<Index>&
indices_from_cwd(const bool readonly = true) {
#ifndef ENABLE_INDEXING
    ASSUME(readonly);
#endif
    const auto indices = _gen_indices<Index>(false, readonly);
    ifL (indices) return *indices;
    else {
        ERR("Error finding indices from current working directory");
        exit(3);
    }
#undef gen_indices
}

#ifdef ENABLE_INDEXING

static consteval UntypedAtlasDesc
reduce_to_desc_for_sz(SZ sz, size_t idx, MAYBE_UNUSED size_t atlas_size) {
    auto _desc = idx;
# ifdef ENABLE_SEGMENTS
    if (is_seg_sz(sz)) _desc = rel_offset_for_seg(sz, idx);
    else
# endif
    if (is_blk_sz(sz)) _desc /= blk_stride;
    ASSUME(_desc <= max_Offset);
    return (UntypedAtlasDesc)_desc;
}

template<typename GenIndex, Direction D = Direction::F, Negation N = Negation::I>
static NONNULL bool
create_index_per_sz(GenIndex&& gen_index
                  , const ByteAtlas& atlas, const size_t atlas_size, const SZ sz) {
    constexpr auto d = (uint8_t)D, n = (uint8_t)N;
    ASSUME_SZ(sz);
    ASSUME_ATLAS_SIZE(atlas_size);
    const auto size = size_for_sz(sz);
    ASSUME_SZ_SIZE(size);
    const auto class_idx = idx_for_sz(sz);

    // FIXME: assumes the calls succeed
    auto appended_path = append_mkdir_current_path(sz, false);
    appended_path += append_mkdir_current_path(D, false);
    appended_path += append_mkdir_current_path(N, false);
    ASSUME_VALID_current_path_len;

    bool success = false, skip = false;

    // NOTE: Since gen_index is nonreentrant if we're using a work block device
    // it's important that we've passed it down so the side-effects occur here
    auto _index = gen_index();
    auto&& cleanup = [&] {
        // Separate subtraction b/c
        current_path_len -= appended_path;
        ASSUME(current_path_len < max_current_path_len, current_path_len, "hhu");
        ifL (_index) delete _index;
    };
    ifU (!_index) {
        WRN("Skipping index creation for %hhu, %c, %c, %u", class_idx, Dirs[d], Negs[n], size);
        cleanup();
        return true;
    }
    auto& index = *_index;

# ifndef EMBARALLEL
    // NB: We can be mode-independent because the recover_offset will be 0 if this is new
    const auto recovery = index.recover_offset();
    // Unless fresh, start from right after we stopped
    const Offset _start = !recovery ? 0 : recovery+1;
# else
    const Offset _start = 0; // Index will skip over values we've seen
# endif // EMBARALLEL
    const size_t stride = stride_for_sz(sz)
               ,  start = offset_for_sz(sz, _start)
               ,    end = atlas_end_for_sz(sz, atlas_size);

# ifdef DEBUG
    skip = debug_magic && strstr(debug_magic, "skip");
# endif
    ifL (!skip)
        DBG("Creating index[%hhu]: %c, %c, %u", class_idx, Dirs[d], Negs[n], size);
# ifndef EMBARALLEL
    //#pragma omp single /*nowait*/
    //#pragma omp simd simdlen(sizeof(Word)) safelen(stride) aligned(atlas: sizeof(Word))
# else
    #pragma omp parallel for schedule(monotonic: guided)
# endif // EMBARALLEL
    for(size_t i = start; LIKELY(!skip) && i < end; i += stride) {
        ifU (cancelled) {
            sync();
            skip = true; break;
        }
    #if 0
        const auto offset = (D == Direction::F) ? i : (size-1) - i;
        ASSUME(offset < atlas_size, offset, "lx");
    #endif
        const auto src = (WordAtlasSlice)&atlas[i]; // NB: Intentionally NOT offset; _hash_blk is OK
        const auto desc = reduce_to_desc_for_sz(sz, i, atlas_size);
        //if constexpr (highest_size >= madvise_limit)
            //madvise(src, size, MADV_WILLNEED);
        const bool inserted = index.template update_index<D,N>(src, size, move(desc));
        // NB: Currently this can't fail
        ifU (!inserted) {
            ERR("Failed to update @ %lu index %c, %c, %u; %x",
                i, Dirs[d], Negs[n], size, desc);
            skip = true; break;
        }
        //maybe_madvise((void*)src, stride, MADV_COLD);
    }
    ifL (!skip) {
        success = index.mark_done();
        MSG("Done with index[%hhu]: %c, %c, %u", class_idx, Dirs[d], Negs[n], size);
        ifU (!success) WRN("... but something went wrong in marking: %m");
    }

    cleanup();
    return success;
}

template<typename GenIndex>
static NONNULL bool
create_indices_in_cwd(GenIndex&& gen_index, const WordAtlas& _atlas, const size_t atlas_size) {
    const auto& atlas = (ByteAtlas&)_atlas;
    ASSUME_ATLAS_SIZE(atlas_size);

    // For each SZ class
    for(SZ sz = first_sz; sz <= last_sz; sz++) {
        ifU (skip_sole_sized(sz)) { DBG("Skipping sz %hhu", sz); continue; }
    #define PER_NEG(D, Neg)                                              \
        if ((!sole_direction || *sole_direction == Direction::D)         \
          &&(!sole_negation || *sole_negation == Negation::Neg)          \
          && !create_index_per_sz<GenIndex, Direction::D, Negation::Neg> \
                                 (FWD(gen_index), atlas, atlas_size, sz) \
           ) return false;
        // TODO: Rework to APPLY_Negations
     #define PER_DIR(D) \
        { PER_NEG(D, I); PER_NEG(D, N); PER_NEG(D, E); PER_NEG(D, O); }
        APPLY_Directions(PER_DIR)
    #undef PER_NEG
    #undef PER_DIR
    }
    MSG("Created indices");
    return true;
}

# define DEF_CREATE_INDICES_IN_CWD(i, I)                                             \
template<bool resume=true>                                                           \
static NONNULL bool                                                                  \
create_##i##indices_in_cwd(const WordAtlas& atlas, const size_t atlas_size) {        \
    auto&& gen_index = !work_blkdev_off ? gen_##i##index : gen_##i##index_via_blkdev;\
    auto&& gen = [=]{ return gen_index(!resume, false); };                           \
    return create_indices_in_cwd(FWD(gen), atlas, atlas_size);                       \
}

# ifdef ENABLE_COUNTING
DEF_CREATE_INDICES_IN_CWD(c, C)
# endif

# ifdef ENABLE_VINDEX
DEF_CREATE_INDICES_IN_CWD(v, V)
# endif

# ifdef ENABLE_ALMANAC
DEF_CREATE_INDICES_IN_CWD(a, A)

#ifndef ENABLE_COUNTING
#error "CIndexing required"
#endif

template<bool destructive>
static NONNULL bool
create_almanac_in_cwd() {
    auto cindex_fd = open_pathed(CINDEX_NAME, sizeof(CIndex::Entries),
                                 false, !destructive, dest_dir_fd);
    ifU (cindex_fd < 0) return false;
    auto cindex = CIndex(cindex_fd);
    int stats_fd = -1;
    if (!work_blkdev_off)
        stats_fd = open_pathed("." ALMANAC_NAME, 0 /* starting size */, true, false, work_fd);
    else {
        if constexpr(destructive) discard_work();
        const auto ret = lseek(work_fd, *work_blkdev_off, SEEK_SET);
        ifU (ret == -1 || (size_t)ret != *work_blkdev_off) {
            ERR("Failed to seek to %zu for working block device %s: %m",
                *work_blkdev_off, secure_getenv(WORK_ENV_VAR));
            return false;
        }
        else stats_fd = work_fd;
    }
    ifU (stats_fd < 0) return false;
    auto zeroes_fd = open_pathed("." ZEROES_NAME, sizeof(Almanac::Zeroes), true, false, work_fd);
    ifU (zeroes_fd < 0) return false;
    auto zeroes = mmap_out<Almanac::Zeroes>(zeroes_fd, sizeof(Almanac::Zeroes), MADV_RANDOM);
    ifU (!zeroes) return false;

    bool success = false;
    auto num_stats = cindex.export_stats(stats_fd, FWD(*zeroes));
    ifL (num_stats) {
        success = (work_blkdev_off
                ? copy_pathed_file(stats_fd, ALMANAC_NAME,
                                   num_stats * sizeof(Almanac::Stat), *work_blkdev_off)
                : move_complete_pathed_file("." ALMANAC_NAME))
               && move_complete_pathed_file("." ZEROES_NAME);
        if constexpr(destructive) {
            if (work_blkdev_off) discard_work(num_stats * sizeof(Almanac::Stat));
            MSG("Deleting CIndex " CINDEX_NAME);
            ifU (unlink_pathed_file(CINDEX_NAME, dest_dir_fd) != 0) {
                WRN("Failed: %m");
                success = false;
            }
        }
    }
    close(stats_fd);
    close(zeroes_fd);
    return success;
}

// HACK: assume_done is a dumb hack; express errors better
template<bool destructive, bool assume_done = true>
static NONNULL bool
create_almanacs_in_cwd() {
    return do_thru_dirs([](const SZ sz, const Direction d, const Negation n) {
        const auto size = size_for_sz(sz);
        ASSUME_SZ_SIZE(size);
        const auto class_idx = idx_for_sz(sz);
        ifU (!create_almanac_in_cwd<destructive>()) {
            if constexpr(assume_done) {
                WRN("Skipped almanac[%hhu]: %c, %c, %u",
                    class_idx, dir_chr(d), neg_chr(n), size);
            } else {
                ERR("Failed almanac[%hhu]: %c, %c, %u",
                    class_idx, dir_chr(d), neg_chr(n), size);
            }
            return assume_done;
        }
        MSG("Created almanac[%hhu]: %c, %c, %u",
            class_idx, dir_chr(d), neg_chr(n), size);
        return true;
    }, true);
}
# endif // ENABLE_ALMANAC

#endif // ENABLE_INDEXING

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// Charting
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef ENABLE_CHARTING

template<SZ sz>
using SpanSZ = ::span<const byte, size_for_sz(sz)>;

template<SZ sz>
using WSpanSZ = ::span<const Word, size_for_sz(sz)/sizeof(Word)>;

using DescRopeBuilder = ConcBuilder<Desc>;
using DescRope = DescRopeBuilder::NodePtr;

// For wont of auxiliary contexts :/
// Heritors need only really worry about implementing chart_sub
template<typename Self, SZ _sz, typename = std::enable_if_t<is_sz_class(_sz)>>
struct _Chartor : Selfed<Self> {
    static consteval auto sz = _sz;
    static_assert(is_sz_class(sz));

    static consteval auto extent = size_for_sz(sz);
    using Span = const SpanSZ<sz>;
    using __Block = ByteAtlasSlice;
    using _Block = const byte[extent];
    using Block = std::array<byte, extent>;

    static INLINE_ constexpr CONST  Span
    to_span(const __Block b) { return Span((const __Block)b, extent); }
    static INLINE_ constexpr CONST  Span
    to_span(WordAtlasSlice s) { return to_span((__Block)s); }

    static consteval auto word_extent = extent/sizeof(Word);
    using WSpan = const WSpanSZ<sz>;
    using __WBlock = WordAtlasSlice;
    using _WBlock = const Word[word_extent];
    using WBlock = std::array<Word, word_extent>;

    static INLINE_ constexpr CONST NONNULL WSpan
    to_wspan(WordAtlasSlice s) { return WSpan((__WBlock)s, word_extent); }
    static INLINE_ constexpr CONST WSpan
    to_wspan(Span s) { return to_wspan((WordAtlasSlice)s.data()); }

private:
    template<Direction D = Direction::F, Negation N = Negation::I>
    static constexpr PURE NONNULL HOT std::optional<Offset>
    match_off(const Hash hash, const Span&& blk, const Index& index,
              const WordAtlas& atlas, MAYBE_UNUSED const size_t atlas_size) {
        ASSUME_ATLAS_SIZE(atlas_size);
        return index.first_satisfying_offset(hash,
            [&](const Offset off) -> std::optional<Offset> const {
                DBG_(off, "u");
                ASSUME(off < atlas_size / sizeof(Word));
                if constexpr(!is_seg_sz(sz)) {
            #ifndef ENABLE_SEGMENTS
                    const auto&& span = to_wspan(&atlas[off]);
                    ASSUME(span.data() == &atlas[off]);
                    const auto&& blk_ = to_wspan(blk);
                    ASSUME((ByteAtlasSlice)blk_.data() == blk.data());
                    if (negationwise_equivalent<Word, word_extent, D, N>(FWD(span), FWD(blk_)))
                        return off;
            #else
                    const auto off_ = off * sizeof(Word);
                    const auto&& atlas_ = (__Block)atlas;
                    const auto&& span = to_span(&atlas_[off_]);
                    ASSUME(span.data() == &atlas_[off_]);
                    if (negationwise_equivalent<byte, extent, D, N>(FWD(span), FWD(blk)))
                        return off;
            #endif // !ENABLE_SEGMENTS
                }
            #ifdef ENABLE_SEGMENTS
                else {
                    const auto off_ = abs_offset_for_seg(sz, off);
                    const auto&& atlas_ = (__Block)atlas;
                    const auto&& span = to_span(&atlas_[off_]);
                    ASSUME(span.data() == &atlas_[off_]);
                    if (negationwise_equivalent<byte, extent, D, N>(FWD(span), FWD(blk)))
                        return off;
                }
            #endif
                return std::nullopt;
            });
    }

    template<Direction D = Direction::F, Negation N = Negation::I>
    static inline constexpr PURE NONNULL HOT std::optional<AtlasDesc>
    try_chart_span(const WordAtlas& atlas, const size_t atlas_size,
                   const TypedIndices& indices, const Span&& blk) {
        ASSUME_ATLAS_SIZE(atlas_size);
        Hash hash = _hash_blk<D,N>((WordAtlasSlice)blk.data(), extent);
        auto idx = idx_for_sz(sz);
        const Index *const NO_ALIAS index = indices[idx][typed_index(D,N)];
        if (auto&& off = match_off<D,N>(hash, FWD(blk), *index, atlas, atlas_size)) {
            DBG("Charting @ %p: %.8x; %c, %c, %hhu (%u)",
                blk.data(), *off, dir_chr(D), neg_chr(N), idx, extent);
            return AtlasDesc(D, N, sz, *off);
        }
        else return std::nullopt;
    }

protected:
    static constexpr PURE NONNULL HOT std::optional<Desc>
    maybe_chart_span(const WordAtlas& atlas, const size_t atlas_size,
                     const TypedIndices& indices, const Span&& blk) {
        ASSUME_ATLAS_SIZE(atlas_size);
        // Try everything, preferring F to B, and Negation in enum order
    #define TCB(_D)                                              \
        if (!sole_direction || *sole_direction == Direction::_D) \
        { constexpr auto D = Direction::_D; APPLY_Negations(_TCB) }
    #define _TCB(Neg) {                                                                  \
        DBG("Matching blk @ %p; %u, %c, %c",                                             \
            blk.data(), extent, dir_chr(D), neg_chr(Negation::Neg));                     \
        if (auto&& desc =                                                                \
                try_chart_span<D,Negation::Neg>(atlas, atlas_size, indices, FWD(blk))) { \
            DBG("Matched blk @ %p; %u, %c, %c",                                          \
                blk.data(), extent, dir_chr(D), neg_chr(Negation::Neg));                 \
            return *desc;                                                                \
        }                                                                                \
        else DBG("Failed to match blk @ %p; %u, %c, %c",                                 \
                 blk.data(), extent, dir_chr(D), neg_chr(Negation::Neg));                \
    }
        APPLY_Directions(TCB)
    #undef TCB
    #undef _TCB
        return std::nullopt;
    }

public:
    static INLINE PURE NONNULL DescRope
    chart_span(const WordAtlas& atlas, const size_t atlas_size,
               const TypedIndices& indices, const Span&& blk) {
        ASSUME_ATLAS_SIZE(atlas_size);
        if (auto&& desc = Self::maybe_chart_span(atlas, atlas_size, indices, FWD(blk))) {
            auto&& rope = DescRope(*desc);
            ASSUME(rope.size() == 1);
            return FWD(rope);
        }
        // Iff we couldn't chart, then we try lower
        else return Self::chart_sub(atlas, atlas_size, indices, FWD(blk));
    }

    static INLINE PURE NONNULL DescRope
    chart_span(const WordAtlas& atlas, const size_t atlas_size,
               const TypedIndices& indices, const Block&& blk) {
        ASSUME_ATLAS_SIZE(atlas_size);
        return Self::chart_span(atlas, atlas_size, indices, Span(blk));
    }

    static INLINE PURE NONNULL DescRope
    chart_span(const WordAtlas& atlas, const size_t atlas_size,
               const TypedIndices& indices, const byte *NO_ALIAS blk) {
        ASSUME_ATLAS_SIZE(atlas_size);
        return Self::chart_span(atlas, atlas_size, indices, Span((__Block)blk, extent));
    }
};

// Forward-declare
template<SZ sz, SZK szk = szk_for_sz(sz)>
struct Chartor;

////////////////////////////////////////////////////////////////////////////////////////////////////

/// Nullary case; guaranteed to be a literal; may encode anything < lowest_seg_size
using LitChartor = Chartor<SZ::Lit, SZK::Lit>;

template<>
// We DON'T inherit from _Chartor because we don't need it
struct Chartor<SZ::Lit, SZK::Lit> final /*: _Chartor<SZ::Lit>*/ {
    template<typename uint = byte>
    static inline constexpr PURE std::enable_if_t<is_lit_size(sizeof(uint)), uint>
    chart_lit(const span<const byte, sizeof(uint)>&& blk)
    { return *LAUNDER((const uint*)blk.data()); }

    template<uint8_t size = LitDesc::max_size>
    static inline constexpr PURE std::enable_if_t<is_lit_size(size), LitDesc>
    chart_lit(const span<const byte, size>&& blk);
};

#define _IMPL_chart_lit(size, expr_using_blk) \
template<>                                    \
inline constexpr PURE LitDesc                 \
Chartor<SZ::Lit, SZK::Lit>::chart_lit<size>   \
(const span<const byte, size>&& blk)          \
{ return {size, (expr_using_blk)}; }

#define IMPL_chart_lit(width) \
_IMPL_chart_lit( (width/__CHAR_BIT__), chart_lit<uint##width##_t>(FWD(blk)) )

//#if sizeof(uint64_t) < sizeof(uintptr_t)
//IMPL_chart_lit(64)
//#endif
IMPL_chart_lit(32)
IMPL_chart_lit(16)
IMPL_chart_lit(8)

#undef IMPL_chart_lit

// 3 bytes has to be done separately
_IMPL_chart_lit(3, *(const uint32_t*)blk.data() & 0xFF'FF'FF )

#undef _IMPL_chart_lit

////////////////////////////////////////////////////////////////////////////////////////////////////

/// Base case; may result in literal
using _FstChartor = _Chartor<Chartor<first_sz, SZK::Fst>, first_sz>;
using FstChartor = Chartor<first_sz>;

template<>
struct Chartor<first_sz, SZK::Fst> final : _FstChartor {
    using typename _FstChartor::Span;

    template<uint8_t size = LitDesc::max_size>
    static INLINE constexpr PURE
    std::enable_if_t<is_lit_size(size), DescRope>
    chart_lit(const span<const byte, size>&& blk) {
        static_assert(LitDesc::max_size == 4, "HACK: taking advantage of small max_size");
        DBG("Charting literal @ %hhu, %p: %.8x", size, blk.data(), (uint32_t)*blk.data());
    #define LIT(offset, count) \
        DescRope(LitChartor::chart_lit<count>(blk.template subspan<offset,count>()))
        return LIT(0,size);
    #if 0
        // HACK: checking for even/odd, not power of 2; OK because our size is so small
        static_assert(LitDesc::max_size*2 == sizeof(Word)/2);
        constexpr Offset offset = size % 2;
        if constexpr (offset == 0) return LIT(0, 1);
        if constexpr (offset == 1) return DescRope(LIT(0, 1)) + DescRope(LIT(offset, size-offset));
    #endif
        UNREACHABLE;
    #if 0
        DescRope ret;
        if constexpr (offset) { // Odd
            ret = LIT(0,1);
            if constexpr (size == 1) return ret;
        }
        return ret + LIT(offset, size-offset);
        //else return ret + LIT(offset, size/2) + LIT(offset + size/2, size/2);
    #endif
    #undef LIT
    }

    // Inductive case
    template<Offset extent = lowest_size>
    static INLINE constexpr PURE
    std::enable_if_t<(extent > LitDesc::max_size && extent <= lowest_size), DescRope>
    chart_lit_span(const span<const byte, extent>&& blk) {
        constexpr auto extent_ = extent - LitDesc::max_size;
        static_assert(extent_ > 0);
        auto&& rope = chart_lit<LitDesc::max_size>(blk.template first<LitDesc::max_size>())
                    + chart_lit_span<extent_>(blk.template last<extent_>());
        return rope;
    }

    // Base case
    template<Offset extent>
    static INLINE constexpr PURE std::enable_if_t<is_lit_size(extent), DescRope>
    chart_lit_span(const span<const byte, extent>&& blk) { return chart_lit<extent>(FWD(blk)); }

    // Nullary case
    template<Offset extent>
    static consteval DescRope
    chart_lit_span(const span<const byte, 0>) { return DescRope(); }

    static INLINE PURE NONNULL DescRope
    chart_sub(const WordAtlas&, size_t, const TypedIndices&, const Span&& blk)
    { return chart_lit_span<lowest_size>(FWD(blk)); }
};

////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef ENABLE_SEGMENTS

/// Inductive case, for all but the first segment SZ
template<SZ sz, typename = std::enable_if_t<sz != first_sz && is_seg_sz(sz)>>
using _SegChartor = _Chartor<Chartor<sz, SZK::Seg>, sz>;

template<SZ sz>
struct Chartor<sz, SZK::Seg> final : _SegChartor<sz> {
    static_assert(szk_for_sz(sz) == SZK::Seg);
    using typename _SegChartor<sz>::Span;
    using _SegChartor<sz>::extent;

    static consteval auto sub_sz = sz-1;
    static consteval auto sub_extent = size_for_sz(sub_sz);
    using SubSpan = const SpanSZ<sub_sz>;

    // NB: Currently unused
    static consteval auto num_pieces = num_seg_pieces(sz);
    static consteval auto piece_idx  = piece_idx_for_seg_sz(sz);

public:
    static INLINE PURE NONNULL DescRope
    chart_sub(const WordAtlas& atlas, const size_t atlas_size,
              const TypedIndices& indices, const Span&& blk) {
        ASSUME_ATLAS_SIZE(atlas_size);
        // Is our sub in the same segment class?
        if constexpr (extent == sub_extent)
            return FWD(Chartor<sub_sz>::chart_span(atlas, atlas_size, indices, FWD(blk)));
        else {
            // NOTE: Intentionally mutable; fancier algo for fancier sizes in future fancier times
            constexpr auto leftovers = extent - sub_extent;
            static_assert(leftovers > 0);
            auto&& rope = Chartor<sub_sz>::chart_span(atlas, atlas_size, indices,
                                                      blk.template subspan<0, sub_extent>())
                /*...*/ +
                // FIXME: Suboptimal b/c we eagerly chart the literal vs. e.g. starting a new span
                FstChartor::chart_lit<leftovers>(blk.template subspan<sub_extent, leftovers>());
            return FWD(rope);
        }
    }
};

#endif // ENABLE_SEGMENTS
////////////////////////////////////////////////////////////////////////////////////////////////////

/// Inductive case, for all block sizes (except the first iff we're not using segments)
template<SZ sz, typename = std::enable_if_t<sz != first_sz && is_blk_sz(sz)>>
using _BlkChartor = _Chartor<Chartor<sz, SZK::Blk>, sz>;

template<SZ sz>
struct Chartor<sz, SZK::Blk> final : _BlkChartor<sz> {
public:
    static_assert(szk_for_sz(sz) == SZK::Blk);
    using typename _BlkChartor<sz>::Span;
    using _BlkChartor<sz>::extent;

    static consteval auto sub_sz = sz-1;
    static consteval auto sub_extent = size_for_sz(sub_sz);
    using SubSpan = const SpanSZ<sub_sz>;

    static consteval auto max_extent = highest_blk_size
                        , min_extent =  lowest_blk_size
                        , max_num_extents = max_extent/min_extent;
    static_assert(extent == min_extent || extent % sub_extent == 0);
    static consteval auto num_extents = extent/sub_extent;
    static_assert(num_extents <= max_num_extents || sz == first_blk_sz);

    using SubSpans = const std::array<const SubSpan, num_extents>;

private:
    template<Offset offset>
    static INLINE_ constexpr PURE SubSpan
    _mk_subspan(Span&& blk) { return FWD(blk.template subspan<offset, sub_extent>()); }

    template<Offset last_offset, Offset... offsets>
    static INLINE_ constexpr PURE SubSpans
    _mk_subspans(Span&& blk) {
        constexpr const uint8_t num_offsets = sizeof...(offsets)+1;
        if constexpr (num_offsets == num_extents)
            return FWD(SubSpans{_mk_subspan<offsets>(FWD(blk))...
                              , _mk_subspan<last_offset>(FWD(blk))});
        else return FWD(_mk_subspans<last_offset+sub_extent, offsets..., last_offset>(FWD(blk)));
    }

protected:
    static INLINE_ constexpr PURE SubSpans
    mk_subspans(Span&& blk) {
        return FWD(_mk_subspans<0>(FWD(blk)));
    #if 0
        SubSpans spans;
        auto offset = sub_extent;
        // Initialize spans, doling them out in sub_extent strides
        for (auto& s : spans) {
            s = blk.subspan<sub_extent>(offset);
            offset += sub_extent;
        }
        return spans;
    #endif
    }

public:
    static INLINE PURE NONNULL DescRope
    chart_sub(const WordAtlas& atlas, const size_t atlas_size,
              const TypedIndices& indices, const Span&& blk) {
        ASSUME_ATLAS_SIZE(atlas_size);
        DescRope ret;
        auto& subspans = FWD(mk_subspans(FWD(blk)));
        auto begin = std::make_move_iterator(subspans.begin());
        const auto end = std::make_move_iterator(subspans.end());
        // TODO: parallelize
        for (auto&& it = begin; it != end; it++)
            ret = ret + Chartor<sub_sz>::chart_span(atlas, atlas_size, indices, FWD(*it));
        return FWD(ret);
    }
};

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// Top-level Charting API
////////////////////////////////////////////////////////////////////////////////////////////////////

/// Inductive case, regardless of whether segments are enabled or not
template<SZ tgt_sz = last_sz>
struct TopChartor final {
    static_assert(is_sz_class(tgt_sz));
    // Forward-declaration
    static INLINE constexpr PURE NONNULL /*FLATTEN*/ HOT DescRope
    chart_span(const WordAtlas& atlas, const size_t atlas_size, const TypedIndices& indices,
               const Offset size, const byte *NO_ALIAS const blk) {
        ASSUME_ATLAS_SIZE(atlas_size);
        constexpr auto tgt_size = size_for_sz(tgt_sz);
        /// Invariant: will only ever be called with a size that is at most tgt_sz
        ASSUME(0 < size && size <= tgt_size, size, "lu");

        constexpr auto next_tgt_sz = tgt_sz-1;
        constexpr auto next_tgt_size = size_for_sz(next_tgt_sz);
        using NextTopChartor = TopChartor<next_tgt_sz>;

        // Iff the size is our tgt_size, we'll chart directly
        if (size == tgt_size) {
            DBG("Directly charting @ %u, %p", tgt_size, blk);
            return Chartor<tgt_sz>::chart_span(atlas, atlas_size, indices, blk);
        } else
        // Fastpath for if our given size is actually better suited to a smaller sz
        ifL (size <= next_tgt_size)
            return NextTopChartor::chart_span(atlas, atlas_size, indices, size, blk);
        else {
            /// Invariant: `tgt_size > size > next_tgt_size`, due to the above Invariant
            // Otherwise indirect with chart_span of next_tgt_size on a full blk and its leftovers
            const auto leftover_extent = size - next_tgt_size;
            DBG("Splitting chart @ %u -> %u, %p: %u", tgt_size, next_tgt_size, blk, size);
            //auto leftover_span = ::span(&blk[next_tgt_size], leftover_extent);
            auto&& rope = Chartor<next_tgt_sz>::chart_span(atlas, atlas_size, indices, blk)
                        + NextTopChartor::chart_span(atlas, atlas_size, indices,
                                                     leftover_extent, &blk[next_tgt_size]);
            return rope;
        }
    }
};

////////////////////////////////////////////////////////////////////////////////////////////////////

/// Nullary case
#if 0
template<>
struct TopChartor<SZ::Lit> final {
#endif
    /// Chart literal block by stepping everything lower than lowest_size until we find ours
    template<Offset leftover_size = lowest_size - 1>
    static INLINE constexpr PURE NONNULL /*FLATTEN*/
    std::enable_if_t<(leftover_size > 0 && leftover_size < lowest_size), DescRope>
    chart_lit(const Offset size, const byte *NO_ALIAS const blk) {
        ASSUME(0 < size && size <= leftover_size, size, "u");
        if (leftover_size == size) {
            DBG("Charting leftovers @ %u, %p", leftover_size, blk);
            const auto&& span = ::span<const byte, leftover_size>(blk, leftover_size);
            return FstChartor::template chart_lit_span<leftover_size>(FWD(span));
        }
        else if constexpr(leftover_size > 1) return chart_lit<leftover_size-1>(size,blk);
        else UNREACHABLE;
    }
#if 0
    static INLINE PURE NONNULL DescRope
    chart_span(MAYBE_UNUSED WordAtlas& atlas, MAYBE_UNUSED size_t atlas_size,
               MAYBE_UNUSED const TypedIndices& indices,
               Offset size, const byte *NO_ALIAS blk) {
    { return (chart_lit(size, blk)); }
};
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////

/// Base case
template<>
struct TopChartor<first_sz> final {
    static INLINE PURE NONNULL DescRope
    chart_span(const WordAtlas& atlas, const size_t atlas_size, const TypedIndices& indices,
               const Offset size, const byte *NO_ALIAS blk) {
        ASSUME_ATLAS_SIZE(atlas_size);
        ASSUME(size > 0 && size <= lowest_size, size, "u");
        if (size == lowest_size) return FstChartor::chart_span(atlas, atlas_size, indices, blk);
        else return chart_lit(size, blk);
            //return TopChartor<SZ::Lit>::chart_lit(size, blk);
    }
};

////////////////////////////////////////////////////////////////////////////////////////////////////
// Next-to-top-level charting functions, allowing for different means of output

static INLINE PURE NONNULL HOT DescRope // NOTE: Do not FLATTEN
top_chart_span(const WordAtlas& atlas, const size_t atlas_size, const TypedIndices& indices,
               const Offset size, const byte *NO_ALIAS const blk)
{ return TopChartor<>::chart_span(atlas, atlas_size, indices, size, blk); }

// FIXME
template<typename PerRope, typename T>
using ensure_conforming_PerRope =
    std::enable_if_t<result_conforms<bool, PerRope, const DescRope, Offset /*num_descs*/>, T>;

template<typename PerRope>
// Negative values are error codes
static NONNULL HOT ssize_t
//ensure_conforming_PerRope<PerRope, ssize_t>
chart(const WordAtlas& atlas, const size_t atlas_size, const TypedIndices& indices,
      const FILE& in, const PerRope&& per_rope) {
    ASSUME_ATLAS_SIZE(atlas_size);
    std::array<byte, highest_size> buf {(byte)0}; // This is the most we'll take in at a time
    size_t in_sz = 0;
    Offset num_descs = 0;
    DBG("Beginning of atlas: %s @ %p", wtox(atlas[0]), atlas);
    while(!feof(&in)) {
        ifU (cancelled) return -4;
        Offset buf_offset = 0;
        while(buf_offset < highest_size && !feof(&in)) {
            // NB: This entire loop is just to fill the buffer once
            const auto to_read = highest_size - buf_offset;
            const auto bytes_read = fread(&buf[buf_offset], 1, to_read, &in);
            ASSUME(bytes_read <= to_read, bytes_read, "lu");
            ifU (bytes_read < to_read) {
                ifU (feof(&in)) {
                    DBG("We buffered shorter than expected: %lu++%u; %m", bytes_read, to_read);
                } else
                ifU (ferror(&in)) {
                    // TODO: Error handling
                    ERR("Encountered error during charting @ %lu++%u: %m", bytes_read, to_read);
                    clearerr(&in);
                    return -1;
                } else {
                    ERR("We failed to read: %lu++%u; %m", bytes_read, to_read);
                    return -2;
                }
                // TODO: Inner retry/fill loop
            }
            DBG("Read: %s... [%zu]", wtox(*(Word*)buf.data()), bytes_read);
            buf_offset += bytes_read;
        }
        DBG("Charting %u after %lu @ %u", buf_offset, in_sz, num_descs);
        auto&& rope = top_chart_span(atlas, atlas_size, indices, buf_offset, buf.data());
        const auto rope_size = rope.size();
        ASSUME(rope_size);
        const bool success = per_rope(FWD(rope), num_descs);
        ifU (!success) {
            ERR("Failed charting %u @ %u, after %lu", buf_offset, num_descs, in_sz);
            return -3;
        }
        else DBG("Charted %u", rope_size);
        num_descs += rope_size;
        in_sz += buf_offset;
    }
    return in_sz;
}

/// NOTE: Does NOT account for Metadata
static NONNULL ssize_t
_chart(const WordAtlas& atlas, const size_t atlas_size, const TypedIndices& indices,
       const FILE& in, decltype(AZFile::body)& out) {
    const auto in_size = chart(atlas, atlas_size, indices, in,
        [&out](DescRope rope, Offset num_descs) {
            Offset off = 0;
            DescRopeBuilder::apply(rope, [&](Desc desc) -> bool {
                out[num_descs + off++] = FWD(desc);
                return true;
            });
            ASSUME(off > 0);
            FWD(rope).destroy();
            return true;
        });
    return in_size;
}

static NONNULL ssize_t
chart(const WordAtlas& atlas, const size_t atlas_size, const TypedIndices& indices,
      const FILE& in, AZ NO_ALIAS out) {
    const auto in_size = _chart(atlas, atlas_size, indices, in, out->body);
    ifL (in_size >= 0) {
        DBG("Done charting!");
        out->metadata = AZFile::Metadata { .version = current_version
                                         , .atlas_id = current_atlas_id
                                         , .decharted_size = (size_t)in_size };
    } else ERR("Error charting: %li", in_size);
    return in_size;
}

static constexpr const AZMeta dynamic_metadata = { .version = AZMeta::dyn_version
                                                 , .atlas_id = current_atlas_id
                                                 , .decharted_size = 0 }; // Dynamic size

static ssize_t
chart(const WordAtlas& atlas, const size_t atlas_size, const TypedIndices& indices,
      const FILE& in, int out_fd) {
    if (auto ret = write(out_fd, &dynamic_metadata, sizeof dynamic_metadata)
      ; UNLIKELY(ret != sizeof dynamic_metadata)) {
        ERR("Error writing metadata: %lX; %m", ret);
        return false;
    }
    auto in_size = chart(atlas, atlas_size, indices, in,
        [=](DescRope rope, MAYBE_UNUSED Offset num_descs) {
            bool ret = DescRopeBuilder::apply(rope, [&](Desc desc) -> bool {
                if (auto ret = write(out_fd, &desc, sizeof desc)
                  ; UNLIKELY(ret != sizeof desc)) {
                    ERR("Error writing desc @ %u: %x, %lX; %m", num_descs, desc, ret); return false;
                }
                return true;
            });
            FWD(rope).destroy();
            return LIKELY(ret);
        });
    return in_size;
}

static ssize_t
chart(const WordAtlas& atlas, const size_t atlas_size, const TypedIndices& indices,
      const FILE& in, FILE& out) {
    if (auto ret = fwrite(&dynamic_metadata, sizeof dynamic_metadata, 1, &out)
      ; UNLIKELY(ret != 1)) {
        ERR("Error writing metadata: %lX; %m", ret);
        return false;
    }
    const auto in_size = chart(atlas, atlas_size, indices, in,
        [&out](DescRope rope, MAYBE_UNUSED Offset num_descs) {
            bool ret = DescRopeBuilder::apply(rope, [&](Desc desc) -> bool {
                if (auto ret = fwrite(&desc, sizeof desc, 1, &out)
                  ; UNLIKELY(ret != 1)) {
                    ERR("Error writing desc @ %u: %x, %lX; %m", num_descs, desc, ret); return false;
                }
                return true;
            });
            FWD(rope).destroy();
            return LIKELY(ret);
        });
    return in_size;
}

#if 0
static ssize_t
chart(const WordAtlas& atlas, size_t atlas_size, const TypedIndices& indices,
      const FILE& in, DescOStream& out) {
    out.write(&dynamic_metadata, sizeof dynamic_metadata);
    ifU (out.rdstat() != std::ios_base::goodbit) {
        ERR("Error writing metadata; %m");
        return false;
    }
    const auto in_size = chart(atlas, atlas_size, indices, in,
        [=](const DescRope rope, MAYBE_UNUSED Offset num_descs) {
            for(const Desc desc: rope) {
                out.write(&desc, sizeof(desc));
                ifU (out.rdstate() != std::ios_base::goodbit) {
                    ERR("Error writing desc @ %u: %u; %m", num_descs, desc);
                    return false;
                }
            }
            return true;
        });
    return in_size;
}
#endif

// TODO: Refactor charting wrt in & out

#endif // ENABLE_CHARTING

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// Top-level decharting API
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef ENABLE_DECHARTING

template<typename Write, typename T>
using ensure_conforming_Write =
    std::enable_if_t<result_conforms<bool /*return Type*/, Write, const byte*, size_t>, T>;

template<typename Write>
static NONNULL ensure_conforming_Write<Write, bool>
__dechart(const WordAtlasSlice& atlas_chunk, Negation neg, Direction dir, Offset size,
          const Write&& write) {
    ASSUME_SZ_SIZE(size);
    const auto blk = as_bytes(span(atlas_chunk, &atlas_chunk[size-1]));
    maybe_madvise(const_cast<Word*>(atlas_chunk), size, MADV_WILLNEED | MADV_SEQUENTIAL);
    if (dir == Direction::F) {
        return std::all_of(blk.cbegin(), blk.cend(),
            [&,neg](auto w) -> bool {
                ifU (cancelled) return false;
                auto w_ = negate(w, neg);
                ASSUME_EQ_IFF_IDENTITY(neg, w, w_);
                return write((const byte*)&w_, sizeof(w_));
            });
    } else {
        ASSUME(dir == Direction::B);
        return std::all_of(blk.crbegin(), blk.crend(),
            [&,neg](auto _w) -> bool {
                ifU (cancelled) return false;
                auto w = reverse_bits(_w);
                auto w_ = negate(w, neg);
                ASSUME_EQ_IFF_IDENTITY(neg, w, w_);
                return write((const byte*)&w_, sizeof(w_));
            });
    }
}

/// 0 iff error
template<typename Write>
static inline NONNULL HOT ensure_conforming_Write<Write, size_t>
dechart_desc(Desc _desc, const WordAtlas& atlas, const Write&& write) {
    ifU (_desc.is_lit()) {
        const auto&& lit_desc = FWD(_desc.l);
        const auto size = lit_desc.size();
        DBG("Decharting literal: %u, %hhu", lit_desc.lit, size);
        ASSUME(is_lit_size(size), size, "hhu");
        ifU (!write(reinterpret_cast<const byte*>(&lit_desc.lit), size)) {
            ERR("Error writing decharted literal: %u; %m", lit_desc.lit);
            return 0;
        }
        return size;
    } else {
        auto&& desc = FWD(_desc.a);
        const auto size = size_for_sz(desc.sz);
        const auto stride = stride_for_sz(desc.sz);
        const auto off = offset_for_sz(desc.sz, desc.off);
        DBG("Decharting: %lu, %u, %c, %c",
            off, size, dir_chr(desc.dir), neg_chr(desc.neg));
        ifU (!__dechart(&atlas[off], desc.neg, desc.dir, size, FWD(write))) {
            ERR("Error writing decharted desc: %lu, %u, %c, %c; %m",
                off, size, dir_chr(desc.dir), neg_chr(desc.neg));
            return 0;
        }
        return size;
    }
}

template<typename Write>
static NONNULL
ensure_conforming_Write<Write, bool>
_dechart(const WordAtlas& atlas, const AZ NO_ALIAS in, uint32_t num_desc, const Write&& write) {
    for(decltype(num_desc) i=0; i < num_desc; i++)
        ifU (!dechart_desc(in->body[i], atlas, FWD(write))) return false;
    return true;
}

// TODO: Intake from buffer/pipe

/// NOTE: `WRITE` is a headless lambda
#define DEF_DECHART(InArg, OutArg, WRITE)                                \
static inline NONNULL bool                                               \
dechart(const WordAtlas& atlas, InArg in, uint32_t num_desc, OutArg out) \
{ return _dechart(atlas, in, num_desc, [&out](const byte *bytes, size_t size) -> bool WRITE); }

DEF_DECHART(const AZ NO_ALIAS, ByteOStream&,
{ out.write(bytes, size); return LIKELY(out.rdstate() == std::ios_base::goodbit); })

DEF_DECHART(const AZ NO_ALIAS, const int /*fd*/,
{ return (size_t)::write(out, bytes, size) == size; })

DEF_DECHART(const AZ NO_ALIAS, byte *NO_ALIAS, { std::memcpy(out, bytes, size); return true; })

// TODO: Refactor decharting wrt in

#undef DEF_DECHART

#endif // ENABLE_DECHARTING

//} // namespace atlas

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// UI
////////////////////////////////////////////////////////////////////////////////////////////////////

//using namespace atlas;

enum class Mode : char {
    H='h', /// Help
#ifdef ENABLE_CHARTING
    C='c', /// Chart
#endif
#ifdef ENABLE_DECHARTING
    D='d', /// Dechart
#endif
#ifdef ENABLE_INDEXING
# ifdef ENABLE_COUNTING
    K='k', KK='K', /// Create counting index (CIndex)
# endif
# ifdef ENABLE_VINDEX
    V='v', VV='V', /// Create variable index (VIndex)
# endif
# ifdef ENABLE_ALMANAC
    A='a', AA='A', /// Create almanac
    I='i', II='I', /// Create almanac-using index (AIndex)
# endif
#endif // ENABLE_INDEXING
#ifdef ENABLE_QUERIES
    Q='q',
#endif
};

static constexpr const char *const ATLAS_FLAGS =
    "h"
#ifdef ENABLE_CHARTING
    " c"
#endif
#ifdef ENABLE_DECHARTING
    " d"
#endif
#ifdef ENABLE_INDEXING
# ifdef ENABLE_COUNTING
    " kK"
# endif
# ifdef ENABLE_VINDEX
    " vV"
# endif
# ifdef ENABLE_ALMANAC
    " aA iI"
# endif
#endif //ENABLE_INDEXING
#ifdef ENABLE_QUERIES
    " q"
#endif
    ;

#ifdef ENABLE_QUERIES
enum class QProp : char {
    H='h', // Hash of atlas @ ($dir $neg $sz $index)
    L='l', // List of hash-equivalent index entries @ {$dir $neg $size $hash}
    I='i', // Check the index for integrity @ {$dir $neg $sz}
# ifdef ENABLE_INDEXING
    C='c', // Count of atlas bucket from cndex @ ($dir $neg $sz $hash)
    S='s', // Stats of cndex @ {$dir $neg $sz}
#  ifdef ENABLE_ALMANAC
    R='r', // Running count of in-progress almanac-using index generation @ ($dir $neg $sz $hash)
#  endif // ENABLE_ALMANAC
# endif // ENABLE_INDEXING
# ifdef ENABLE_ALMANAC
#  ifndef NDEBUG
    V='v', // Verify validity of complete almanac @ {$dir $neg $sz}
#  endif
# endif // ENABLE_ALMANAC
};
#endif // ENABLE_QUERIES

#ifndef SINGULAR_ATLAS
# define ATLAS_ARG "$atlas "
#else
# define ATLAS_ARG ""
#endif // !SINGULAR_ATLAS

static constexpr const char *const _usage =
#ifdef SINGULAR_ATLAS
    "Compiled with a baked-in atlas of size " STRINGIFY(SINGULAR_ATLAS_SIZE) " bytes\n"
#elif defined(SINGULAR_ATLAS_SIZE)
    "Only supports atlases of size " STRINGIFY(SINGULAR_ATLAS_SIZE) " bytes\n"
#endif
    "Usage:\n"
    "\t# atlas [%s] " /*<- ATLAS_FLAGS*/ ATLAS_ARG "{$in $out}\n"
    "\tCWD taken to be index root\n\n"
    "# atlas h\n"
    "Display this usage message\n"
    "\n"
#ifdef ENABLE_CHARTING
    "# atlas c " ATLAS_ARG "{$in|-(stdin)} {$out?/stdout(-)}\n"
    "Chart $in to $out\n"
    "\n"
#endif
#ifdef ENABLE_DECHARTING
    "# atlas d " ATLAS_ARG "{$in|-(stdin)} {$out?/stdout(-)}\n"
    "Dechart $in to $out\n"
    "\n"
#endif
#ifdef ENABLE_INDEXING
# ifdef ENABLE_COUNTING
    "# atlas [kK] " ATLAS_ARG "\n"
    "Generate c(k)ount index (\"" CINDEX_NAME "\") into CWD"
    "; 'K' to continue\n"
    "\n"
# endif
# ifdef ENABLE_VINDEX
    "# atlas [vV] " ATLAS_ARG "\n"
    "Generate variable-length index "
    "(\"" VINDEX_NAME "\" & \"" TYPEDEX_NAME "\" & \""
        APPENDIX_NAME "\" & \"*" APPENDIX_SUFFIX "\"...)"
    " into CWD"
    "; 'V' to continue\n"
    "\n"
# endif
# ifdef ENABLE_ALMANAC
    "# atlas [aA]\n"
    "Export existing count index as an Almanac "
    "(\"" ALMANAC_NAME "\" & \"" ZEROES_NAME "\") into CWD"
    "; not resumable"
    "; 'A' to also destroy cndex\n"
    "\n"
    "# atlas [iI] " ATLAS_ARG "\n"
    "Generate almanac-using index (\"" AINDEX_NAME "\") into CWD"
    "; 'I' to continue\n"
    "\n"
# endif
#endif // ENABLE_INDEXING
#ifdef ENABLE_QUERIES
    "# atlas q " ATLAS_ARG "$prop {$arg...}\n"
    "Query ${prop}erties of the atlas/indices:\n"
    "h : Hash of atlas @ {$dir $neg $size $index}\n"
    "l : List of hash-equivalent index entries @ {$dir $neg $size $hash}\n"
# ifndef NDEBUG
    "i : Check the index for integrity @ {$dir $neg $sz}\n"
# endif
# ifdef ENABLE_INDEXING
#  ifdef ENABLE_COUNTING
    "c : Count of atlas bucket from counting index @ {$dir $neg $size $hash}\n"
    "s : Stats of cndex @ {$dir $neg $sz}\n"
#  endif // ENABLE_COUNTING
#  ifdef ENABLE_ALMANAC
    "r : Running count of in-progress almanac-using index generation @ {$dir $neg $size $hash}\n"
#  endif // ENABLE_ALMANAC
# endif // ENABLE_INDEXING
# ifdef ENABLE_ALMANAC
#  ifndef NDEBUG
    "v : Verify validity of complete almanac @ {$dir $neg $sz}\n"
#  endif
# endif // ENABLE_ALMANAC
    "\n"
#endif // ENABLE_QUERIES
    "\nEnvironment variables:\n"
    SOLE_DIRECTION_ENV_VAR ":\t Which direction, if any, to be the sole used\n"
    //SOLE_NEGATION_ENV_VAR ":\t Which negation, if any, to be the sole used\n"
    SOLE_SIZE_ENV_VAR ":\t Which size, if any, to be the sole used\n"
#ifdef ENABLE_INDEXING
    WORK_ENV_VAR ":\t Working directory or block device\n"
    DDIR_ENV_VAR ":\t Destination directory\n"
    WORK_OFF_ENV_VAR ":\t Offset from which to be use the working block device\n"
#endif
#ifdef DEBUG
    DEBUG_ENV_VAR ": Set fine-grained debug options (see source code)\n"
#endif
    "\n"
    "\nSignals:\n"
    "SIGINT, SIGQUIT: Cancel and sync\n"
    "SIGHUP: Sync\n"
    "\n"
    "\nLICENSE:\n" LICENSE
    "\n";

static void COLD usage() { __MSG(stderr, _usage, ATLAS_FLAGS); }

static void COLD
signal_handler(const int signum) { switch(signum) {
case SIGINT:
case SIGQUIT:
case SIGPIPE:
case SIGBUS:
    MSG("\nInterrupted; cleaning up");
    cancelled = true;
[[fallthrough]];
case SIGHUP:
    sync();
[[fallthrough]];
default:
    DBG_(signum, "d");
}}

int main(const int argc, const char *argv[]) {
#define admonish_user(...) VA_SELECT(admonish_user, __VA_ARGS__)
#define admonish_user_2(ret, msg) { ERR(msg "\n"); usage(); return ret; }
#define admonish_user_1(ret) { DBG_HERE; usage(); return ret; }
    DBG_(argc, "d");
    constexpr const auto atlas_arg_bias =
    #ifdef SINGULAR_ATLAS
        0;
    #else
        1;
    #endif
    int num_args = argc - 1 /* for the progname */;
    if (num_args < 1 + atlas_arg_bias) admonish_user(1);
    auto&& get_arg = [&,argc](uint8_t arg) { return argv[arg + (argc - num_args)]; };

    // HORRIBLE HACK: first letter of first arg determines mode
    const auto mode_chr = get_arg(0); num_args--;
    if (strlen(mode_chr) != 1) admonish_user(1, "Excpecting mode to be a single character");
    const auto mode = (Mode)mode_chr[0];
    //DBG_(mode, "c");

    static struct sigaction sigact;
    sigact.sa_handler = signal_handler;
    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags = SA_RESTART;

#define REGISTER_SIG(S)                               \
    ifU (sigaction(SIG##S, &sigact, nullptr) == -1) { \
        ERR("Failed to register SIG" #S ": %m");      \
        return 3 + SIG##S;                            \
    }
    REGISTER_SIG(INT)
    REGISTER_SIG(QUIT)
    REGISTER_SIG(PIPE)
    REGISTER_SIG(BUS)
    REGISTER_SIG(HUP)
#undef REGISTER_SIG

    umask(0022);

    // Set options from any environment variables (iff necessary)
    switch(mode) {
    case Mode::H: usage(); return 0;
    default: {
        if (const auto s = secure_getenv(SOLE_DIRECTION_ENV_VAR)) {
            if (const auto d = parse_direction(s)) {
                DBG("Using sole direction: %c", dir_chr(*d));
                sole_direction = d;
            } else WRN(SOLE_DIRECTION_ENV_VAR " set to invalid direction: %s", s);
        }

        if (const auto s = secure_getenv(SOLE_SIZE_ENV_VAR)) {
            if (const auto size = parse_sz_size(s)) {
                DBG("Using sole size: %u", *size);
                sole_size = size;
            } else WRN(SOLE_SIZE_ENV_VAR " set to invalid size: %s", s);
        }

    #ifdef ENABLE_INDEXING
        auto&& maybe_set_dir = [](const auto env_var, int& dir_fd, const bool can_blkdev) {
            if (auto s = secure_getenv(env_var)) {
                DBG("%s = %s", env_var, s);
                if (auto dir = opendir(s)) {
                    dir_fd = dirfd(dir);
                    ifL (dir_fd != -1) {
                        DBG("Setting dir: %s ; %d", s, dir_fd);
                        return;
                    }
                } else if (can_blkdev) {
                    dir_fd = open(s, O_RDWR | O_NOATIME);
                    ifL (dir_fd != -1) { ifL(is_blkdev(dir_fd)) {
                        DBG("Setting blkdev: %s ; %d", s, dir_fd);
                        ASSUME(!strcmp(env_var, WORK_ENV_VAR));
                        if (auto s = secure_getenv(WORK_OFF_ENV_VAR)) {
                            if (auto off = parse_size(*s)) work_blkdev_off = *off;
                            else WRN(WORK_ENV_VAR " set to invalid size: %s", s);
                        }
                        else work_blkdev_off = 0;
                        DBG("work_blkdev_off: %zu", *work_blkdev_off);
                        return;
                    } else {
                        WRN("Failed setting %s blkdev: %s ; %m", env_var, s);
                        dir_fd = -1;
                    }}
                }
                WRN("Failed setting %s dir: %s ; %m", env_var, s);
            }
        };
        maybe_set_dir(WORK_ENV_VAR, work_fd, true);
        maybe_set_dir(DDIR_ENV_VAR, dest_dir_fd, false);
    #endif

    #ifdef DEBUG
        ifU (debug_magic = secure_getenv(DEBUG_ENV_VAR))
            DBG("Using debug magic: %s", debug_magic);
    #endif
    }}

    // Do we need the atlas/almanac?
    switch(mode) {
#ifdef ENABLE_INDEXING
# define CASES(M, creation, ...)                                                      \
    case Mode::M:    return create_##creation##_in_cwd<false>( __VA_ARGS__ ) ? 0 : 1; \
    case Mode::M##M: return create_##creation##_in_cwd<true >( __VA_ARGS__ ) ? 0 : 1;
# ifdef ENABLE_ALMANAC
    CASES(A, almanacs)
# endif
// We reuse CASES further below
#endif // ENABLE_INDEXING
    default: break;
    }

#ifndef SINGULAR_ATLAS
    const auto atlas_path = get_arg(0); num_args--;
    DBG_(atlas_path, "s");
    const int atlas_fd = open(atlas_path, O_RDONLY | O_NOATIME);
    struct stat atlas_stat;
    ifU (fstat(atlas_fd, &atlas_stat) != 0) {
        ERR("Failed to stat %s: %m", atlas_path);
        return 2;
    }
    const size_t atlas_size = get_file_size(atlas_fd, atlas_stat);
    MSG("Atlas: %s (%zu bytes) [max %zu]", atlas_path, atlas_size, max_atlas_size);
# ifndef SINGULAR_ATLAS_SIZE
    ASSUME_ATLAS_SIZE(atlas_size);
# else
    ifU (atlas_size != ATLAS_SIZE) {
        ERR("Given atlas of size %zu whereas expected %zu", atlas_size, ATLAS_SIZE);
        return 2;
    }
# endif // !SINGULAR_ATLAS_SIZE
    const int atlas_madv_flags =
    #ifdef ENABLE_INDEXING
        // TODO: Include Mode::V
        (mode == Mode::I || mode == Mode::II) ? (MADV_SEQUENTIAL | MADV_WILLNEED) :
    #endif
        MADV_RANDOM;
    const auto& atlas = mmap_atlas<Word>(atlas_fd, atlas_size, atlas_madv_flags);
# ifndef NDEBUG
    ifU (!&atlas) admonish_user(1, "Failed to get atlas");
# endif
    // atlas_fd closed
#endif // !SINGULAR_ATLAS

    switch (mode) {

#ifdef ENABLE_INDEXING
# ifdef ENABLE_COUNTING
    CASES(K, cindices, atlas, atlas_size)
# endif
# ifdef ENABLE_VINDEX
    CASES(V, vindices, atlas, atlas_size)
# endif
# ifdef ENABLE_ALMANAC
    CASES(I, aindices, atlas, atlas_size)
# endif
# undef CASES
#endif // ENABLE_INDEXING

#ifdef ENABLE_QUERIES
    case Mode::Q: {
        const auto prop_chr = get_arg(0); num_args--;
        if (strlen(prop_chr) != 1) admonish_user(1, "Expected query property");
        const auto prop = (QProp)prop_chr[0];

        // TODO: Currently all properties' arguments are prefixed with {$dir $neg $sz}

        #define PARSE_DIRECTION(argn, errno)                      \
            const auto _d = parse_direction(get_arg(argn));       \
            ifU (!_d) admonish_user(errno, "Expected direction"); \
            const auto d = *_d;

        #define PARSE_NEGATION(argn, errno)                      \
            const auto _n = parse_negation(get_arg(argn));       \
            ifU (!_n) admonish_user(errno, "Expected negation"); \
            const auto n = *_n;

        #define PARSE_SZ(argn, errno)                                       \
            const auto _sz = parse_sz(get_arg(argn));                       \
            ifU (!_sz) admonish_user(errno, "Size must be a SZClass size"); \
            const auto sz = *_sz;

        #define PARSE_HASH(argn, errno)                                                        \
            const auto _hash = parse_size(get_arg(argn), 16);                                  \
            ifU (!_hash || *_hash > max_Hash) admonish_user(errno, "Hash must be 32-bit hex"); \
            const Hash&& hash = *_hash;

        switch (prop) {
        // Hash of atlas @ {$dir $neg $size $index}
        case QProp::H: {
            if (num_args != 4) admonish_user(1);

            PARSE_DIRECTION(0, -1)
            PARSE_NEGATION(1, -2)

            /// NB: For now must also coincidentally be a SZ size
            const auto _size = parse_size(get_arg(2));
            if (!_size || lowest_size > *_size || *_size > max_Offset)
                admonish_user(-3, "Size must be >= lowest_size & <= max_Offset");
            const auto size = (Offset)*_size;

            const auto _idx = parse_size(get_arg(3));
            if (!_idx || *_idx > max_Offset) admonish_user(-4, "Index must be valid");
            const auto idx = (Offset)*_idx;

            #define PER_NEG(Neg) ((n == Negation::Neg) && ((d == Direction::F) \
            ? _hash_blk<Direction::F, Negation::Neg>(atlas[idx], size) \
            : _hash_blk<Direction::B, Negation::Neg>(atlas[idx], size))) ||
            const auto hash = APPLY_Negations(PER_NEG) 0;
            #undef PER_NEG
            MSG("Hash: %.8x", hash);
        #ifndef DEBUG
            return 0;
        #else
            return hash;
        #endif
        }
        // List of hash-equivalent index entries @ {$dir $neg $size $hash}
        case QProp::L: {
            if (num_args != 4) admonish_user(1);

            PARSE_DIRECTION(0, -1)
            PARSE_NEGATION(1, -2)
            PARSE_SZ(2, -3)
            PARSE_HASH(3, -4)

            const TypedIndices& indices = indices_from_cwd(true);
            for_SZ(sz, [&,d,n,hash](const SZ sz) -> void {
                const auto sz_idx = idx_for_sz(sz);
                const auto index = indices[sz_idx][typed_index(d,n)];
                MSG("Indexed (%c,%c,%hhu[%u]) atlas offsets:",
                    dir_chr(d), neg_chr(n), sz_idx, size_for_sz(sz));
                ifU (!index) { WRN("Failed to find index [%hhu]", sz_idx); }
                // Utilize first_satisfying_offset just to print, ignoring the result
                else index->first_satisfying_offset(hash, [](Offset idx) -> std::optional<bool> {
                    MSG("%.8x", idx);
                    // Unconditionally return nothing so we visit all possible subentries
                    return std::nullopt;
                });
            });
            delete[] &indices;
            return 0;
        }
    #ifndef NDEBUG
        // Check the index for integrity @ {$dir $neg $sz}
        case QProp::I: {
            if (num_args != 3) admonish_user(1);

            PARSE_DIRECTION(0, -1)
            PARSE_NEGATION(1, -2)
            PARSE_SZ(2, -3)

            const TypedIndices& indices = indices_from_cwd(true);
            bool all_valid = true;

            for_SZ(sz, [&,n,d](const SZ sz) -> void {
                const auto sz_idx = idx_for_sz(sz);
                const auto index = indices[sz_idx][typed_index(d,n)];
                DBG("Index %c,%c,%hhu[%u]", dir_chr(d), neg_chr(n), sz_idx, size_for_sz(sz));
                ifU (!index) { WRN("Failed to find index [%hhu]", sz_idx); }
                else {
                    const auto& atlas_ = reinterpret_cast<ByteAtlas&>(*atlas);
                #define PER_NEG(Neg) ((n == Negation::Neg) && ((d == Direction::F) \
                ? index->validate<Direction::F, Negation::Neg>(atlas_, sz) \
                : index->validate<Direction::B, Negation::Neg>(atlas_, sz))) ||
                    bool valid = APPLY_Negations(PER_NEG) false;
                #undef PER_NEG
                    if (!valid)
                        WRN("Index %c/%c,%u is invalid!", dir_chr(d), neg_chr(n), size_for_sz(sz));
                    all_valid &= valid;
                }
            });
            delete[] &indices;
            return all_valid ? 0 : 1;
        }
    #endif // !NDEBUG
    #ifdef ENABLE_INDEXING
    # ifdef ENABLE_COUNTING
        // Count of atlas bucket from cndex @ {$dir $neg $size $hash}
        case QProp::C: {
            if (num_args != 4) admonish_user(1);

            PARSE_DIRECTION(0, -1)
            PARSE_NEGATION(1, -2)
            PARSE_SZ(2, -3)
            PARSE_HASH(3, -4)

            const auto _indices = gen_cindices(false, true);
            ifU (!_indices)
                admonish_user(-5, "Error finding CIndices from current working directory");
            const TypedCIndices& indices = *_indices;

            for_SZ(sz, [&,d,n,hash](const SZ sz) -> void {
                const auto idx = idx_for_sz(sz);
                const auto index = indices[idx][typed_index(d,n)];
                ifU (!index) { WRN("Failed to find index [%hhu]", idx); }
                else {
                    const auto count = index->find_entry(hash);
                    MSG("Count [%hhu]: %hhu", idx, count);
                    delete index;
                }
            });
            delete[] &indices;
            return 0;
        }
        // Stats of cndex @ {$dir $neg $sz}
        case QProp::S: {
            if (num_args != 3) admonish_user(1);

            PARSE_DIRECTION(0, -1)
            PARSE_NEGATION(1, -2)
            PARSE_SZ(2, -3)

            const auto _indices = gen_cindices(false, true);
            ifU (!_indices)
                admonish_user(-4, "Error finding CIndices from current working directory");
            const TypedCIndices& indices = *_indices;

            for_SZ(sz, [&,d](const SZ sz) -> void {
                const auto idx = idx_for_sz(sz);
                const auto index = indices[idx][typed_index(d,n)];
                ifU (!index) { WRN("Failed to find index [%hhu]", idx); }
                else {
                    index->print_counts();
                    delete index;
                }
            });
            delete[] &indices;
            return 0;
        }
    # endif // ENABLE_COUNTING
    # ifdef ENABLE_ALMANAC
        // Running count of in-progress almanac-using index generation @ {$dir $neg $size $hash}
        case QProp::R: {
            if (num_args != 4) admonish_user(1);

            PARSE_DIRECTION(0, -1)
            PARSE_NEGATION(1, -2)
            PARSE_SZ(2, -3)
            PARSE_HASH(3, -4)

            const TypedIndices& indices = indices_from_cwd(true);

            for_SZ(sz, [&,d,n,hash](const SZ sz) -> void {
                const auto idx = idx_for_sz(sz);
                const auto index = indices[idx][typed_index(d,n)];
                ifU (!index) { WRN("Failed to find index [%hhu]", idx); }
                else {
                    const auto count = index->recover_count(hash);
                    MSG("Running count [%hhu]: %hhu", idx, count);
                    delete index;
                }
            });
            delete[] &indices;
            return 0;
        }
    # endif // ENABLE_ALMANAC
    #endif // ENABLE_INDEXING
    #ifdef ENABLE_ALMANAC
    # ifndef NDEBUG
        // Verify validity of complete almanac @ {$dir $neg $sz}
        case QProp::V: {
            if (num_args != 3) admonish_user(1);

            PARSE_DIRECTION(0, -1)
            PARSE_NEGATION(1, -2)
            PARSE_SZ(2, -3)

            bool verified = true;
            for_SZ(sz, [&,d](SZ sz) -> void {
                ifU (!verified) return;
                const auto idx = idx_for_sz(sz);
                auto appended = append_mkdir_current_path(sz);
                appended += append_mkdir_current_path(d);
                appended += append_mkdir_current_path(n);
                const auto almanac = gen_almanac(false, true);
                ifU (!almanac) { WRN("Failed to gen_almanac [%hhu]", idx); }
                else {
                    clear_trailing_current_path(appended);
                    verified &= almanac->verify();
                    ifU (!verified) MSG("Failed verifiying [%hhu]", idx);
                    delete almanac;
                }
            });
            if (verified) {
                MSG("Verification successful!");
                return 0;
            } else {
                MSG("Failed verification!");
                return 1;
            }
        }
    # endif // !NDEBUG
    #endif // ENABLE_ALMANAC
        default: admonish_user(1, "Invalid query property");
        }
        break;
    }
#endif // ENABLE_QUERIES

    case Mode::H:
#if defined ENABLE_ALMANAC && defined ENABLE_INDEXING
    case Mode::A: case Mode::AA:
#endif
        UNREACHABLE;

    default: break;
    }

    int ret = 0;

#if defined ENABLE_CHARTING || defined ENABLE_DECHARTING
    const auto&& std_or_arg = [&](int stdfd, uint8_t argi, bool readonly) -> int { // fd
        const auto name = get_arg(argi);
        return strlen(name) == 1 && !strncmp(name, "-", 1)
               ? stdfd
               : readonly
                 ? open(name, O_RDONLY | O_CLOEXEC)
                 : open(name, O_RDWR | O_CREAT | O_CLOEXEC, 0644);
    };
    const int in_fd = std_or_arg( STDIN_FILENO, 0, true)
           , out_fd = std_or_arg(STDOUT_FILENO, 1, false);
    const auto&& cleanup = [=]{ close(in_fd); close(out_fd); };

#define LOAD_FD(pfx) \
    const auto _##pfx##_stat = get_file_stats(pfx##_fd); \
    ifU (!_##pfx##_stat) { ERR("Invalid " #pfx "_fd: %d", pfx##_fd); cleanup(); return -2; } \
    const auto& pfx##_stat = *_##pfx##_stat; \
    const bool pfx##_pipe = is_pipe(pfx##_stat);
    LOAD_FD(in)
    LOAD_FD(out)
    //LOAD_FD(meta)
#undef LOAD_FD

    switch(mode) {
# ifdef ENABLE_CHARTING
    // TODO: pipes
    case Mode::C: {
        if (in_pipe) {
            // TODO
        } else {
            auto& in = *fdopen(in_fd, "r");
            auto out_size = get_file_size(out_fd, out_stat);
            if (!out_size) {
                // TODO: Inappropriate if not accounting for output
                ftruncate(out_fd, sizeof(AZFile));
                out_size = sizeof(AZFile);
            }
            AZ out = mmap_out<AZFile>(out_fd, out_size, MADV_SEQUENTIAL);
            const auto& indices = indices_from_cwd(true);
            ifU (!chart(atlas, atlas_size, indices, in, out)) ret = 2;
            delete &indices;
        }
        break;
    }
# endif // ENABLE_CHARTING
# ifdef ENABLE_DECHARTING
    // TODO: pipes
    case Mode::D: {
        if (in_pipe) {
            // TODO
        } else {
            const auto in_size = get_file_size(in_fd, in_stat);
            const auto& in = *mmap_in<AZFile>(in_fd, in_size, MADV_SEQUENTIAL);
            const auto num_descs = (in_size - sizeof(AZMeta)) / sizeof(Desc);
            const auto out_size = get_file_size(out_fd, out_stat) ?: in.metadata.decharted_size;
            if (out_size) {
                ftruncate(out_fd, out_size);
                auto out = mmap_out<byte>(out_fd, out_size);
                ifU (!dechart(atlas, in, num_descs, out)) ret = 2;
            } else {
                // TODO: Dynamic size
            }
        }
        break;
    }
# endif // ENABLE_DECHARTING

    case Mode::H:
# ifdef ENABLE_INDEXING
#  ifdef ENABLE_COUNTING
    case Mode::K: case Mode::KK:
#  endif
#  ifdef ENABLE_VINDEX
    case Mode::V: case Mode::VV:
#  endif
#  ifdef ENABLE_ALMANAC
    case Mode::I: case Mode::II:
    case Mode::A: case Mode::AA:
#  endif
# endif // ENABLE_INDEXING
# ifdef ENABLE_QUERIES
    case Mode::Q:
# endif
        UNREACHABLE;
    default: admonish_user(1, "Unrecognized mode");
    }

    cleanup();
#endif // defined ENABLE_CHARTING || defined ENABLE_DECHARTING

    sync();
    return ret;
#undef admonish_user
}
