# ©120[17-20] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats
# >= GCC 10

# Default to typical pagesize
LAST_SZ ?= 4K
_OPTIMIZE ?= SPEED # Or SIZE or DEBUG or XTREME

SRCS := atlas.c++
HDRS := atlas.h++

LIBS := -lstdc++

GCC ?= g++
CC := $(GCC)
LD ?= ld

# Unfortunately there are many options that will silently overflow/wrap a 30+ bit value into 32
max_gcc_num := $(shell echo "2^30" | bc)
max_gcc_nines := 999999

####################################################################################################

DEFNS += -D LAST_SZ=_$(LAST_SZ)
DEFNS += -D ENABLE_SEGMENTS=8
DEFNS += -D ENABLE_COUNTING
#DEFNS += -D ENABLE_VINDEX
DEFNS += -D ENABLE_ALMANAC

DEFNS += -D ENABLE_INDEXING
DEFNS += -D ENABLE_QUERIES
#DEFNS += -D ENABLE_CHARTING
#DEFNS += -D ENABLE_DECHARTING

DEFNS += -D SPACE_EFFICIENT_NIBSET
ifdef EMBARALLEL
# This typically won't help you since everything is very I/O bound
DEFNS += -D EMBARALLEL
endif

DEFNS += -D OPTIMIZE_$(_OPTIMIZE)
ifeq ($(_OPTIMIZE),XTREME)
OPTIMIZE ?= SPEED # Same semantics in the Makefile as SPEED
CPPFLAGS += -Wno-cpp -Wno-sign-compare -Wno-narrowing -Wno-attributes
#else ifeq ($(_OPTIMIZE),SPEED)
#else ifeq ($(_OPTIMIZE),SIZE)
else ifeq ($(_OPTIMIZE),DEBUG)
CFLAGS += -Og -ggdb3
endif
OPTIMIZE ?= $(_OPTIMIZE)

CPPFLAGS += -pipe
CPPFLAGS += -frecord-gcc-switches -fmax-errors=1 #$(shell nproc)

ifneq ($(OPTIMIZE),DEBUG)
CFLAGS += -O2
endif

#DEFNS += -D XXH_INLINE_ALL
#DEFNS += -D XXH_STATIC_LINKING_ONLY
#DEFNS += -D XXH_NO_LONG_LONG  # We only use 32-bit xxhash
#DEFNS += -D XXH_FORCE_MEMORY_ACCESS=2  # Only beneficial for certain targets
#DEFNS += -D XXH_FORCE_NATIVE_FORMAT  # Only relevant on certain big-endian archs

CFLAGS += -fshort-enums -funsigned-bitfields
CFLAGS += -fopenmp -fsimd-cost-model=unlimited
CFLAGS += -Wdisabled-optimization -Wall -Wextra -Wopenmp-simd
CFLAGS += -Wno-ignored-qualifiers -Wno-unused-function
CFLAGS += -march=native -mtune=native
CFLAGS += -fno-plt
CFLAGS += -fwhole-program -flto -flto-partition=none
CFLAGS += -ffunction-sections -fdata-sections

CXXFLAGS += -std=gnu++2a -fconcepts #-fchar8_t
# To allow "hacks" (YKWIM compiler usage) & be less pedantic... If only we could silence the warning
CXXFLAGS += -fpermissive
CXXFLAGS += -fno-exceptions -fno-unwind-tables -fno-asynchronous-unwind-tables
CXXFLAGS += -fno-rtti
CXXFLAGS += -Wsuggest-final-types -Wsuggest-final-methods -Wsuggest-override
#ifneq ($(OPTIMIZE),DEBUG)
ifeq ($(_OPTIMIZE),XTREME)
CXXFLAGS += -fvisibility-inlines-hidden
CXXFLAGS += -fvisibility=hidden
CXXFLAGS += -fnothrow-opt
CXXFLAGS += -fno-enforce-eh-specs
CXXFLAGS += -fvtable-verify=none
CXXFLAGS += -fdevirtualize-speculatively
CXXFLAGS += -fdevirtualize-at-ltrans
CXXFLAGS += -flto-odr-type-merging
endif # !DEBUG
ifndef EMBARALLEL
CXXFLAGS += -fno-threadsafe-statics
endif
####################################################################################################

#ifneq ($(OPTIMIZE),DEBUG)
ifeq ($(_OPTIMIZE),XTREME)
CFLAGS += -fmerge-all-constants
CFLAGS += -ffast-math -mfpmath=both
CFLAGS += -fipa-pta
CFLAGS += -fgcse-after-reload -fsched-pressure -flive-range-shrinkage
CFLAGS += -fira-region=all -fira-loop-pressure -fira-hoist-pressure
CFLAGS += -fivopts
CFLAGS += -fvariable-expansion-in-unroller
CFLAGS += -ftree-loop-ivcanon
CFLAGS += -ftree-loop-distribution
CFLAGS += -ftree-vectorize
#CFLAGS += -fvect-cost-model=unlimited # Can't be trusted to optimize well with this much freedom
CFLAGS += -funsafe-loop-optimizations -Wunsafe-loop-optimizations
CFLAGS += -floop-interchange
CFLAGS += -fbranch-target-load-optimize
#CFLAGS += -fbranch-target-load-optimize2 # Doesn't help atm
CFLAGS += -fstdarg-opt
# NB: x86 doesn't support, but it helps on other targets
CFLAGS += -fsection-anchors
CFLAGS += -frename-registers -fweb
CFLAGS += -fomit-frame-pointer

## UNLEASH THE COMPILER! These'll actually HELP size in the end, so they're enabled unconditionally
CFLAGS += -finline-functions
CFLAGS += --param max-vartrack-size=$(max_gcc_num)
CFLAGS += --param max-vartrack-expr-depth=$(max_gcc_num)
CFLAGS += --param max-vartrack-reverse-op-size=$(max_gcc_num)
CFLAGS += --param ipa-cp-value-list-size=$(max_gcc_num)
CFLAGS += --param ipa-max-aa-steps=$(max_gcc_num)
CFLAGS += --param ipa-max-agg-items=$(max_gcc_num)
CFLAGS += --param max-vrp-switch-assertions=$(max_gcc_num)
CFLAGS += --param max-slsr-cand-scan=$(max_gcc_nines)
CFLAGS += --param max-stores-to-sink=$(max_gcc_num)
CFLAGS += --param max-stores-to-merge=65536 #$(max_gcc_num)
CFLAGS += --param max-goto-duplication-insns=$(max_gcc_num)
CFLAGS += --param max-combine-insns=4
CFLAGS += --param gcse-unrestricted-cost=0
CFLAGS += --param gcse-cost-distance-ratio=1
CFLAGS += --param gcse-unrestricted-cost=1
CFLAGS += --param max-gcse-memory=$(max_gcc_num)
CFLAGS += --param dse-max-object-size=$(max_gcc_num)
CFLAGS += --param max-fields-for-field-sensitive=$(max_gcc_num)
CFLAGS += --param loop-interchange-max-num-stmts=$(max_gcc_num)
CFLAGS += --param loop-max-datarefs-for-datadeps=$(max_gcc_num)
CFLAGS += --param loop-invariant-max-bbs-in-loop=$(max_gcc_num)
CFLAGS += --param ira-max-loops-num=$(max_gcc_num)
CFLAGS += --param ira-max-conflict-table-size=$(max_gcc_nines) # In MB
CFLAGS += --param scev-max-expr-size=$(max_gcc_num)
CFLAGS += --param scev-max-expr-complexity=$(max_gcc_num)
CFLAGS += --param selsched-max-lookahead=$(max_gcc_num)
CFLAGS += --param selsched-max-sched-times=65536 #$(max_gcc_num)
CFLAGS += --param selsched-insns-to-rename=$(max_gcc_num)
CFLAGS += --param max-hoist-depth=0
CFLAGS += --param iv-consider-all-candidates-bound=$(max_gcc_num)
CFLAGS += --param iv-always-prune-cand-set-bound=$(max_gcc_num)
CFLAGS += --param iv-max-considered-uses=$(max_gcc_num)
CFLAGS += --param max-tail-merge-iterations=$(max_gcc_num)
CFLAGS += --param max-tail-merge-comparisons=$(max_gcc_num)
CFLAGS += --param max-crossjump-edges=$(max_gcc_num)
CFLAGS += --param max-delay-slot-insn-search=$(max_gcc_num)
CFLAGS += --param max-rtl-if-conversion-insns=99
CFLAGS += --param ira-max-loops-num=$(max_gcc_num)
CFLAGS += --param ira-max-conflict-table-size=$(max_gcc_num)
CFLAGS += --param lra-max-considered-reload-pseudos=$(max_gcc_num)
CFLAGS += --param max-isl-operations=0
CFLAGS += --param iv-max-considered-uses=$(max_gcc_num)
CFLAGS += --param dse-max-object-size=$(max_gcc_num)
CFLAGS += --param max-dse-active-local-stores=$(max_gcc_num)
CFLAGS += --param sms-dfa-history=16 #$(max_gcc_num)
#CFLAGS += --param sccvn-max-expr-size=$(max_gcc_num)
#CFLAGS += --param sccvn-max-alias-queries-per-access=$(max_gcc_num)
CFLAGS += --param graphite-max-nb-scop-params=0
#CFLAGS += --param graphite-max-arrays-per-scop=$(max_gcc_num)
# We don't care about stack frame size
CFLAGS += --param large-stack-frame=$(max_gcc_num)
CFLAGS += --param large-stack-frame-growth=$(max_gcc_num)
# Yes, really and truly, let it brute-force all it wants if necessary
CFLAGS += --param max-iterations-to-track=$(max_gcc_num)
CFLAGS += --param max-iterations-computation-cost=$(max_gcc_num)
# NB: This is possibly THE most dangerous parameter we allow in terms of blowing up compile time
CFLAGS += --param max-modulo-backtrack-attempts=$(max_gcc_num)
endif # !DEBUG

#ifeq ($(OPTIMIZE),SPEED)
ifeq ($(_OPTIMIZE),XTREME)
CFLAGS += -Ofast
CFLAGS += --no-allow-store-data-races
# ANY function should be considered for inlining, esp. intermediate transformations
CFLAGS += -finline-limit=$(max_gcc_num)
CFLAGS += --param max-early-inliner-iterations=$(max_gcc_num)
CFLAGS += --param max-inline-recursive-depth=$(max_gcc_num)
CFLAGS += --param max-inline-recursive-depth-auto=$(max_gcc_num)
CFLAGS += --param max-inline-insns-single=$(max_gcc_num)
CFLAGS += --param max-inline-insns-auto=$(max_gcc_num)
CFLAGS += --param max-inline-insns-recursive=$(max_gcc_num)
CFLAGS += --param max-inline-insns-recursive-auto=$(max_gcc_num)
CFLAGS += --param large-function-growth=$(max_gcc_num)
CFLAGS += --param loop-block-tile-size=$(max_gcc_num)
CFLAGS += --param max-tree-if-conversion-phi-args=65536 #$(max_gcc_num)
CFLAGS += --param max-speculative-devirt-maydefs=$(max_gcc_num)
CFLAGS += --param max-cse-path-length=65536 #$(max_gcc_num)
CFLAGS += --param max-cse-insns=$(max_gcc_num)
CFLAGS += --param max-reload-search-insns=$(max_gcc_num)
CFLAGS += --param max-cselib-memory-locations=$(max_gcc_num)
CFLAGS += --param max-sched-ready-insns=65536 #$(max_gcc_num)
CFLAGS += --param max-sched-region-blocks=$(max_gcc_num)
CFLAGS += --param max-sched-region-insns=$(max_gcc_num)
CFLAGS += --param max-sched-extend-regions-iters=$(max_gcc_num)
CFLAGS += --param max-pipeline-region-insns=$(max_gcc_num)
CFLAGS += --param max-pipeline-region-blocks=$(max_gcc_num)
CFLAGS += --param max-fsm-thread-length=$(max_gcc_nines)
CFLAGS += --param max-fsm-thread-paths=$(max_gcc_nines)
CFLAGS += --param max-fsm-thread-path-insns=$(max_gcc_nines)
# Yes, srsly, and -funroll-all-loops may even help someday (doesn't add any benefit atm)
CFLAGS += -funroll-loops
CFLAGS += -fpeel-loops
CFLAGS += -fsplit-loops
CFLAGS += -funswitch-loops
CFLAGS += --param tracer-dynamic-coverage=100
CFLAGS += --param max-partial-antic-length=0
CFLAGS += --param max-unroll-times=$(max_gcc_num)
CFLAGS += --param max-peeled-insns=$(max_gcc_num)
CFLAGS += --param max-peel-times=$(max_gcc_num)
CFLAGS += --param max-peel-branches=$(max_gcc_num)
# We CANNOT enable these, lest compile-time truly become useless
# CFLAGS += --param max-completely-peeled-insns=$(max_gcc_num)
# CFLAGS += --param max-completely-peel-times=$(max_gcc_num)
CFLAGS += --param max-completely-peel-loop-nest-depth=$(max_gcc_num)
CFLAGS += --param max-unswitch-insns=$(max_gcc_num)
CFLAGS += --param max-unswitch-level=$(max_gcc_num)
CFLAGS += --param sra-max-scalarization-size-Ospeed=$(max_gcc_num)
endif # SPEED

ifeq ($(OPTIMIZE),SIZE)
CFLAGS += -Os
#CFLAGS += --param sra-max-scalarization-size-Osize=1
CFLAGS += --param store-merging-allow-unaligned=1
endif

####################################################################################################

LDFLAGS += -Wl,-O1
LDFLAGS += -Wl,--relax        # for some linker optimizations
LDFLAGS += -Wl,--as-needed
LDFLAGS += -Wl,--hash-style=gnu
LDFLAGS += -Wl,--gc-sections,--print-gc-sections  # discard unused sections (e.g. functions & data)
LDFLAGS += -Wl,--demangle,--unresolved-symbols=report-all,--warn-common,--error-unresolved-symbols
LDFLAGS += -Wl,-z,now
LDFLAGS += -Wl,-z,combreloc,-z,norelro
#LDFLAGS += $(CFLAGS) $(CXXFLAGS)

PROFILE  := -fprofile-arcs -fvpt
PROFILING = $(PROFILE) -fprofile-generate -fauto-profile
PROFILED  = $(PROFILE) -fprofile-use

# Options for debugging (incl. the assembly)
DFLAGS += -gdwarf-5 -fvar-tracking-assignments
DFLAGS += -fdebug-types-section

SANITIZE := -fsanitize-recover
# NB: AtlasDesc isn't aligned and never will be
SANITIZE += -fsanitize=address -fsanitize=pointer-compare -fsanitize=pointer-subtract
SANITIZE += -fsanitize=leak
SANITIZE += -fsanitize=undefined
SANITIZE += -fsanitize-address-use-after-scope

ifneq ($(OPTIMIZE),DEBUG)
DEBUG += -D span_CONFIG_CONTRACT_LEVEL_OFF
DEBUG += -D NDEBUG
DEBUG += -D REASSURE
#DEBUG += -D INSHALLAH
#DEBUG += $(SANITIZE)
else
DEBUG += -D DEBUG
DEBUG += $(DFLAGS)
#DEBUG += $(SANITIZE)
#DEBUG += -lexplain
endif
DEBUG += -D TERSE

## Compiler debugging/introspection
DDEBUG += -save-temps
#DDEBUG += -time=ddebug/time
#DDEBUG += -fdump-passes
#DDEBUG += -fdump-statistics
#DDEBUG += -fdump-tree-all-lineno=ddebug/tree
#DDEBUG += -fdump-ipa-all
#DDEBUG += -fopt-info-optall-all=ddebug/optall
# DDEBUG += -fopt-info-inline-all=ddebug/inline
# DDEBUG += -fopt-info-loop-all=ddebug/loop
# DDEBUG += -fopt-info-omp-all=ddebug/omp
# DDEBUG += -fopt-info-vec-all=ddebug/vec
# DDEBUG += -fopt-info-ipa-all=ddebug/ipa
#DDEBUG += -flto-report -flto-report-wpa
#DDEBUG += -fmem-report-wpa -fmem-report
#DDEBUG += -fpre-ipa-mem-report  -fpost-ipa-mem-report

# Options for debugging the assembly
DDEBUG += -fverbose-asm -fdump-unnumbered
#DDEBUG += -fstack-usage #-dP
#DDEBUG += -Wa,--keep-locals,-acdhlmn #=$(@:%.o=%.S) # output the assembly
#DDEBUG += -Wa,-adhln=atlas.S # output the assembly

#DEBUG += $(DDEBUG)

TIMING := -v
TIMING += -ftime-report
TIMING += -ftime-report-details

#DEBUG += $(TIMING)

# Same as above, but for the final link
#LDFLAGS += -ggdb3
#LDFLAGS += -fverbose-asm -fdump-unnumbered
#LDFLAGS += -fdebug-types-section
#LDFLAGS += -fdump-final-insns=$(TARGET).rtl
#LDFLAGS += -save-temps
#LDFLAGS += -dA
#LDFLAGS += -flto-report

SELFDIR := $(realpath $(CURDIR))

INCLUDE := $(SELFDIR)/include/

####################################################################################################

.PHONY : clean all profile profile-use indexer chartor dechartor index

all: atlas

clean: Makefile
	-rm -f atlas atlas.{o,ii,res,s,su,0,1,2,3,cpp.}* _atlas.o ddebug/* gen/* tools/pvrcvry

####################################################################################################
#TODO: Specializations
profile: CFLAGS += $(PROFILING)
profile: atlas

profile-use: CFLAGS += $(PROFILED)
profile-use: profile

indexer: profile-use

chartor: profile-use

dechartor: profile-use

####################################################################################################
# Taken to be the path to the atlas
ifdef SINGULAR_ATLAS
ATLAS = $(realpath $(SINGULAR_ATLAS))
ATLAS_SIZE = $(shell $(SELFDIR)/tools/get-atlas-size.sh $(ATLAS))
DEFNS += -D SINGULAR_ATLAS=\"$(ATLAS)\" -D SINGULAR_ATLAS_SIZE=$(ATLAS_SIZE)
SRCS += _atlas.o

index: INDEX_DEST ?= $(SELFDIR)/test/
index: atlas
	cd $(INDEX_DEST)
	time $(SELFDIR)/atlas i

__atlas.o: $(ATLAS)
	$(LD) -r -b binary $(ATLAS) -o __atlas.o

# Fixup symbol names ( @_@ )
_atlas.o: __atlas.o
	$(eval start_sym = $(shell nm __atlas.o | grep start | cut -d\  -f3))
	$(eval end_sym = $(shell nm __atlas.o | grep end | cut -d\  -f3))
	objcopy --pure \
	--rename-section .data=.rodata.atlas,alloc,load,data,contents,readonly \
	--redefine-sym $(start_sym)=_atlas_start \
	--redefine-sym $(end_sym)=_atlas_end \
	__atlas.o _atlas.o
	-rm __atlas.o

else ifdef SINGULAR_ATLAS_SIZE
DEFNS += -D SINGULAR_ATLAS_SIZE=$(SINGULAR_ATLAS_SIZE)
endif # SINGULAR_ATLAS{,_SIZE}
.INTERMEDIATE: _%atlas.o
####################################################################################################

CPPFLAGS += $(DEFNS)
FLAGS := $(CPPFLAGS) $(CFLAGS) $(CXXFLAGS) $(LDFLAGS) $(DEBUG)

atlas: $(SRCS) $(HDRS) Makefile $(INCLUDE)
	$(CC) -o $@ $(SRCS) $(LIBS) -I $(SELFDIR) -I $(INCLUDE) $(FLAGS)

atlas.S: $(SRCS) $(HDRS) Makefile $(INCLUDE)
	$(CC) -S -o $@ $(SRCS) $(LIBS) -I $(SELFDIR) -I $(INCLUDE) $(FLAGS)

RCVRY_SRCS := $(SELFDIR)/tools/pvrcvry.c++
tools/pvrcvry: $(RCVRY_SRCS) Makefile $(INCLUDE)
	$(CC) -o $@ $(RCVRY_SRCS) -I $(INCLUDE) $(FLAGS)

