#!/bin/bash -e
# NB: intentionally no `-x` because we `set -x` below
# ©120[17-20] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats

readonly self_dir=`realpath $(dirname $(realpath "$0"))`

export RAND="${1:-/dev/urandom}"

clear
set -x
"${self_dir}"/remake.sh "$RAND"

readonly hour_centisecs=$((60 * 60 * 100))

set_vm() { echo $1 | sudo tee /proc/sys/vm/$2 > /dev/null; }

setup() {
    set_vm 100 swappiness
    set_vm 100 dirty_ratio
    set_vm 100 dirty_background_ratio
    set_vm $hour_centisecs dirty_expire_centisecs
    set_vm $hour_centisecs dirty_writeback_centisecs
}

run() {
    sudo sync
    set_vm 3 drop_caches
    "${self_dir}"/run.sh just "$@"
}

setup
# Count
run k
# Export almanacs and delete cndices
run A
# Make andices
run i
