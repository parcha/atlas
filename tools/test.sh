#!/bin/sh -ex
# ©120[17-20] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats

readonly self_dir=`realpath $(dirname $(realpath "$0"))`

readonly mode="$1"; shift

export RAND="${RAND:-/dev/urandom}"
cd "${TEST_DIR:-test/}"
#sudo rm -rf *

#TEST_SIZE=${SIZE:-1}
#KiBs=$((TEST_SIZE * 1024))
# Make arbitrary atlas, making sure to be truly random
#sudo dd if="$RAND" of=./rand-atlas bs=1024 count=$KiBs iflag=fullblock status=none #&
#sudo chown $USER:$USER ./rand-atlas
#RAND="./rand-atlas"

"${self_dir}"/remake.sh "$RAND"

"${self_dir}"/run.sh $mode "$@"
