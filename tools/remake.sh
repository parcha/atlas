#!/bin/bash -ex
# ©120[17-20] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats

ATLAS="${1:-/dev/urandom}"; shift
self_dir=`realpath $(dirname $(realpath "$0"))`

make -C "$self_dir/.." clean
export SINGULAR_ATLAS="$ATLAS"
#time -v
ionice -c 3 nice -n 19 make -C "$self_dir/.." -Sj1 "$@"
#`nproc`
#make $self_dir/tools/pvrcvry
