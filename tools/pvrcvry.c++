// ©120[17-20] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats

#include "util.h++"
#include "msg.h++"
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <cerrno>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <limits>
#include <ctime>
#include <sys/time.h>

#define CURSOR_UP(N) "\033[" #N "A"
#define CURSOR_DN(N) "\033[" #N "B"
#define CURSOR_RT(N) "\033[" #N "C"
#define CURSOR_LF(N) "\033[" #N "D"
#define PV_INVOCATION "pv -cW -F '         %%p %%b %%r %%a [%%e |%%I| %%t]'"

using Checkpoint = uint32_t;

static inline constexpr const unsigned default_sleepsecs = 1, default_diff_scale = 1024
                                     , bufsz = 64, niceness = 19;
static inline constexpr const auto max_ckpt = std::numeric_limits<Checkpoint>::max();

static char buf[bufsz];
static bool awaiting = true;
static const int zero_fd = open("/dev/zero", O_RDONLY);

static void load_time() {
    static auto now = time(nullptr);
    ctime_r(&now, buf);
    buf[strlen(buf)-1] = '\0'; // Eat the extra newline
}

// TODO: Use actual terminal cursor control
static inline void print_ckpt(const Checkpoint ckpt) { __MSG(stderr, "\r%.8x", ckpt); }

// ./$progname [rcvry_path] [sleepsecs] [diff_scale]
int main(int argc, const char *argv[]) {
    static int retval = 1;
    nice(niceness);

    ASSUME(zero_fd != -1);
    static const auto rcvry_path = (argc < 2) ? "rcvry" : argv[1];
    static const int rcvry_fd = open(rcvry_path, O_RDONLY);
    ASSUME(rcvry_fd != -1);
    static struct stat rcvry_stats;
    if (auto ret = fstat(rcvry_fd, &rcvry_stats)
      ; UNLIKELY(ret != 0)) {
        ERR("Failed to stat: %d; %m", ret);
        return -1;
    }
    static const bool is_blk = S_ISBLK(rcvry_stats.st_mode);

#define parse_or_default(var, argn) \
    static const auto var = (argc < argn+1) ? default_##var : atoi(argv[argn]) ?: default_##var
    parse_or_default(sleepsecs, 2);
    parse_or_default(diff_scale, 3);

    using Checkpointer = volatile const Checkpoint *const;
    static auto ckptr = (Checkpointer)mmap(nullptr, sizeof(Checkpoint), PROT_READ, MAP_PRIVATE, rcvry_fd, 0);
    ASSUME(ckptr && ckptr != MAP_FAILED);
start:
    static auto last_ckpt = *ckptr;
    const auto&& scale = [&last_ckpt,diff_scale] (Checkpoint ckpt)
        { return (ckpt - last_ckpt) / diff_scale; };
    const auto remaining = max_ckpt - last_ckpt;
    load_time();
    __MSG(stderr, "Starting from %.8x (%.8x) @ %s", last_ckpt, remaining, buf);
    if (sleepsecs != default_sleepsecs)  __MSG(stderr, "; sampling every %u sec", sleepsecs);
    if (diff_scale != 1) __MSG(stderr, "; scaled by %u", diff_scale);
    MSG(""); // Just newline

    snprintf(buf, sizeof(buf), PV_INVOCATION " -Ss %u -i %u > /dev/null",
             scale(max_ckpt), sleepsecs*2 /*Nyquist*/ );
    static auto pipe_file = popen(buf, "we");
    ASSUME(pipe_file);
    const int pipe_fd = fileno(pipe_file);
    ASSUME(pipe_fd != -1);

    for (auto ckpt = *ckptr; UNLIKELY(awaiting) || (ckpt && ckpt < max_ckpt); ckpt = *ckptr) {
        if (const auto diff = scale(ckpt)) {
            awaiting = false;
            if (const auto spliced = splice(zero_fd, nullptr, pipe_fd, nullptr, diff, SPLICE_F_MORE)
              ; UNLIKELY(spliced != diff)) {
                if (spliced > 0) {
                    last_ckpt += spliced;
                    goto wait;
                }
                //Lif (errno == EAGAIN) goto wait;
                ERR("Failed to splice: %m");
                goto exit;
            }
            last_ckpt = ckpt;
        }
    wait:
        print_ckpt(ckpt);
        sleep(sleepsecs);
    }
    load_time();
    MSG("Detected completion @ %s", buf);
    // If we're using a block device for working, it'll be reused, so go await the next use
    if (is_blk) {
        pclose(pipe_file);
        goto start;
    }
    retval = 0;

exit:
#ifndef NDEBUG
    close(zero_fd);
    munmap((void*)ckptr, sizeof(Checkpoint));
    close(rcvry_fd);
    pclose(pipe_file);
#endif // !NDEBUG
    return retval;
}
