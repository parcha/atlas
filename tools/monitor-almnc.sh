#!/bin/sh
# ©120[17-20] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats

almnc="${1:-.almnc}"
sleepytime="${2:-1}"

while true; do
    du --apparent-size -m "$almnc"
    sleep "$sleepytime"
    tput cuu1; tput el
done
