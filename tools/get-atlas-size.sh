#!/bin/sh -e
# ©120[17-20] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats

if [ -f "$1" ]; then
    du --apparent-size -b "$1" | cut -f1
elif [ -b "$1" ]; then
    sudo blockdev --getsize64 "$1"
else
    echo "Failed to get size for $1" >&2
    exit 1
fi
