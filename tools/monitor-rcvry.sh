#!/bin/sh
# ©120[17-20] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats

rcvry="${1:-rcvry}"
sleepytime="${2:-3}"

while true; do
    xxd -l 4 -e "$rcvry"  | cut -d\  -f 2
    sleep "$sleepytime"
    tput cuu1; tput el
done
