#!/bin/bash -e
# ©120[17-20] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats

readonly self_dir=`realpath $(dirname $(realpath "$0"))`

readonly how="$1"; shift
readonly mode="$1"; shift

export OMP_WAIT_POLICY=active
export OMP_DYNAMIC=false
export OMP_PROC_BIND=true
export OMP_NUM_THREADS=`nproc`
export OMP_SCHEDULE="static,`nproc`"

# NOTE: Don't use taskset if EMBARALLEL; we pin to the last CPU
run() { sudo -E time -v ionice -c 1 -n 7 nice -n -20 chrt -r 7 taskset -c $((`nproc`-1)) "$@"; }

readonly invocation="${self_dir}/../atlas $mode $RAND"

just_run() {
	run $invocation "$@" 2>&1 | tee -a ${mode}test.log &
	local pid=$!

	# Avoid the OOM killer
	sleep 1s
	echo '-17' | sudo tee /proc/$pid/oom_score_adj > /dev/null

	wait
}

perf_run()
{ run perf record --all-kernel --call-graph dwarf -sF 99 -r 7 -o "$mode-perf.data" $invocation; }

debug_run() { run cgdb --args $invocation "$@"; }

grind_run() { run valgrind $invocation "$@"; }

####################################################################################################
set -x

${how}_run "$@"
#just_run "$@"
#perf_run "$@"
#debug_run "$@"
#grind_run "$@"
