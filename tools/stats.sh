#!/bin/sh -e
# ©120[17-20] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats

sizes=(1 2 4 8 16 32 64 128 256 512)
units=(B K M)
initial_offset=16 # bc we start with 16B
end_size=4M

for u in ${units[@]}; do
	for s in ${sizes[@]}; do
		if test $u = ${units[0]}; then
			if test $s -lt $initial_offset; then continue; fi
		fi
		sz=$u$s
		# TODO
		if test $sz = $end_size; then exit 0; fi
	done
done
