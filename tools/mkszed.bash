#!/bin/bash -e
# ©120[17-20] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats

RAND="$1"; shift
self_dir=`realpath $(dirname $(realpath "$0"))`

opts=(SIZE SPEED XTREME)
szs=(16B 32B 64B 128B 256B 512B 1K 2K 4K 8K 16K) # 32K 64K 128K 256K 512K 1M 2M 4M)
for sz in "${szs[@]}"; do
    for opt in "${opts[@]}"; do
        dest="$self_dir/../hist/atlas-$sz-$opt"
        if [[ ! -e "$dest" ]]; then
            LAST_SZ=$sz _OPTIMIZE=$opt "$self_dir/remake.sh" "$RAND" "$@"
            mv atlas "$dest"
            c++filt < atlas.s > "$dest.S"
            chmod a-w "$dest"{,.S}
        fi
    done
done
