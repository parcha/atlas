Welcome to some combination of an engineering project & template, a communal art project, and a mathematical experiment!
You'll need `gcc` ≥10, GNU `make`, and a POSIX-compliant `sh`ell.
You'll also want up to 64 GiB of π.
(Or other sufficiently random data.)

`atlas` is a program which "charts" and "decharts" information using an "atlas" as reference, effectively encoding (charting (as an "enchartor")) and decoding (decharting (as a "dechartor")) information in terms of the atlas.
This has trivial uses in terms of [steganography](https://en.wikipedia.org/wiki/Steganography), but by using *e.g.* π as the atlas it is hoped that one can use `atlas` in practice to losslessly "compress" information for archival usage or expensive transmissions (*e.g.* across Space).

#### WARNING

If you're using the indexing functionality to create your own indices, please be aware that — while it may sound silly — **this will stress your hardware stack in ways *nothing* has before**.
If nothing else, be aware that you'll be ~~sadistically~~ abnormally accelerating the death of your storage hardware.

Creating the indices is *very* memory & I/O intensive, though not in the sense that it would bog down the machine otherwise.
Quite the contrary: the machine will just fundamentally be unable to offer decent performance.
As far as my ~~sacrificial offerings~~ empirical investigations have informed me, it's simply due to the combination of both π-derived random, incompressible data *and* π-derived control flow (into the giant hash table).
From [TLB](https://en.wikipedia.org/wiki/Translation_lookaside_buffer)-wrecking to confounding compression-using algorithms all along your storage stack (*e.g.* in firmware), you'll find no end to bizarre hiccups, hangups, and outright errors.

## Build

```
% cd $srcdir
% env LAST_SZ=16B ./tools/remake.sh /dev/PI
```

For testing's sake, without a "true" atlas, one can use any sufficiently random block of data; *e.g.* `/dev/random` or — if large enough — `/dev/urandom`. (*cf.* `$srcdir/tools/test.sh`)

*NB*: `sz`s, as in `LAST_SZ` above start at `16B`. See the code for others, but enabling just the first `sz` is enough for testing.

## Use

*FROM THE SAME DIRECTORY*, with enough space for — assuming a 64 GiB atlas — ≈67 GiB *per supported `sz`* for all the metadata we're going to produce…

##### Producing counting indices of the atlas:
```
% env ATLAS=/dev/PI $srcdir/tools/run.sh just k
```
##### Producing almanacs of the atlas, using the counting indices:
```
% env ATLAS=/dev/PI $srcdir/tools/run.sh just a
```
##### Producing indices of the atlas, using the almanacs:
```
% env ATLAS=/dev/PI $srcdir/tools/run.sh just i
```
##### Charting a file using the atlas, almanacs, and indices:
```
% env ATLAS=/dev/PI $srcdir/tools/run.sh just c $in_file $out_file.pi
```
##### Decharting a file using *only* the atlas:
```
% env ATLAS=/dev/PI $srcdir/tools/run.sh just d $in_file.pi $out_file
```
## The License

… in the source code currently reads:

```
"©(12017 ~ " __DATE__ ") Alex Marquez <alex@grokalex.com>" \
"\nGNU AGPL-v3+, with the following constraints (in \"violation\" of the 'original' license):" \
"\n* Licensees of this software reject the concept of \"intellectual property\"," \
"\n  and thus copyright and patents, among its(') other forms" \
"\n* Modifications made to this software are INHERENTLY in the Public Domain AND" \
"\n  subject to the terms of this License in jurisdictions recognizing \"intellectual property\""
```

Thus, there's a high likelihood that — in the current political climate — in your jursidication this code is effectively subject to the rules of the [GNU AGPL-v3 license](https://gnu.org/licenses/agpl.html), or later.


---
# Atlas

## , the project

In planning, producing, and testing this program, I generated a 64 GiB chunk of π as the exemplar atlas.
It can be found at ***TODO*** .
It was produced using the [`y-cruncher`](www.numberworld.org/y-cruncher/) unfortunately-proprietary software, many [**TiB**](https://en.wikipedia.org/wiki/Tebibyte) of virtual memory, and *months* of time.

## , the idea

[π](https://en.wikipedia.org/wiki/Pi) is supposedly a [normal number](https://en.wikipedia.org/wiki/Normal_number) and an awfully ubiquitous universal constant.
So if we were to use it as an "atlas" (à la [texture-indexing](https://en.wikipedia.org/wiki/Texture_atlas)), it should theoretically be serviceable as a sort of "universal atlas", at least if one uses a large enough amount (of digits).
It's also kinda definitionally "symmetric".
So if we read it in reverse, we should have the same seemingly-normal entropic distribution... and the same is true if we negate all the digits, right?

Therefore, given a finite amount of this atlas we should be able to press it into service at (≈)2× the potential via directional reading (modulo palindromes), and 2× further via optional negation.
If we accept negation by parity, we add another 2 possibilities to the negation, bringing our multiplier to 8.
Thus, for any given amount of π, using this scheme we have effectively (≈)8 equally-entropically-normal-but-distinct chances to match data to it.

By carefully encoding the requisite transformations and offset into π, we can achieve a 5-byte descriptor, which at the given word-size of 16 bytes (128-bit) yields a minimum non-literal "compression" (really, charting) ratio of **16:5**.
By using Segment `sz`s, we can also offer ratios of *e.g*. 8:5, if the word size isn't granular enough.

## , the usage

*NB*: This is the usage message if one enables all modes of operation.

***TODO***

## , the implementation

The entire indexing and de/charting process revolves around pre/computed/ing `mmap`ped hash tables.
Indexing involves simply going over the entire atlas and recording (in an index-specific manner) what offset(s) correspond(s) to which hash value.
The transformation (negation) applied and the `sz` are implicit, denoted through the directories in which the index is nested.

### Monitoring progress

#### Build
```
% make tools/pvrcvry
```
#### Use

While performing some operation that produces a `rcvry` file, it can be monitored with a crude [`pv`](https://www.ivarch.com/programs/pv.shtml)-using [utility](tools/pvrcvry.c++).

```
% cd $index_dir/$sz/$dir
% $src_dir/tools/pvrcvry $opt_rcvry_file $opt_polling_secs $opt_scaling_factor
```

***TODO***

### CIndexing

A **C**ounting index simply runs the atlas through the hash function and records how many hits each value in the hash space gets, up to 255.

### VIndexing

A **V**ariable index is used for when the precise atlas is unknown, but we *do* know that the frequency of certain numbers of hash collisions are bounded.
It revolves around keeping a certain number of "direct" entries encoded directly into the hash table.
If those direct entries are exceeded, they're dumped to a *separate* appendix ("apndx") and the hash table entry instead encodes a list of indirected entry IDs.
These IDs correspond to progressively-smaller further appendices ("bpndx", "cpndx", …) and are allocated sequentially.
By knowing ahead-of-time the most hash collisions we can expect per frequency, we can encode these indirected entry IDs as progressively-smaller bitfields, thus packing support for many appendices into the same index entry as would've been used for direct entries.

*NB*:
To be clear, this is **NOT** the recommended mode of operation.
It is more of a historical note and of niche usage (*i.e.*, not in this project), and for exposition.
Use an almanac-enabled index instead.

### Almanac-ing & AIndexing

An Almanac is a *highly* specialized accounting of all buckets seen in our indexing of the atlas, gaplessly-sized and contiguous, eliding holes by separately accounting for them in a sparse bitmap.
It's the skeleton of a perfect hash (table).

***TODO***

An **A**lmanac-using index is… eponymous.
It contains one element for every hash value, knowing that exactly that many will be generated, and that we can pack them all together and work out which stretch of elements belong to the same bucket by using an almanac.
Searching over an almanac is performed with a carefully coded [variant of binary search](https://en.wikipedia.org/wiki/Binary_search_algorithm#Alternative_procedure) meant for when one's $`n`$ is very high — as it is in our case of $`n ≈ 2^{32}`$.

An Almanac entry stores the `hash` corresponding to this entry, the *absolute* `offset` in the AIndex of the hash bucket corresponding to this entry, and the `count` of *exactly* how many collisions there'll be in said bucket.
In order to minimize the size of each Almanac entry, we don't encode the `offset` literally, but rather as a 17-bit signed difference versus the stored `hash` value.
By also storing its `count` as a 7-bit value, we fit an Almanac entry in 7 bytes.
(Notably, this makes a huge performance difference for 64-bit machines, as 7 will fit in 8 but 9 wouldn't've.)

### Charting

Charting is performed by a chain of Chartors, each templated on a `sz`.
With higher levels of optimization, the compiler explores the exponential/combinatorial space of inlining successive layers of the tree of Chartors.

### Decharting

Decharting is supremely simple; we use the atlas as instructed by the incoming AtlasDescs, streaming out the appropriately-transformed blocks.

### Debugging

First, consider whether you'll want to make use of any of GCC's [santizers](https://gcc.gnu.org/onlinedocs/gcc/Instrumentation-Options.html).
If so, you'll want to adjust the `SANITIZE` variable in the [Makefile](Makefile) and enable it in the following `DEBUG` setup clause.

### Sidebars/Miscellany

#### [Segment `sz`s](atlas.h++)

The word size of 16 bytes was chosen carefully to fit into a very optimal space for needing 32 bits to express word-aligned offsets for the exemplar 64 GiB atlas.
In order to express block sizes smaller than the word size, we take the simple and seemingly/hopefully-entropically-neutral approach of encoding offsets in terms of a ≥word-size *Segment size*.
This Segment is composed of a number of pieces, each of which is represented by a distinct `sz` value.
Thus, for example, we can emulate an 8-byte word-size by dually using both the indices (, etc.) for each constituent `sz`: `8_0` and `8_1`.
In a similar manner, we modulo-encode the offsets of stranger Segment sizes: 6, 7, 9, and 10 bytes.

#### `EMBARALLEL`

While both indexing and de/charting are very much I/O-bound on today's personal workstations, if you have an *obscene* setup wherein it's CPU-bound, then we've got you covered!
In order to enable minimal code changes, there're essentially alternate threadsafe versions of all core algorithms and data, and auto-parallelized loops largely fueled by [OpenMP](https://www.openmp.org).
Good luck and Godspeed, friend.

#### `REASSURE` & `INSHALLAH`

These are cheeky macros that control how error-level conditions and warning-level conditions are treated, respectively.

* Enabling `REASSURE` will ***TODO***
* Enabling `INSHALLAH` will ***TODO***

#### [`ASSUME`](include/util.h++)

The `ASSUME` family of macros are essentially powerful `assert`s.
If one enables `NDEBUG` *and* `REASSURE`, instead of being outright left out, "`assert`"s leverage the compiler's `__builtin_unreachable` function to express that control flow cannot continue if this condition isn't true.
This has the effect of occasionally enabling the compiler to backwards-reason using this information (*e.g.* during [SAT solving](https://en.wikipedia.org/wiki/Boolean_satisfiability_problem#Algorithms_for_solving_SAT)) and thus gain further insight and yield further optimization opportunities.

#### [`Nibset`](include/nibset.h++)

This is just a straightforward implementation of a 4-bit-entry packed array.

#### [`ConcBuilder`](include/conc/concbuilder.h++)

This is a highly optimized, value-semantics-based, [`ConcRope`](https://aleksandar-prokopec.com/resources/docs/lcpc-conc-trees.pdf)-backed append/prepend/concat/internal-iteration-only mostly-persistent data structure.

***TODO***

#### Compiler optimization

The discerning reader may have noticed a cornucopia of seemingly-superfluous/[ricer](https://www.shlomifish.org/humour/by-others/funroll-loops/Gentoo-is-Rice.html) `--param` flags for GCC.
These *are* admittedly set to brute-force values, but the parameters themselves each serve a purpose and at least give the compiler further insight (cf. the "Compiler debugging/introspection" [Makefile](./Makefile) section) which may enable it to be more a savant and less an idiot, now and ever more in the future.

---
## FAQ

#### Why `C++` and not *e.g.* [`Rust`](https://rust-lang.org)?

Because at the time of composing this program `Rust` lacked a variety of features that would've validated its preferential use/utility, *e.g.* first-class bitfields and `mmap` support in borrow-checking.
Ostensibly I could've split it into a `C/++` & `Rust` program, but at the time of this writing I believe it's more accessible ~~and concise~~ in its current, pure `C++` form, however ***many*** *fundamental* gripes I have with the language.
