#define LICENSE \
"©(12017 ~ " __DATE__ ") Alex Marquez <alex@grokalex.com>" \
"\nGNU AGPL-v3+, with the following constraints (in \"violation\" of the 'original' license):" \
"\n* Licensees of this software reject the concept of \"intellectual property\"," \
"\n  and thus copyright and patents, among its(') other forms" \
"\n* Modifications made to this software are INHERENTLY in the Public Domain AND" \
"\n  subject to the terms of this License in jurisdictions recognizing \"intellectual property\""

#pragma once
#define _LARGEFILE64_SOURCE

#include "util.h++"
#include "msg.h++" // for ASSUME_3
#include <cstdint>
#include <cstddef>
#include <limits>
#include <array>
#include <type_traits>
#include <optional>
//#include <boost/endian/buffers.hpp> // TODO

////////////////////////////////////////////////////////////////////////////////////////////////////
//namespace atlas {

#if defined SINGULAR_ATLAS && !defined SINGULAR_ATLAS_SIZE
# warning "When defining SINGULAR_ATLAS, one must also specify its SINGULAR_ATLAS_SIZE"
#endif

using Word = uint128_t;

// 0,1,0,1,.. vs 1,0,1,0,..; truncated to uint width
template<typename uint, typename = std::enable_if_t<is_unsigned_integral<uint>>>
consteval uint neg_even = (uint)0xAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA_u128;
template<typename uint, typename = std::enable_if_t<is_unsigned_integral<uint>>>
consteval uint neg_odd  = (uint)0x55555555555555555555555555555555_u128;
static_assert(neg_even<Word> == ~neg_odd<Word>);

////////////////////////////////////////////////////////////////////////////////////////////////////
// Atlas Descriptors

#define APPLY_Directions(M) M(F) M(B)
#define APPLY_Negations(M) M(I) M(N) M(E) M(O)
struct AtlasDesc final {
    // TODO: Half (pivot symmetric halves around center) & Quarter (same but with 1/4s)
    enum class Dir : bool { F, B } dir : 1; /// Bitwise reverse ("F"orward or "B"ackward)

    enum class Neg : unsigned char {
        I, /// Identity
        N, /// Negated
        E, /// Even bits negated
        O, /// Odd bits negated
        // ...
        /// Potentially other "isosymmetric" transformations
    } neg : 2;

    /**
     * The only requirement of SzClass (AKA SZ) enumerators is that the size each represents must be
     * an integer multiple of the one prior, with the least block SZ being: a power of two, the size
     * of Word, and coming directly after the last segment enumerator, if any.
     *
     * The segment enumerators must represent a larger amount of bytes than the size of an AtlasDesc
     * (5), and must come directly after the sentinel `Lit` enumerator. The total segments each type
     * of enumerator represents must be >= the standard block size.
     **/
    enum class SzClass : unsigned char {
        Lit, /// Literal
        /// Segments; FIXME: only 8 is currently acceptable (see _hash)
    #ifdef ENABLE_SEGMENTS
    # if ENABLE_SEGMENTS <= 6
        _6B_0, _6B_1, _6B_2,
    # endif
    # if ENABLE_SEGMENTS <= 7
        _7B_0, _7B_1, _7B_2,
    # endif
    # if ENABLE_SEGMENTS <= 8
        _8B_0, _8B_1,
    # endif
    # if ENABLE_SEGMENTS <= 9
       _9B_0, _9B_1,
    # endif
    # if ENABLE_SEGMENTS <= 10
       _10B_0, _10B_1,
    # endif
    #endif // ENABLE_SEGMENTS
                            // Blocks
                            _16B, _32B, _64B, _128B, _256B, _512B,
        _1K, _2K, _4K, _8K, _16K, _32K, _64K, _128K, _256K, _512K,
        _1M, _2M, _4M,
        COUNT /// Fake; counts values of enumeration
    } sz : 5;
    static_assert((uint8_t)SzClass::COUNT <= 32 /* 2**5 */);

    /// SzClass "Kind" (including dependent kinds)
    enum class SzKind : unsigned char {
        Lit, /// Literal
        Fst, /// Lowest SZ before Lit
    #ifdef ENABLE_SEGMENTS
        Seg, /// Range for segments
    #endif
        Blk, /// Range for blocks
    };

    /// Offset into Atlas in 128-bit (16-byte) words; Max 64 GiB; Dir::B counts the same
    using Off = uint32_t;
    Off off;

    inline constexpr AtlasDesc(Dir dir, Neg neg, SzClass sz, Off off)
    : dir{dir}, neg{neg}, sz{sz}, off{off} {}
} PACKED;
static_assert(sizeof(AtlasDesc) <= 5);
using SZ = AtlasDesc::SzClass;
using SZK = AtlasDesc::SzKind;
using Direction = AtlasDesc::Dir;
using Negation = AtlasDesc::Neg;
using Offset = AtlasDesc::Off;

#define _COUNT(_) 1 +
consteval uint8_t num_directions = APPLY_Directions(_COUNT) 0;
consteval uint8_t num_negations = APPLY_Negations(_COUNT) 0;
#undef _COUNT

consteval Offset max_Offset = std::numeric_limits<Offset>::max();
consteval size_t max_Offsets = (size_t)max_Offset+1u;
consteval size_t max_atlas_size = max_Offsets * sizeof(Word);

struct LitDesc final {
/*template<SZ sz, SZK szk = szk_for_sz(sz)>
friend struct Chartor;*/
private:
    /*const*/ /*std::byte*/bool __reserved1 : 1 = (bool)0;
    uint8_t _size : 2;  /// in bytes [1, 4], i.e., 32-bit max
    /*const*/ /*SZ*/ unsigned char _sz : 5 = (unsigned char)SZ::Lit;

public:
    inline constexpr LitDesc(uint8_t size, uint32_t lit) : _size{size-1u}, lit{lit} {}
    DEFAULT_MOVING(LitDesc)
    DEFAULT_COPYING(LitDesc)

//protected:
    // HACK: Because we can be trusted to restrict our dynamic creation of LitDescs
    uint32_t lit;

    inline constexpr PURE uint8_t size() const { return _size+1u; }
    static consteval decltype(_size) max_size = 4;
    static_assert(max_size < sizeof(AtlasDesc));

#if 0
    template<typename uint>
    inline constexpr LitDesc(uint lit) : LitDesc(sizeof(uint), lit)
    { static_assert(sizeof(uint) <= max_sz); }
#endif
} PACKED;

template<typename uint>
consteval std::enable_if_t<is_unsigned_integral<uint>, bool>
is_lit_size(uint size) { return (0 < size) && (size <= LitDesc::max_size); }

static_assert(sizeof(LitDesc) == sizeof(AtlasDesc));
/// The main, generic Descriptor, abstracting over actual AtlasDescs and LitDescs
union Desc { AtlasDesc a; LitDesc l;
private:
    struct { uint8_t head; uint32_t tail; } PACKED internal;
public:
    inline constexpr Desc() : internal{} {}
    inline constexpr Desc(AtlasDesc a) : a{a} {}
    inline constexpr Desc(LitDesc   l) : l{l} {}
    inline constexpr PURE bool is_lit() const { return a.sz == SZ::Lit; }
};
static_assert(sizeof(Desc) == sizeof(AtlasDesc));

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// Negation & Numerics API

template<typename uint, Negation Neg>
consteval std::enable_if_t<is_unsigned_integral<uint>
, uint> negate(const uint);

// NOTE: Test expressions are compile-time sanity checks on `negate<...>(0)` & `negate<...>(MAX)`
#define _IMPL_negate(uint, Neg, expr, test_expr0, test_exprM)              \
template<> inline constexpr uint                                           \
negate<uint, Negation::Neg>(const uint _) { return (expr); }               \
static_assert(negate<uint, Negation::Neg>( (uint)0u) == (uint)(test_expr0));\
static_assert(negate<uint, Negation::Neg>(~(uint)0u) == (uint)(test_exprM));

static_assert(num_negations == 4);
#define IMPL_negate(uint)                                                \
_IMPL_negate(uint, I,  _                ,  (uint)0, ~(uint)0)            \
_IMPL_negate(uint, N, ~_                , ~(uint)0,  (uint)0)            \
_IMPL_negate(uint, E, _ ^ neg_even<uint>, neg_even<uint>, neg_odd<uint>) \
_IMPL_negate(uint, O, _ ^ neg_odd<uint> , neg_odd<uint> , neg_even<uint>)

IMPL_negate(std::byte)
IMPL_negate(uint8_t)
IMPL_negate(uint16_t)
IMPL_negate(uint32_t)
IMPL_negate(uint64_t)
IMPL_negate(uint128_t)
//IMPL_negate(uint256_t)
//IMPL_negate(uint512_t)

#undef IMPL_negate
#undef _IMPL_negate

static_assert(sizeof(Word) > sizeof(uint64_t), "*FittedUInts only extend up to Word size");
#define COND_BITS(N, COND) std::conditional_t<(bitwidth COND N), uint##N##_t,
#define IMPL__FittedUInt(COND) \
    COND_BITS(8, COND) COND_BITS(16, COND) COND_BITS(32, COND) COND_BITS(64, COND) void>>>>
#define DEF_FittedUInt(L, COND) \
    template<uint8_t bitwidth, typename = std::enable_if_t<(bitwidth > 0)>> \
    using L##FittedUInt = IMPL__FittedUInt( COND );

/// Gives the unsigned integer that exactly holds `bitwidth` bits
DEF_FittedUInt(Eq, ==)
/// Gives the unsigned integer that most closely holds `bitwidth+1` bits
DEF_FittedUInt(Hi, <)
/// TODO: Gives the unsigned integer that most closely holds `bitwidth-1` bits
DEF_FittedUInt(Lo, >)

#undef DEF_FittedUInt
#undef IMPL__FittedUInt
#undef COND_BITS

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// SZ API

INLINE constexpr       SZ& operator++(SZ& sz) { auto& _sz = sz; ((uint8_t&)sz)++; return _sz; }
INLINE constexpr       SZ& operator++(SZ& sz, int) { return ++sz; }
consteval SZ  operator+ (SZ self, SZ sz) { return (SZ)((uint8_t)self+(uint8_t)sz); }
consteval SZ  operator+ (SZ self, uint8_t sz) { return (SZ)((uint8_t)self+sz); }

INLINE constexpr       SZ& operator--(SZ& sz) { auto& _sz = sz; ((uint8_t&)sz)--; return _sz; }
INLINE constexpr       SZ& operator--(SZ& sz, int) { return --sz; }
consteval SZ  operator- (SZ self, SZ sz) { return (SZ)((uint8_t)self-(uint8_t)sz); }
consteval SZ  operator- (SZ self, uint8_t sz) { return (SZ)((uint8_t)self-sz); }

////////////////////////////////////////////////////////////////////////////////////////////////////
// Block-based SZs

consteval uint8_t first_blk_sz_pow2 = 4; /// 16B
consteval SZ first_blk_sz = SZ::_16B
                        ,  last_blk_sz =
                            #ifndef LAST_SZ
                                SZ::COUNT-1;
                            #else
                                SZ::LAST_SZ;
                            #endif
static_assert(last_blk_sz >= first_blk_sz);
consteval uint8_t num_blk_szs = uint8_t(last_blk_sz - first_blk_sz) + 1u;
consteval bool is_blk_sz(SZ sz) { return (first_blk_sz <= sz) && (sz <= last_blk_sz); }
#define ASSUME_BLK_SZ(var) ASSUME(is_blk_sz(var), var, "hhx {" #var "}")

consteval uint8_t
idx_for_blk_sz(SZ sz)
{ ASSUME_BLK_SZ(sz); return (uint8_t)(sz - first_blk_sz); }

consteval uint8_t
pow2_for_blk_sz(SZ sz)
{ ASSUME_BLK_SZ(sz); return idx_for_blk_sz(sz) + first_blk_sz_pow2; }

consteval SZ
blk_sz_for_pow2(uint8_t pow2)
{ return (SZ)(pow2 - first_blk_sz_pow2) + first_blk_sz; }

consteval Offset
size_for_blk_sz(SZ sz) {
    auto pow = pow2_for_blk_sz(sz);
    return pow ? (1 << pow) : 0;
}

consteval auto  lowest_blk_size = size_for_blk_sz(first_blk_sz)
                          , highest_blk_size = size_for_blk_sz(last_blk_sz);
static_assert((lowest_blk_size % sizeof(Word)) == 0);
consteval bool
is_blk_size(Offset size) {
    //return lowest_blk_size <= size && size <= highest_blk_size;
    for(SZ sz = first_blk_sz; sz <= last_blk_sz; sz++)
        if (size_for_blk_sz(sz) == size) return true;
    return false;
}
#define ASSUME_BLK_SIZE(var) ASSUME(is_blk_size(var), var, "x {" #var "}")

inline constexpr auto blk_stride = sizeof(Word);
static_assert(blk_stride <= lowest_blk_size);
static_assert(sizeof(Word) % blk_stride == 0);

////////////////////////////////////////////////////////////////////////////////////////////////////
// Segment-based SZs

#define ASSUME_SEG_SZ(var) ASSUME(is_seg_sz(var), var, "hhx {" #var "}")

#ifndef ENABLE_SEGMENTS
consteval SZ first_seg_sz = first_blk_sz
                        ,  last_seg_sz = last_blk_sz;
consteval bool is_seg_sz(SZ) { return false; }

consteval auto lowest_seg_size  = lowest_blk_size;
consteval auto highest_seg_size = highest_blk_size;
consteval bool is_seg_size(Offset) { return false; }
#else

consteval SZ first_seg_sz = SZ::Lit+1
                        ,  last_seg_sz = first_blk_sz-1;
consteval uint8_t num_seg_szs = (uint8_t)(last_seg_sz - first_seg_sz) + 1;
consteval bool is_seg_sz(SZ sz) { return (first_seg_sz <= sz) && (sz <= last_seg_sz); }

consteval uint8_t
idx_for_seg_sz(SZ sz)
{ ASSUME_SEG_SZ(sz); return (uint8_t)(sz - first_seg_sz); }

consteval Offset
size_for_seg_sz(SZ sz) {
    ASSUME_SEG_SZ(sz);
    switch(sz) {
#ifdef ENABLE_SEGMENTS
# if ENABLE_SEGMENTS <= 6
    case SZ::_6B_0 ... SZ::_6B_2: return 6;
# endif
# if ENABLE_SEGMENTS <= 7
    case SZ::_7B_0 ... SZ::_7B_2: return 7;
# endif
# if ENABLE_SEGMENTS <= 8
    case SZ::_8B_0 ... SZ::_8B_1: return 8;
# endif
# if ENABLE_SEGMENTS <= 9
    case SZ::_9B_0 ... SZ::_9B_1: return 9;
# endif
# if ENABLE_SEGMENTS <= 10
    case SZ::_10B_0 ... SZ::_10B_1: return 10;
# endif
#endif // ENABLE_SEGMENTS
    default: UNREACHABLE;
    }
}

consteval uint8_t
piece_idx_for_seg_sz(SZ sz) {
    ASSUME_SEG_SZ(sz);
    switch(sz) {
#ifdef ENABLE_SEGMENTS
# if ENABLE_SEGMENTS <= 6
    case SZ::_6B_0 ... SZ::_6B_2: return (uint8_t)(sz - SZ::_6B_0);
# endif
# if ENABLE_SEGMENTS <= 7
    case SZ::_7B_0 ... SZ::_7B_2: return (uint8_t)(sz - SZ::_7B_0);
# endif
# if ENABLE_SEGMENTS <= 8
    case SZ::_8B_0 ... SZ::_8B_1: return (uint8_t)(sz - SZ::_8B_0);
# endif
# if ENABLE_SEGMENTS <= 9
    case SZ::_9B_0 ... SZ::_9B_1: return (uint8_t)(sz - SZ::_9B_0);
# endif
# if ENABLE_SEGMENTS <= 10
    case SZ::_10B_0 ... SZ::_10B_1: return (uint8_t)(sz - SZ::_10B_0);
# endif
#endif // ENABLE_SEGMENTS
    default: UNREACHABLE;
    }
}

consteval uint8_t
num_seg_pieces(SZ sz) {
    ASSUME_SEG_SZ(sz);
    switch(sz) {
#ifdef ENABLE_SEGMENTS
# if ENABLE_SEGMENTS <= 6
    case SZ::_6B_0 ... SZ::_6B_2: return piece_idx_for_seg_sz(SZ::_6B_2) + 1;
# endif
# if ENABLE_SEGMENTS <= 7
    case SZ::_7B_0 ... SZ::_7B_2: return piece_idx_for_seg_sz(SZ::_7B_2) + 1;
# endif
# if ENABLE_SEGMENTS <= 8
    case SZ::_8B_0 ... SZ::_8B_1: return piece_idx_for_seg_sz(SZ::_8B_1) + 1;
# endif
# if ENABLE_SEGMENTS <= 9
    case SZ::_9B_0 ... SZ::_9B_1: return piece_idx_for_seg_sz(SZ::_9B_1) + 1;
# endif
# if ENABLE_SEGMENTS <= 10
    case SZ::_10B_0 ... SZ::_10B_1: return piece_idx_for_seg_sz(SZ::_10B_1) + 1;
# endif
#endif // ENABLE_SEGMENTS
    default: UNREACHABLE;
    }
}

consteval auto lowest_seg_size  = size_for_seg_sz(first_seg_sz)
             , highest_seg_size = size_for_seg_sz(last_seg_sz);

consteval bool
is_seg_size(Offset size) {
    //return lowest_seg_size <= size && size <= highest_seg_size;
    for(SZ sz = first_seg_sz; sz <= last_seg_sz; sz++)
        if (size_for_seg_sz(sz) == size) return true;
    return false;
}
#define ASSUME_SEG_SIZE(var) ASSUME(is_seg_size(var), var, "x {" #var "}")
static_assert(lowest_seg_size > LitDesc::max_size);
static_assert((lowest_blk_size % highest_seg_size) <= lowest_seg_size);

/*static_assert([]{
    for(SZ sz = first_seg_sz; sz <= last_seg_sz; sz++) {
        auto pcs  = num_seg_pieces(sz);
        auto size = size_for_seg_sz(sz);
        if (blk_stride > pcs * size || pcs * size >= blk_stride + size)
            return false;
    }
    return true;
}(),
"Expected all pieces of a given size represented by segments to total >= blk_stride "
"and not exceed that stride by any amount >= the segment size");*/

consteval size_t
abs_offset_for_seg(SZ sz, Offset off)
{ ASSUME_SEG_SZ(sz); return off * num_seg_pieces(sz) + piece_idx_for_seg_sz(sz); }

consteval Offset
rel_offset_for_seg(SZ sz, size_t off)
{ ASSUME_SEG_SZ(sz); return (off - piece_idx_for_seg_sz(sz)) / num_seg_pieces(sz); }

#endif // ! ENABLE_SEGMENTS (after else)
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// Top-level SZ/K API

consteval SZ first_sz = first_seg_sz
           , last_sz  = last_blk_sz;
#ifdef ENABLE_SEGMENTS
static_assert(last_sz > first_sz);
#else
static_assert(last_sz >= first_sz);
#endif
consteval uint8_t num_szs = (uint8_t)(last_sz - first_sz) + 1;
consteval bool is_sz_class(SZ sz) { return (first_sz <= sz) && (sz <= last_sz); }
#define ASSUME_SZ(var) ASSUME(is_sz_class(var), var, "hhx {" #var "}")

consteval auto idx_for_sz(SZ sz) { ASSUME_SZ(sz); return (uint8_t)(sz - first_sz); }

/*#ifdef ENABLE_SEGMENTS
template<SZ sz, typename = std::enable_if_t<is_seg_sz(sz)>>
using SegStride = EqFittedUInt<(size_for_seg_sz(sz) * __CHAR_BIT__)-1>;

template<SZ sz, typename = std::enable_if_t<is_sz_class(sz)>>
using SZ_Stride = std::conditional_t<is_seg_sz(sz), SegStride<sz>, decltype(blk_stride)>;
#endif*/

consteval SZK
szk_for_sz(SZ sz) {
    ifU (sz == SZ::Lit) return SZK::Lit;
    if (sz == first_sz) return SZK::Fst;
#ifdef ENABLE_SEGMENTS
    if (is_seg_sz(sz)) return SZK::Seg;
#endif
    if (is_blk_sz(sz)) return SZK::Blk;
    UNREACHABLE;
}

consteval Offset
size_for_sz(SZ sz) {
    ASSUME_SZ(sz);
    if (is_blk_sz(sz)) return size_for_blk_sz(sz);
#ifdef ENABLE_SEGMENTS
    if (is_seg_sz(sz)) return size_for_seg_sz(sz);
#endif
    UNREACHABLE;
    return 0;
}

consteval auto lowest_size  = lowest_seg_size
             , highest_size = highest_blk_size;

consteval bool
is_sz_size(Offset size)
{ return is_seg_size(size) || is_blk_size(size); }
#define ASSUME_SZ_SIZE(var) ASSUME(is_sz_size(var), (size_t)(var), "zX {" #var "}")

consteval std::optional<SZ>
size_to_sz(Offset size) {
    ASSUME(is_sz_size(size));
    for(SZ sz = first_sz; sz <= last_sz; sz++) if (size_for_sz(sz) == size) return sz;
    return std::nullopt;
}

consteval size_t
stride_for_sz(SZ sz) {
#ifdef ENABLE_SEGMENTS
    if (is_seg_sz(sz)) return size_for_seg_sz(sz) * num_seg_pieces(sz);
#endif
    if (is_blk_sz(sz)) return blk_stride;
    UNREACHABLE;
}

consteval size_t
unit_for_sz(SZ sz) {
#ifdef ENABLE_SEGMENTS
    if (is_seg_sz(sz)) return size_for_seg_sz(sz);
#endif
    if (is_blk_sz(sz)) return blk_stride;
    UNREACHABLE;
}

consteval size_t
offset_for_sz(SZ sz, Offset off) {
#ifdef ENABLE_SEGMENTS
    if (is_seg_sz(sz)) return abs_offset_for_seg(sz, off);
#endif
    if (is_blk_sz(sz)) return off * blk_stride;
    UNREACHABLE;
}

consteval size_t
atlas_end_for_sz(SZ sz, size_t atlas_size) {
#ifdef ENABLE_SEGMENTS
    if (is_seg_sz(sz)) return atlas_size - (atlas_size % stride_for_sz(sz));
#endif
    if (is_blk_sz(sz)) return atlas_size;
    UNREACHABLE;
}

consteval auto&&
for_SZ = [](SZ sz, auto&& todo) -> void {
#ifdef ENABLE_SEGMENTS
    if (is_seg_sz(sz))
        for (SZ sz_ = sz; size_for_sz(sz_) == size_for_sz(sz); sz_++) todo(sz_);
    else
#endif // ENABLE_SEGMENTS
        todo(sz);
};

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// Atlases and Slices thereof

template<typename uint>
using AtlasSlice = const uint *const NO_ALIAS;

template<typename uint>
#ifndef SINGULAR_ATLAS_SIZE

using Atlas = AtlasSlice<uint>;
# define ASSUME_ATLAS_SIZE(var)                        \
ASSUME(var >= highest_size, var, "zX {" #var "}"); \
ASSUME(var % blk_stride == 0, var, "zX {" #var "}");   \
ASSUME(var <= max_atlas_size, var, "zX {" #var "}");

#else // !SINGULAR_ATLAS_SIZE

# define ATLAS_SIZE SINGULAR_ATLAS_SIZE/*##ul*/
// ... continuing template from before #ifndef
using Atlas = const uint[ATLAS_SIZE/sizeof(uint)];
static_assert(ATLAS_SIZE <= std::numeric_limits<size_t>::max());
static_assert(ATLAS_SIZE >= highest_size);
static_assert(ATLAS_SIZE % sizeof(Word) == 0);
static_assert(ATLAS_SIZE <= max_atlas_size);
# define ASSUME_ATLAS_SIZE(var) ASSUME(var == ATLAS_SIZE, var, "zX {" #var "}")

#endif // ! SINGULAR_ATLAS_SIZE

using WordAtlas = Atlas<Word>;
using ByteAtlas = Atlas<std::byte>;

using WordAtlasSlice = AtlasSlice<Word>;
using ByteAtlasSlice = AtlasSlice<std::byte>;

/// Atlas-charted file
struct PACKED AZFile final {
    //const auto magik = { '#', '~' };
    /** Ideas for further Metadata:
     * * Which compression algorithm was prior used (often just via the suffix e.g. _.tar.xz.pi)
     * * Hash (+ algo) of custom atlas, to check
     **/
    condst struct PACKED Metadata final {
        static consteval auto ext_version = /*(std::byte)*/0b11
                            , dyn_version = /*(std::byte)*/0b00;
        /// TODO: [Some sort of magic number]
        decltype(ext_version) version : 2 = /*(unsigned char)*/dyn_version;
        enum class AtlasID : unsigned char/*decltype(version)*/
        { Pi, E, /*...*/ EXT = 0b111 } atlas_id : 3 = /*(unsigned char)*/AtlasID::EXT;
        size_t decharted_size:59 = 0; /// 0 iff "dynamic" size (i.e. couldn't be output at the time)
    } metadata;
    static_assert(sizeof(metadata) <= sizeof(uintptr_t));
    ///uint64_t num_desc; /// So, this is actually implicit in the size of the file (/end of stream)
    Desc body[/*num_desc*/];

    ONLY_DEFAULT_MOVEABLE(AZFile)
};
static_assert(sizeof(AZFile) >= sizeof(Desc));
using AZ = AZFile *const;
using AZMeta = AZFile::Metadata;
using AtlasID = AZMeta::AtlasID;

static consteval auto current_version = AZFile::Metadata::dyn_version;
// TODO: Make dynamic
static constexpr condst AtlasID current_atlas_id = AtlasID::Pi;
// TODO: Multiple different atlases, one per ID

//} // namespace atlas
