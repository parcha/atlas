// ©201[7-9] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats

#pragma once
#include "util.h++"
#include <cstdint>
#include <type_traits>
#include <limits>

template<typename I>
inline constexpr std::enable_if_t<is_unsigned_integral<I>, I>
full_hi_half = (I)(std::numeric_limits<I>::max() << (std::numeric_limits<I>::digits >> 1));

template<typename I>
inline constexpr std::enable_if_t<is_unsigned_integral<I>, I>
full_lo_half = (I)(std::numeric_limits<I>::max() >> (std::numeric_limits<I>::digits >> 1));

/**
 * ffs - find first (least-significant) bit set
 * @x: the word to search
 *
 * This is defined the same way as ffs.
 * Note fls(0) = 0, fls(1) = 1, fls(0x80000000) = 32.
 */
template<typename I>
INLINE constexpr std::enable_if_t<is_unsigned_integral<I>, uint8_t>
ffs(I x) {
    if (!x) return 0;
    constexpr uint8_t num_bits = std::numeric_limits<I>::digits;
    I r = num_bits;
    for (uint8_t b = (uint8_t)(1 << (num_bits-1)), shft = (b>>1); b; b >>= 1, shft += (b>>1)) {
        const I mask = full_lo_half<I> >> shft;
        if (!(x & mask)) {
            x <<= b;
            r -= b;
        }
    }
    return r;
}

/**
 * fls - find last (most-significant) bit set
 * @x: the word to search
 *
 * This is defined the same way as ffs.
 * Note fls(0) = 0, fls(1) = 1, fls(0x80000000) = 32.
 */
template<typename I>
INLINE constexpr std::enable_if_t<is_unsigned_integral<I>, uint8_t>
fls(I x)
{
    if (!x) return 0;
    constexpr uint8_t num_bits = std::numeric_limits<I>::digits;
    I r = num_bits;
    for (uint8_t b = (uint8_t)(1 << (num_bits-1)), shft = (b>>1); b; b >>= 1, shft += (b>>1)) {
        const I mask = full_hi_half<I> << shft;
        if (!(x & mask)) {
            x <<= b;
            r -= b;
        }
    }
    return r;
}
