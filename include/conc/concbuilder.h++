// ©120[17-20] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats

#pragma once

#include "../util.h++"
//#include "../msg.h++"
#include <cstdint>
#include <utility>
#include <type_traits>
#include <memory>

using _NodePtr = uintptr_t;
static inline constexpr const auto ConcBuilder_cap = sizeof(_NodePtr)*2u;
static inline constexpr const auto ConcBuilder_meta_size = 1u;
// _NodePtrs are only 56 bits in length, so converted pointers need the top byte free
static inline constexpr const auto NodePtr_free_byte =
    (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__) ? (sizeof(_NodePtr)-1u) : (_NodePtr)0u;
#define ASSUME_NodePtr(p) \
    ASSUME((p) && (bool)( ( (std::byte*) &(p))[(unsigned)NodePtr_free_byte] ) )
static_assert(sizeof(_NodePtr)*__CHAR_BIT__ == 64u
             ,"ConcBuilder internals assume 64-bit pointers (with <=56 used)");
#define CHUNKS_FOR(T) (CHUNK_BITS(T) / __CHAR_BIT__)
#define CHUNK_BITS(T) (ConcBuilder_cap * __CHAR_BIT__ / sizeof(T))

// Implementation of a ConcTree/ConcRope-backed builder/iterator for immutable sequences of values
// at least 1 byte smaller than a pointer
// NOTE:
// * It ONLY supports conc, append(/prepend), concat, & internal iteration (no cursors);
//   NOT e.g. insertion, updating, indexing, or even (cached) size
// * ALL Node pointers shave the top 8 bits for type & level; they aren't stored per struct
// * There is no empty state; it is given by a nullptr, which aliases a null Conc node
// * Case classes are static funcs encoding Type
// TODO:
// * Parameterize on allocator
// FIXME:
template<typename T, uint16_t _chunk_bits = CHUNK_BITS(T),
         typename = std::enable_if_t< sizeof(T) <= (sizeof(_NodePtr) - ConcBuilder_meta_size)
                                   && ((sizeof(T) * _chunk_bits/__CHAR_BIT__) <= ConcBuilder_cap) >>
class ConcBuilder final {
    static_assert(sizeof(std::byte) == ConcBuilder_meta_size);
public:
    static inline constexpr const uint8_t chunk_cap = _chunk_bits / __CHAR_BIT__;
    static_assert((1u < chunk_cap) && (chunk_cap <= ConcBuilder_cap),
                  "ConcBuilder assumes >1 3-bit capacity, with the last value as a sentinel");

private:
    union Node {
    friend ConcBuilder;

        T entries[chunk_cap];
        struct {
            _NodePtr left;
            //_NodePtr mid;
            _NodePtr right;
        } PACKED;

        inline constexpr Node(_NodePtr left, _NodePtr right) : left(left), right(right)
        { ASSUME_NodePtr(left); ASSUME_NodePtr(right); }

        INLINE_ constexpr Node(T val) : entries( {val} ) {}
        template<typename... Ts
               , typename = std::enable_if_t<(1 < sizeof...(Ts)) && (sizeof...(Ts) <= chunk_cap)> >
        inline constexpr Node(Ts... vals) : entries( {FWD(vals)...} ) {}

        ONLY_DEFAULT_MOVEABLE(Node)
    } PACKED;
    static_assert(sizeof(Node) <= ConcBuilder_cap);

    enum class Type : unsigned char
    { Conc=0u, Single=1u, Chunk, C3, C4, C5, C6, Append = sizeof(_NodePtr)-ConcBuilder_meta_size };
    static INLINE_ constexpr CONST Type size_to_type(const uint8_t size)
    { ASSUME((0 < size) && (size <= chunk_cap)); return (Type)size; }
    static inline constexpr const auto largest_chunk = size_to_type(chunk_cap);

//protected:
public:
    /// This is the ONLY valid interface to Nodes, which are otherwise opaque to clients
    class NodePtr final {
    friend ConcBuilder;

        union {
            _NodePtr raw;
            struct {
                Type type : 3;
                uint8_t level : 5; // Thus we support an effective size of up to 2^(2^5)=2^32-1
                union {
                    uintptr_t _node : 56; /// Must never be null
                    T val;
                } PACKED;
            } PACKED;
        };

        static inline constexpr const uint8_t highest_level = (1 << 5)-1;

public:
        explicit INLINE_ constexpr NodePtr() : raw(NULL) {}
        INLINE_ constexpr operator _NodePtr() const { return raw; }
        INLINE_ constexpr NodePtr(_NodePtr raw) : raw(raw) { ASSUME_NodePtr(raw); }

    private:
        inline constexpr NodePtr(const Type type, const Node& node, const uint8_t level = 0)
        : type(type), level(level), _node((_NodePtr)&node)
        { ASSUME(type != Type::Single); ASSUME(level <= highest_level); }

        INLINE_ constexpr PURE Node&
        node() /*condst*/& {
            ASSUME(type != Type::Single && _node);
        #pragma GCC diagnostic push
        #pragma GCC diagnostic ignored "-Wint-to-pointer-cast"
            return *(Node*)_node;
        #pragma GCC diagnostic pop
        }

        INLINE_ constexpr PURE const Node&
        node() const& { return const_cast<const Node&>( const_cast<NodePtr&>(*this).node() ); }

        inline void del() && {
            ASSUME(type != Type::Single && _node);
            delete &node();
            raw = NULL;
        }

    public:
        inline constexpr NodePtr(const T val, const uint8_t level = 0)
        : type(Type::Single), level(level), val(val) { ASSUME(level <= highest_level); }

        DEFAULT_CONSTRUCTION(NodePtr)

        INLINE_ constexpr PURE NodePtr left () const& { return (NodePtr)(node().left);  }
        INLINE_ constexpr PURE NodePtr right() const& { return (NodePtr)(node().right); }

        // FIXME: Totally bonkers
        //inline ~NodePtr() { if (type != Type::Single && _node) del(); }

        void destroy() && { switch(type) {
        case Type::Single: return;
        case Type::Conc:
        case Type::Append: {
            auto&& l = left(), r = right();
            std::move(l).destroy();
            std::move(r).destroy();
        }
        [[fallthrough]];
        case Type::Chunk ... largest_chunk:
            std::move(*this).del(); return;
        default: UNREACHABLE;
        }}

        INLINE_ constexpr PURE uint8_t size() const
        { ASSUME(type <= largest_chunk); return (uint8_t)type; }

        INLINE_ constexpr CONST bool operator!() const { return !raw; }
    } PACKED;
    static_assert(sizeof(NodePtr) == sizeof(_NodePtr));

private:

    static inline constexpr NodePtr
    Conc(NodePtr&& left, NodePtr&& right, const Type type = Type::Conc) {
        ASSUME(!!left); ASSUME(!!right);
        ASSUME(type == Type::Conc || type == Type::Append);
        const auto node = new Node((_NodePtr)left, (_NodePtr)right);
        ASSUME_NodePtr(node);
        const auto level = left.level > right.level ? left.level : right.level;
        ASSUME(level < NodePtr::highest_level);
        return NodePtr(type, *node, level+1);
    }

    static INLINE_ constexpr NodePtr
    Append(NodePtr&& left, NodePtr&& right) { return Conc(FWD(left), FWD(right), Type::Append); }

    static constexpr NodePtr
    concat(NodePtr&& x, NodePtr&& y) {
        ASSUME(!!x); ASSUME(!!y);
        const int_fast16_t diff = x.level - y.level;
        ifL (-1 <= diff && diff <= 1) return Conc(FWD(x), FWD(y));
        if (diff < -1) {
            ASSUME(x.type == Type::Conc || x.type == Type::Append);
            auto l = x.left(), r = x.right();
            if (l.level >= r.level) return Conc(FWD(l), concat(FWD(r), FWD(y)));
            else {
                ASSUME(r.type == Type::Conc || r.type == Type::Append);
                auto&& rr = concat(FWD(r), FWD(y));
                if (rr.level == x.level - 3) return Conc(FWD(l), Conc(r.left(), FWD(rr)));
                else return Conc(Conc(FWD(l), r.left()), FWD(rr));
            }
        } else {
            ASSUME(diff > 1);
            ASSUME(y.type == Type::Conc || y.type == Type::Append);
            auto r = y.right(), l = y.left();
            if (r.level >= l.level) return Conc(concat(FWD(x), FWD(l)), FWD(r));
            else {
                ASSUME(l.type == Type::Conc || l.type == Type::Append);
                auto&& nll = concat(FWD(x), l.left());
                if (nll.level == y.level - 3) return Conc(Conc(FWD(nll), l.right()), FWD(r));
                else return Conc(FWD(nll), Conc(l.right(), FWD(r)));
            }
        }
    }

    static constexpr NodePtr
    wrap(NodePtr&& x, NodePtr&& y) {
        ASSUME(!!x); ASSUME(!!y);
        ASSUME(y.type != Type::Append);
        if (x.type == Type::Append) return wrap(x.left(), conc(x.right(), FWD(y)));
        else return conc(FWD(x), FWD(y));
    }

public:

    static INLINE_ constexpr NodePtr
    norm(NodePtr&& n) {
        ASSUME(!!n);
        ifL (n.type != Type::Append) return FWD(n);
        else return wrap(n.left(), n.right());
    }

    static INLINE_ constexpr CONST NodePtr Single(T val) { return NodePtr(val); }

    template<typename... Ts>
    static inline constexpr std::enable_if_t<(1 < sizeof...(Ts)) && (sizeof...(Ts) <= chunk_cap)
    , NodePtr>
    Chunk(const Ts... vals) {
        const auto node = new Node(vals...);
        ASSUME_NodePtr(node);
        return NodePtr(size_to_type(sizeof...(Ts)), *node);
    }

    static constexpr NodePtr
    Chunk(const T val, T vals[], const uint8_t len) {
        ASSUME(len < chunk_cap);
        const auto node = new Node(val);
        ASSUME_NodePtr(node);
        std::uninitialized_move_n(vals, len, &node->entries[1]);
        return NodePtr(size_to_type(len+1), *node);
    }

    static constexpr NodePtr
    conc(NodePtr&& x, NodePtr&& y) {
        ifU (!x) return FWD(y);
        ifU (!y) return FWD(x);
        auto&& x_ = norm(FWD(x)), y_ = norm(FWD(y));
        switch(x_.type) {
        case Type::Single:
        case Type::Chunk ... largest_chunk:
            return append(FWD(x_), FWD(y_));
        case Type::Conc:
        case Type::Append:
            switch(y_.type) {
            case Type::Single:
            case Type::Chunk ... largest_chunk:
                return append(FWD(x_), FWD(y_));
            case Type::Conc:
            case Type::Append:
                return concat(FWD(x_), FWD(y_));
            default: UNREACHABLE;
            }
        default: UNREACHABLE;
        }
    }

    static constexpr NodePtr
    append(NodePtr&& x, NodePtr&& y) {
        ifU (!x) return FWD(y);
        ifU (!y) return FWD(x);
        ASSUME(y.type != Type::Append);
        switch(x.type) {
        case Type::Conc: return Append(FWD(x), FWD(y));
        case Type::Single: switch(y.type) {
            case Type::Single: return Chunk(x.val, y.val);
            case Type::Chunk ... largest_chunk: if constexpr (chunk_cap > 2) {
                if (y.size() < chunk_cap) {
                    const auto n = Chunk(x.val, y.node().entries, y.size());
                    y.del();
                    return n;
                }
            }
            [[fallthrough]];
            case Type::Conc: return Conc(FWD(x), FWD(y));
            default: UNREACHABLE;
        }
        case Type::Chunk ... largest_chunk: {
            ASSUME(x.size() > 1);
            if (x.size() == chunk_cap) { return Conc(FWD(x), FWD(y)); }
            else switch(y.type) {
            case Type::Single: {
                auto x_ = x;
                x_.type++;
                x_.node().entries[x.size()] = y.val;
                return x_;
            }
            case Type::Chunk ... largest_chunk: if constexpr (chunk_cap > 2) {
                const auto xz = x.size(), yz = y.size();
                ifU (xz + yz <= chunk_cap) {
                    auto x_ = x;
                    auto& nx_ = x_.node();
                    auto& ny = y.node();
                    std::uninitialized_copy_n(ny.entries, yz, &nx_.entries[xz]);
                    x_.type = size_to_type((uint8_t)x_.type + yz);
                    y.del();
                    return x_;
                }
            }
            [[fallthrough]];
            case Type::Conc: return Conc(FWD(x), FWD(y));
            default: UNREACHABLE;
            }
        }
        case Type::Append: {
            auto r = x.right();
            if (r.level > y.level) return Append(FWD(x), FWD(y));
            else {
                auto&& zs = Conc(FWD(r), FWD(y));
                auto& l = x.left();
                auto&& ret = [&]() -> NodePtr { switch (l.type) {
                case Type::Append: return append(FWD(l), FWD(zs));
                default:
                    if (l.level <= x.level) return conc(FWD(l), FWD(zs));
                    else return Append(FWD(l), FWD(zs));
                }}();
                x.del();
                return ret;
            }
        }
        default: UNREACHABLE;
        }
    }

    static INLINE_ constexpr NodePtr
    append(NodePtr&& x, const T val) { return append(FWD(x), Single(val)); }
    static INLINE_ constexpr NodePtr
    append(const T val, NodePtr&& x) { return append(Single(val), FWD(x)); }

    template<typename F>
    static constexpr bool
    apply(NodePtr start, F&& f) { ASSUME(!!start); switch(start.type) {
    case Type::Single: return f(start.val);
    case Type::Chunk ... largest_chunk: {
        for(uint8_t i=0; i<start.size(); i++) ifU (!f(start.node().entries[i])) return false;
        return true;
    }
    case Type::Conc:
    case Type::Append: {
        ifU (!apply(start.left(), FWD(f))) return false;
        return apply(start.right(), FWD(f));
    }
    default: UNREACHABLE;
    }}
    /*template<typename F>
    static inline constexpr PURE bool
    operator()(NodePtr&& start, F&& f) { return apply<F>(FWD(start), FWD(f)); }*/

    template<typename G, G g, bool par/*ity*/ = false>
    static constexpr PURE bool
    apply(NodePtr start, NodePtr&& end) { ASSUME(!!start); ASSUME(!!end); switch(start.type) {
    case Type::Single: return g(start.val, FWD(end));
    case Type::Chunk ... largest_chunk: {
        for(uint8_t i=0; i<start.size(); i++)
            ifU (!g(start.node().entries[i], FWD(end))) return par;
        return !par;
    }
    case Type::Conc:
    case Type::Append: {
        ifU (!apply(start.left(), FWD(end))) return par;
        return apply(start.right(), FWD(end));
    }
    default: UNREACHABLE;
    }}
    /*template<typename G, G g, bool parity=true>
    static inline constexpr CONST bool
    operator()(NodePtr&& start, NodePtr&& end) { return apply<G,g, parity>(FWD(start), FWD(end)); }*/

    /// Caution: expensive!
    static constexpr PURE uint32_t
    size(NodePtr start) {
        uint32_t accum = 0;
        auto b = apply(FWD(start), [&accum](T) { accum++; return true; });
        ASSUME(b);
        return accum;
    }

};

template<typename T, uint8_t chunk_cap = CHUNKS_FOR(T)>
static INLINE_ constexpr typename ConcBuilder<T, chunk_cap>::NodePtr
operator+(typename ConcBuilder<T, chunk_cap>::NodePtr x
        , typename ConcBuilder<T, chunk_cap>::NodePtr y)
{ return ConcBuilder<T, chunk_cap>::append(FWD(x), FWD(y)); }

template<typename T, uint8_t chunk_cap = CHUNKS_FOR(T)>
static INLINE_ constexpr typename ConcBuilder<T, chunk_cap>::NodePtr
operator+(const T val, typename ConcBuilder<T, chunk_cap>::NodePtr x)
{ return ConcBuilder<T, chunk_cap>::append(FWD(val), FWD(x)); }

template<typename T, uint8_t chunk_cap = CHUNKS_FOR(T)>
static INLINE_ constexpr typename ConcBuilder<T, chunk_cap>::NodePtr
operator+(typename ConcBuilder<T, chunk_cap>::NodePtr x, const T val)
{ return ConcBuilder<T, chunk_cap>::append(FWD(x), FWD(val)); }

template<typename T, uint8_t chunk_cap = CHUNKS_FOR(T)>
static INLINE_ constexpr typename ConcBuilder<T, chunk_cap>::NodePtr&
operator+=(typename ConcBuilder<T, chunk_cap>::NodePtr& x
         , typename ConcBuilder<T, chunk_cap>::NodePtr y)
{ return x = x + y; }

template<typename T, uint8_t chunk_cap = CHUNKS_FOR(T)>
static INLINE_ constexpr typename ConcBuilder<T, chunk_cap>::NodePtr&
operator+=(typename ConcBuilder<T, chunk_cap>::NodePtr& x, const T val)
{ return x = x + val; }

#undef ASSUME_NodePtr
#undef CHUNKS_FOR
#undef CHUNK_BITS
