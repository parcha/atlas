// ©201[7-9] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats

#if 1
#define NDEBUG
#define REASSURE
#else
#define DEBUG
#endif

#include "concbuilder.hpp"
#include "../msg.hpp"

using TestConcBuilder = ConcBuilder<uint32_t, 4>;
//using TestConcBuilder = ConcBuilder<uint16_t, 6>;
using NodePtr = TestConcBuilder::NodePtr;
using std::move;

static void MSG_size(NodePtr n)
{ MSG("Size: %x", TestConcBuilder::size(move(n))); }
static void MSG_meta(NodePtr n)
{ MSG("Type/Level: %hhu/%u", n.type, n.level); }
static void MSG_vals(NodePtr n)
{ TestConcBuilder::apply(move(n), [](auto val) { printf("%x", val); return true; }); printf("\n"); }

static void MSG_all(NodePtr n)
{ MSG_size(n); MSG_meta(n); MSG_vals(n); }

static NodePtr append(NodePtr x, NodePtr y) {
    MSG("\nAppending...");
    auto n = TestConcBuilder::append(move(x), move(y));
    MSG_all(n);
    return n;
}

static NodePtr conc(NodePtr x, NodePtr y) {
    MSG("\nConcing...");
    auto n = TestConcBuilder::conc(move(x), move(y));
    MSG_all(n);
    return n;
}

int main(int argc, char *argv[]) {
    MSG("Hello"); argc--;
    auto node0 = TestConcBuilder::Single(argc+0)
       , node1 = TestConcBuilder::Single(argc+1)
       , node2 = TestConcBuilder::Single(argc+2)
       , node3 = TestConcBuilder::Single(argc+3)
       , node4 = TestConcBuilder::Single(argc+4)
       , node56 = TestConcBuilder::Chunk(argc+5, argc+6)
       , node789 = TestConcBuilder::Chunk(argc+7, argc+8, argc+9);
    MSG("World");

    auto&& node01 = append(node0, node1);
    auto&& node012 = append(node01, node2);
    auto&& node3012 = conc(node3, node012);
    auto&& node30124 = append(node3012, node4);
    auto&& node78956 = append(node789, node56);
    auto&& nodes = conc(node30124, node78956);

    MSG("\nNorming");
    auto&& nodes_ = TestConcBuilder::norm(nodes);
    MSG_all(nodes_);
    nodes_.destroy();
    return 0;
}
