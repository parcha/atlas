// ©120[17-20] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats

#pragma once
#include "util.h++"
#include "popcnt.h++"

// NOTE: We roll our own hex converting so we can transparently support multiple bit sizes, esp.
// uint128_t, which standard printf doesn't support.

template<typename I>
INLINE constexpr CONST std::enable_if_t<is_unsigned_integral<I>, uint8_t>
itox_sz(I i) {
    const auto b = fls<I>(i);
    if (b != 0) return (b / 4) + ((b % 4) ? 1 : 0);
    else        return 1;
}

// Convert integer to hex string WITHOUT dynamic allocation or a null terminator (length is precise)
template<typename I>
inline constexpr std::enable_if_t<is_unsigned_integral<I>, void>
_itox(I val, char str[], uint8_t len) {
    ASSUME(divides(2, len));
    ASSUME(sizeof(I)*2 >= len);
    ASSUME(len >= itox_sz(val));
    constexpr auto start_char = 'a' /*'A'*/;
    for(I acc = val, i = 0; i < (I)len; acc>>=4, i++) {
        const uint8_t n = acc & 0xF;
        str[len-1-i] = n + ((n < 10) ? '0' : (start_char - 10));
    }
}

template<typename I>
_INLINE constexpr std::enable_if_t<is_unsigned_integral<I>, void>
_itox(I val, char str[]) { _itox(val, str, itox_sz(val)); }

#define itox(...) VA_SELECT(itox, __VA_ARGS__)
#define itox_2(_val, _digits) ({ \
    const auto __val = (_val); \
    const auto __len = (_digits); \
    auto __str = (char *const)alloca(__len+1); \
    ASSUME_1(__str); \
    _itox(__val, __str, __len); \
    __str[__len] = '\0'; \
    __str; \
})
#define itox_1(_val) itox_2(_val, itox_sz(__val /*captured in itox_2*/))

// Define overloads for `itox`
#if 0
#define DECL_itox(I) \
inline constexpr void itox(I v, char str[], uint8_t len);

DECL_itox(uint8_t)
DECL_itox(uint16_t)
DECL_itox(uint32_t)
DECL_itox(uint64_t)
DECL_itox(uint128_t)

#undef DECL_itox
#endif
