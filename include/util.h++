// ©120[17-20] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats

#pragma once
#include <cassert>
#include <cstddef>
#include <type_traits>
#include <limits>
#include "suffix128.hpp" // GCC doesn't have 128-bit literals yet...

// REASSURE: The programmer is asserting there will be no "errors"
// INSHALLAH: They additionally assert there will be no "warnings"... good luck with that
#if defined INSHALLAH && !defined REASSURE
# define REASSURE
#endif

#if defined DEBUG && defined NDEBUG
# warning "DEBUG and NDEBUG both defined; weird combos yield weird results"
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

// NB: Sometimes disabled until all uses are audited against our "golden" compiler
#define _INLINE __attribute__((always_inline))
#define INLINE_ inline _INLINE
#define NOINLINE __attribute__((noinline))
#define _FLATTEN __attribute__((flatten))
#define LEAF __attribute__((leaf))

#ifdef OPTIMIZE_XTREME
# define INLINE INLINE_
# define FLATTEN _FLATTEN
#else // OPTIMIZE_XTREME
# define INLINE inline
# define FLATTEN
#endif // OPTIMIZE_XTREME

#define PACKED __attribute__((packed))

#define NOCLONE __attribute__((noclone))

#define NORETURN __attribute__((noreturn))

#define TRANSPARENT __attribute__((transparent_union))

#define ALIGNED(x) __attribute__((aligned(x)))

#define CLEANUP(f) __attribute__((cleanup(f)))

#ifndef DEBUG
#define PURE __attribute__((pure))
#define CONST __attribute__((const))
#else // DEBUG
// Due to debug printing, we broadly can't trust these
#define PURE
#define CONST
#endif // DEBUG

#define NONNULL __attribute__((nonnull))
#define RETURNS_NONNULL __attribute__((returns_nonnull))
#ifndef DEBUG
#define SHOULD_NONNULL RETURNS_NONNULL
#else
#define SHOULD_NONNULL
#endif

#define NEWLIKE __attribute__((malloc))

#define HOT __attribute__((hot))
#define COLD __attribute__((cold))

#define USED __attribute__((used))

#define OPTIMIZE(flag) __attribute__((optimize(flag)))

#define DEPRECATED __attribute__((deprecated))

#define MAYBE_UNUSED __attribute__((unused))
#define NO_ALIAS __restrict

#define ALIGN_LIKE(T) alignas(alignof(T))

#if !defined(DEBUG) || (defined(NDEBUG) && defined(INSHALLAH))
# define UNREACHABLE __builtin_unreachable()
#else
# define UNREACHABLE abort()
#endif

#define SECTION(sec) __attribute__((section(#sec)))

#define DEFAULT_MOVE_CONSTRUCTOR(Type) constexpr Type(Type&&) = default;
#define DEFAULT_COPY_CONSTRUCTOR(Type) explicit constexpr Type(const Type&) = default;

#define DEFAULT_MOVER(Type)  constexpr Type& operator=(Type&&) = default;
#define DEFAULT_COPIER(Type) constexpr Type& operator=(const Type&) = default;

#define DEFAULT_MOVING(Type)  DEFAULT_MOVE_CONSTRUCTOR(Type) DEFAULT_MOVER(Type)
#define DEFAULT_COPYING(Type) DEFAULT_COPY_CONSTRUCTOR(Type) DEFAULT_COPIER(Type)

#define DEFAULT_CONSTRUCTION(Type) DEFAULT_MOVING(Type) DEFAULT_COPYING(Type)

#define DELETE_MOVE_CONSTRUCTOR(Type) constexpr Type(Type&&) = delete;
#define DELETE_COPY_CONSTRUCTOR(Type) explicit constexpr Type(const Type&) = delete;

#define DELETE_MOVER(Type)  constexpr Type& operator=(Type&&) = delete;
#define DELETE_COPIER(Type) constexpr Type& operator=(const Type&) = delete;

#define DELETE_MOVING(Type)  DELETE_MOVE_CONSTRUCTOR(Type) DELETE_MOVER(Type)
#define DELETE_COPYING(Type) DELETE_COPY_CONSTRUCTOR(Type) DELETE_COPIER(Type)

#define DELETE_CONSTRUCTION(Type) DELETE_MOVING(Type) DELETE_COPYING(Type)

#define ONLY_DEFAULT_MOVEABLE(Type) DELETE_COPYING(Type) DEFAULT_MOVING(Type)
#define ONLY_DEFAULT_COPYABLE(Type) DELETE_MOVING(Type) DEFAULT_COPYING(Type)

////////////////////////////////////////////////////////////////////////////////////////////////////
// Macro generation utilities

#define CAT(A,B) A ## B
#define SELECT(NAME, NUM) CAT( NAME##_, NUM )

#define GET_COUNT(_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11,_12,_13,_14,_15,_16, COUNT, ...) COUNT
#define VA_SIZE(...) GET_COUNT(__VA_ARGS__, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1)
#define VA_SELECT(NAME, ...) SELECT(NAME, VA_SIZE(__VA_ARGS__))(__VA_ARGS__)
#define VA_SELECT_1(NAME, _0) SELECT(NAME, 0)(_0)

#define APPLY(m, ...) VA_SELECT_1(APPLY, m, ##__VA_ARGS__)
#define APPLY_0(m)
#define APPLY_1(m, _0) \
m(_0)
#define APPLY_2(m, _0, _1) \
m(_0) m(_1)
#define APPLY_3(m, _0, _1, _2) \
m(_0) m(_1) m(_2)
#define APPLY_4(m, _0, _1, _2, _3) \
m(_0) m(_1) m(_2) m(_3)
#define APPLY_5(m, _0, _1, _2, _3, _4) \
m(_0) m(_1) m(_2) m(_3) m(_4)
#define APPLY_6(m, _0, _1, _2, _3, _4, _5) \
m(_0) m(_1) m(_2) m(_3) m(_4) m(_5)
#define APPLY_7(m, _0, _1, _2, _3, _4, _5, _6) \
m(_0) m(_1) m(_2) m(_3) m(_4) m(_5) m(_6)
#define APPLY_8(m, _0, _1, _2, _3, _4, _5, _6, _7) \
m(_0) m(_1) m(_2) m(_3) m(_4) m(_5) m(_6) m(_7)
#define APPLY_9(m, _0, _1, _2, _3, _4, _5, _6, _7, _8) \
m(_0) m(_1) m(_2) m(_3) m(_4) m(_5) m(_6) m(_7) m(_8)
#define APPLY_10(m, _0, _1, _2, _3, _4, _5, _6, _7, _8, _9) \
m(_0) m(_1) m(_2) m(_3) m(_4) m(_5) m(_6) m(_7) m(_8) m(_9)
#define APPLY_11(m, _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _A) \
m(_0) m(_1) m(_2) m(_3) m(_4) m(_5) m(_6) m(_7) m(_8) m(_9) m(_A)
#define APPLY_12(m, _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _A, _B) \
m(_0) m(_1) m(_2) m(_3) m(_4) m(_5) m(_6) m(_7) m(_8) m(_9) m(_A) m(_B)
#define APPLY_13(m, _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _A, _B, _C) \
m(_0) m(_1) m(_2) m(_3) m(_4) m(_5) m(_6) m(_7) m(_8) m(_9) m(_A) m(_B) m(_C)
#define APPLY_14(m, _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _A, _B, _C, _D) \
m(_0) m(_1) m(_2) m(_3) m(_4) m(_5) m(_6) m(_7) m(_8) m(_9) m(_A) m(_B) m(_C) m(_D)
#define APPLY_15(m, _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _A, _B, _C, _D, _E) \
m(_0) m(_1) m(_2) m(_3) m(_4) m(_5) m(_6) m(_7) m(_8) m(_9) m(_A) m(_B) m(_C) m(_D) m(_E)
#define APPLY_16(m, _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _A, _B, _C, _D, _E, _F) \
m(_0) m(_1) m(_2) m(_3) m(_4) m(_5) m(_6) m(_7) m(_8) m(_9) m(_A) m(_B) m(_C) m(_D) m(_E) m(_F)

////////////////////////////////////////////////////////////////////////////////////////////////////

#define STRINGIFY(X) _STRINGIFY(X)
#define _STRINGIFY(X) #X
#define CHARIFY(X) (#X)[0]

#define EXPECT(x, v) __builtin_expect((x), (v))
// Use these when the expected condition is at least roughly 99.99% (or 1/10000 for UNLIKELY)
#define LIKELY(x)   EXPECT(!!(x), true)
#define UNLIKELY(x) EXPECT(!!(x), false)

#define ifU(_) if ( UNLIKELY( (_) ) )
#define ifL(_) if (   LIKELY( (_) ) )

#define ASSUME(...) VA_SELECT(ASSUME, ##__VA_ARGS__)

#if defined NDEBUG && defined REASSURE
#define ASSUME_1(cond) ifU (!(cond)) { UNREACHABLE; }
#else
#define ASSUME_1(cond) { assert(LIKELY(cond)); }
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////
//#ifdef __cplusplus

// Hacky, C++-only container_of impl
#ifndef container_of
#define container_of(ptr, type, member) ({           \
const auto ___mptr = (decltype(type::member)*)(ptr); \
(type *)((std::byte *)___mptr - offsetof(type, member)); })
#endif

#define offsetof_of(S, m, T) (__builtin_offsetof(S, m) / sizeof(T))

//#ifndef FWD
# define FWD(...) std::forward<decltype( __VA_ARGS__ )>( __VA_ARGS__ )
//#endif

#ifndef LAUNDER
# define LAUNDER(...) std::launder( FWD( __VA_ARGS__ ) )
#endif

#ifdef EMBARALLEL
# define volatilish volatile
#elif INSHALLAH
# define volatilish condst
#else
# define volatilish
#endif

#ifdef DEBUG
# define debuggable volatile
//#elif defined NDEBUG
//# define debuggable condst
#else
# define debuggable
#endif

/// Conditionally constant / mutable
#ifdef ENABLE_INDEXING
# define condst
# define mudable mutable
#else
# define condst const
# define mudable
#endif

#ifndef uint128_t
using uint128_t = unsigned __int128;
#endif

template<typename I>
inline constexpr bool is_unsigned_integral =
    (std::is_integral_v<I> && !std::numeric_limits<I>::is_signed) || std::is_same_v<I, std::byte>;

template<typename Ia, typename Ib>
INLINE_ constexpr CONST bool divides(Ia a, Ib b) { return (b % a) == 0; }

//#endif // __cplusplus
