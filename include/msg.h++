// ©120[17-20] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats

#pragma once
#include "util.h++"
#include <cstdlib>
#include <cstdio>

// Supplemental to util.hpp
#define ASSUME_3(cond, var, fmt) ifU (!(cond)) { DBG_(var, fmt); ASSUME_1(cond); }

#define __MSG(to, fmt, ...)   fprintf(to, fmt, ##__VA_ARGS__)
#define _MSG(to, fmt, ...)   __MSG(to, fmt "\n", ##__VA_ARGS__)
#define MSG(fmt, ...)        _MSG(/*stdout*/stderr, fmt, ##__VA_ARGS__)

#ifdef ENABLE_ASCII_ESCAPES
    #define ascii_esc_bold "\033[1m"
    #define ascii_esc_norm "\033[0m"
#else // ENABLE_ASCII_ESCAPES
    #define ascii_esc_bold
    #define ascii_esc_norm
#endif // ENABLE_ASCII_ESCAPES


#define wrn_esc_hd ascii_esc_bold
#define wrn_esc_tl ascii_esc_norm

#define _WRN(fmt, ...) _MSG(stderr, wrn_esc_hd "WARN: " fmt wrn_esc_tl, ##__VA_ARGS__)
#ifndef TERSE
    #define WRN _WRN
#elif defined(TERSE) && !defined(DEBUG)
    #define WRN(...) { } // Shut up
#elif INSHALLAH
    #define WRN(...) { UNREACHABLE; } /* have faith that we're warn-less */
#else // !TERSE
    #define WRN DBG /* potentially render WRN ineffectual */
#endif


#define err_esc_hd ascii_esc_bold
#define err_esc_tl ascii_esc_norm

#define _ERR(fmt, ...)  _MSG(stderr, err_esc_hd "ERROR: " fmt err_esc_tl, ##__VA_ARGS__)
#ifndef NDEBUG
    #define ERR _ERR
#elif defined(DEBUG) /* NDEBUG */
    #define ERR _WRN
#elif !defined(REASSURE) /* NDEBUG & !DEBUG */
    #define ERR DBG /* render ERR ineffectual */
#else /* NDEBUG & !DEBUG & REASSURE */
    #define ERR(...) { UNREACHABLE; } /* reassure the compiler */
#endif


#define dbg_esc_hd ascii_esc_bold
#define dbg_esc_tl ascii_esc_norm

#ifdef DEBUG
    #ifndef TERSE
        // For now, we always wrap, for clarity
        static inline constexpr const size_t header_split_cutoff = 0;
        #define DBG(fmt, ...) {                                               \
            if constexpr(sizeof(__FILE__) + sizeof(__PRETTY_FUNCTION__) < header_split_cutoff) \
                _MSG(stderr, "@%s:%d,%s; " dbg_esc_hd fmt dbg_esc_tl,         \
                     __FILE__, __LINE__, __PRETTY_FUNCTION__, ##__VA_ARGS__); \
            else                                                              \
                _MSG(stderr, "@%s:%d,%s\n\t" dbg_esc_hd fmt dbg_esc_tl,       \
                     __FILE__, __LINE__, __PRETTY_FUNCTION__, ##__VA_ARGS__); \
        }
    #else // !TERSE*/
        // If we've ALSO enabled TERSE, still enable DBG, but be terse in our header
        #define DBG(fmt, ...) _MSG(stderr, dbg_esc_hd "DEBUG: " fmt dbg_esc_tl, ##__VA_ARGS__)
    #endif // !TERSE
#else // DEBUG
    #define DBG(fmt, ...) { }
#endif // DEBUG
#define DBG_HERE DBG("")
#define DBG_(expr, fmt) DBG(#expr ":\t%" fmt, expr)
