// ©120[17-20] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats
// NOTE: Intended to be multiply-sourced

#ifdef IOTA
#undef IOTA
#endif

#ifdef __INIT_COUNTER__
#undef __INIT_COUNTER__
#endif

//  TODO: How to capture name?
#if 0
#define DECL_INIT_COUNTER \
constexpr auto __INIT_COUNTER__##__LINE__ = __COUNTER__;
#endif

static inline constexpr const auto __INIT_COUNTER__ = __COUNTER__;
#define IOTA (__COUNTER__ - __INIT_COUNTER__ - 1)
