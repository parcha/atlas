// ©120[17-20] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats

#pragma once
#include <cstdint>
#include <limits>
#include <cstring> // memset
#include <type_traits>
#include "util.h++"

#ifdef EMBARALLEL
# include <atomic>
#endif

using std::byte;

// Bit-compact array where the elements are nibbles (4-bits)
template<size_t nelems, uint8_t max_val = (1 << 4)-1>
struct Nibset {
public:
    static inline constexpr const uint8_t max_nib = (1 << 4)-1;
    static_assert(max_val <= max_nib);
    using Pos = uint32_t;
    static_assert(1 < nelems && nelems <= 1+(size_t)std::numeric_limits<Pos>::max(),
                  "nelems must be > 1 & <= 2^32");

protected:
    using _Storage = byte;
    using Storage =
    #ifndef EMBARALLEL
        _Storage;
    #else
        std::atomic<_Storage>;
    #endif
    static_assert(sizeof(_Storage) == sizeof(Storage));

#ifdef SPACE_EFFICIENT_NIBSET
    static inline constexpr const uint8_t nibs_per = __CHAR_BIT__/4 * sizeof(Storage);
    static inline constexpr const Pos size = (nelems / nibs_per) + ((nelems % nibs_per) ? 1 : 0);

    struct Div final { const Pos quot, rem; };
    static CONST INLINE constexpr Div div_pos(Pos pos) { return {pos / nibs_per, pos % nibs_per}; }
#else
    static inline constexpr const Pos size = nelems;
#endif // SPACE_EFFICIENT_NIBSET

private:
    Storage data[size];

public:
    DEFAULT_MOVING(Nibset)

    inline void
    set(uint8_t val) {
        ASSUME(val <= max_val);
    #ifdef SPACE_EFFICIENT_NIBSET
        memset(data, val | (val << 4), size);
    #else
        memset(data, val, size);
    #endif
    }

    inline void reset(void) { set(0); }

    inline void
    set(Pos pos, uint8_t val) {
        ASSUME(pos < nelems);
        ASSUME(val <= max_val);
    #ifdef SPACE_EFFICIENT_NIBSET
        Div d = div_pos(pos);
        auto mask_shift = 4*d.rem;
        auto mask = (_Storage)0b1111 << mask_shift;
        data[d.quot] = (data[d.quot] & ~mask) | ((_Storage)val << mask_shift);
    #else
        data[pos] = val;
    #endif
    }

    PURE inline uint8_t
    get(Pos pos) const {
        ASSUME(pos < nelems);
    #ifdef SPACE_EFFICIENT_NIBSET
        Div d = div_pos(pos);
        auto mask_shift = 4*d.rem;
        auto mask = (_Storage)0b1111 << mask_shift;
        auto ret = (uint8_t)((data[d.quot] & mask) >> mask_shift);
    #else
        auto ret = (uint8_t)data[pos];
    #endif
        ASSUME(ret <= max_val);
        return ret;
    }
} PACKED;
