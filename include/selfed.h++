// ©120[17-20] Alex Marquez <alex@grokalex.com>; GNU AGPL-v3+ with caveats

#pragma once
#include "util.h++"

template<typename _Self> struct Selfed {
protected:
    using Self = _Self;

#define _SELF (this->_self())

    CONST INLINE constexpr RETURNS_NONNULL Self *const
    _self() { return (Self *const)this; }
    CONST INLINE constexpr RETURNS_NONNULL const Self *const
    _self() const { return (const Self *const)this; }

#define SELF (this->self())

    PURE INLINE constexpr Self &NO_ALIAS
    self() & { return *_self(); }
    PURE INLINE constexpr const Self &NO_ALIAS
    self() const& { return *_self(); }
};
